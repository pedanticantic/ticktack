<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    return View::make('welcome');
});

Route::get('/register', function() {
    return View::make('register', ['username' => '']);
});

Route::post('/register', 'UserController@handleRegistration');

Route::get('/login', function() {
    return View::make('login', ['username' => '']);
});

Route::post('/login', 'UserController@handleLogIn');
    
Route::get('/api', function() {
    return View::make('api');
});
Route::get('/api/v1/login/', 'UserController@login');

// All these pages (routes) require the user to logged in, hence grouping them together.
Route::group(['before' => 'auth'], function() {
    
    Route::get('/logout', 'UserController@handleLogOut');
    
    Route::get('/release_notes', function() {
        return View::make('release_notes');
    });
    
    // Projects
    Route::get('/projects', function() {
        return View::make('projects.projects', ['projects' => Project::listPaginated()]);
    });
    Route::get('/project/create', function() {
        return View::make('projects.project_form', ['allUsers' => Project::getAllUsers()]);
    });
    Route::post('/project/create', 'ProjectController@handleSave');
    Route::get('/project/{id}', 'ProjectController@showProject');
    Route::get('/project/edit/{id}', 'ProjectController@editProject');
    Route::post('/project/edit/{id}', 'ProjectController@handleSave');
    
    // Tickets
    Route::get('/project/{projectid}/ticket/create', function($projectid) {
        Session::put('projectid', $projectid);
        $current_user_id = Auth::id();
        $assigned_to_user_id = null;
        $project = Project::find($projectid);
        if ($project != null) {
            $assigned_to_user_id = $project->lead_user_id;
        }
        return View::make('tickets.ticket_form', ['current_user_id' => $current_user_id, 'assigned_to_user_id' => $assigned_to_user_id, 'allProjects' => Project::getAllProjects(), 'allUsers' => Project::getAllUsers(), 'allPriorities' => Ticket::getAllPriorities(), 'allStatuses' => Ticket::getAllStatuses()]);
    });
    Route::post('/project/{projectid}/ticket/create', 'TicketController@handleSaveNew');
    Route::get('/ticket/create', function() {
        $current_user_id = Auth::id();
        return View::make('tickets.ticket_form', ['current_user_id' => $current_user_id, 'allProjects' => Project::getAllProjects(), 'allUsers' => Project::getAllUsers(), 'allPriorities' => Ticket::getAllPriorities(), 'allStatuses' => Ticket::getAllStatuses()]);
    });
    Route::post('/ticket/create', 'TicketController@handleSaveNew');
    Route::get('/ticket/{id}', 'TicketController@showTicket');
    Route::get('/ticket/edit/{id}', 'TicketController@editTicket');
    Route::post('/ticket/edit/{id}', 'TicketController@handleSaveUpdate');
    Route::get('/tickets', 'TicketController@ticketSearch');
    
    // Searching
    Route::post('/tickets/search', 'TicketController@ticketSearchApply');
    Route::get('/tickets/filter', 'TicketController@ticketFilterApply');
    
    // Comments on tickets.
    Route::post('/ticket/{id}/comment/create', 'CommentController@handleSave');
    
    // Assignments.
    Route::get('/assignment/{id}/remove', 'AssignmentController@removeAssignment');
    Route::get('/assignment/{id}/{action}', 'AssignmentController@actionAssignment');
    
    // Ticket relationships
    Route::get('/ticket/relation/{id}/{src}/{action}', 'TicketRelationController@handleAction');
    Route::post('/ticket/{id}/relation/create', 'TicketRelationController@handleCreate');
    
    // Time spent on tickets
    Route::post('/ticket/{id}/time_spent/create', 'TicketTimeSpentController@handleCreate');
    
    // To Do Views
    Route::get('/todoviews', function() {
        return View::make('to_do_views.list');
    });
    Route::get('/todoview/form', function() {
        return View::make('to_do_views.form');
    });
    Route::get('/todoview/form/{id}', function($id) {
        $toDoView = ToDoView::getView($id);
        if ($toDoView == null) {
            return Redirect::to('/todoviews');
        }
        return View::make('to_do_views.form', ['toDoView' => $toDoView]);
    });
    Route::post('/todoview/form', 'ToDoViewController@handleSave');
    Route::get('/todoview/{id}', 'ToDoViewController@showView');
    // Various interactions between tickets and views. I decided the convention would be:
    //   '/todoview/<viewId>/ticket/<ticketId>/<action>'
    Route::get('/todoview/{viewId}/ticket/{ticketId}/add', 'ToDoViewController@addTicketToView');
    Route::get('/todoview/{viewId}/ticket/{ticketId}/update_position', 'ToDoViewController@updateTicketPosition');
    Route::get('/todoview/{viewId}/ticket/{ticketId}/remove', 'ToDoViewController@removeTicket');
    Route::get('/todoview/{viewId}/ticket/{ticketId}/summary', 'ToDoViewController@setTicketSummary');
    // Interactions with the viewport. I decided the convention would be:
    //   '/todoview/<viewId>/<viewport_action>'
    Route::post('/todoview/{viewId}/update_viewport_position', 'ToDoViewController@updateViewportPosition');
    
});

// Route group for API versioning. The first set of routes don't require authentication.
Route::get('api/v1/referenceData', 'TicketController@referenceData');
Route::post('api/v1/user', 'UserController@store');
Route::group(array('prefix' => 'api/v1', 'before' => 'auth.basic'), function()
{
    
    Route::resource('project', 'ProjectController');
    Route::get('user', 'UserController@index');
    
    Route::get('project/{id}/ticket', 'TicketController@index');
    Route::post('project/{id}/ticket', 'TicketController@store');
    
    Route::resource('ticket', 'TicketController');

    Route::post('ticket/{id}/assignment/{user_id}', 'AssignmentController@assignment');
    Route::delete('assignment/{id}', 'AssignmentController@destroy');
    Route::put('assignment/{id}/{direction}', 'AssignmentController@moveAssignment');
    
    Route::resource('ticket/{id}/comment', 'CommentController');
    Route::get('comment', 'CommentController@index');
    
    Route::resource('ticket/{id}/time_spent', 'TicketTimeSpentController');
    
    Route::resource('ticket/{id}/associated', 'TicketRelationController');
    Route::put('ticket/{ticketId}/associated/{relationshipId}/{action}', 'TicketRelationController@update');
    
});