<?php

require_once 'ClearDown.php';

class CommentTest extends TestCase {
    
    /**
     * var int $project
     */
    private $project;
    
    /**
     * var Ticket $ticket
     */
    private $ticket;
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        
        // We need a project to put our tickets against.
        $this->project = new Project;
        $this->project->id = 1;
        $this->project->name = 'For Tickets';
        $this->project->description = 'For Tickets';
        $this->project->lead_user_id = 1;
        $this->project->save();
        
        // And a ticket to put the comments on.
        $this->ticket = new Ticket();
        $this->ticket->project_id = $this->project->id;
        $this->ticket->ticket_type = 'task';
        $this->ticket->summary = 'Testing comments';
        $this->ticket->description = $this->ticket->summary;
        $this->ticket->created_by_user_id = 1;
        $this->ticket->priority = 'high';
        $this->ticket->status = 'new';
        $this->assertTrue($this->ticket->isValid(), 'Ticket for comments should be valid');
        $this->ticket->save();
        
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test the basic comment functionality - creating a model.
     *
     * @return void
     */
    public function testCreate()
    {
        // Test the mandatory fields. Iterate through them - on the nth loop, leave the nth field blank.
        for($commentIndex = 0 ; $commentIndex < 3 ; $commentIndex++) {
            $comment = new Comment();
            if ($commentIndex != 0) {
                $comment->ticket_id = $this->ticket->id;
            }
            if ($commentIndex != 1) {
                $comment->added_by_user_id = 1;
            }
            if ($commentIndex != 2) {
                $comment->details = 'Test comment';
            }
            $this->assertFalse($comment->isValid(), 'Null field was allowed (' . $commentIndex . ')');
        }
    }
    
}
