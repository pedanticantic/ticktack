<?php

require_once 'ClearDown.php';

class ProjectTest extends TestCase {
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test the basic project functionality - creating a model.
     *
     * @return void
     */
    public function testCreate()
    {
        // Try creating projects with missing mandatory fields.
        $projectId1 = 1;
        $projectId2 = 2;
        $projectName = 'UnitTest';
        $projectDescription = 'For unit tests';
        $leadUserId = 1;
        
        // Missing name.
        $project = new Project;
        $project->id = $projectId1;
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertFalse($project->isValid(), 'Null name was allowed');
        
        // Missing description.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = $projectName;
        $project->lead_user_id = $leadUserId;
        $this->assertFalse($project->isValid(), 'Null description was allowed');
        
        // Missing lead_user_id.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = $projectName;
        $project->description = $projectDescription;
        $this->assertFalse($project->isValid(), 'Null lead user was allowed');
        
        // Try creating projects with invalid names.
        // Too short.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = 'aaaa';
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertFalse($project->isValid(), 'Short name allowed');
        // Too long.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = '123456789012345678901234567890a';
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertFalse($project->isValid(), 'Long name allowed');
        // Invalid characters.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = '12345!():;';
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertFalse($project->isValid(), 'Invalid symbols allowed in name');
        
        // And creating project with a duplicate mame.
        // First project.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = $projectName;
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertTrue($project->isValid());
        $project->save();
        // And the duplicate.
        $project = new Project;
        $project->id = $projectId2;
        $project->name = $projectName;
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertFalse($project->isValid(), 'Able to create duplicate name');
        
    }
    
    /**
     * Test the basic project functionality - updating a model.
     *
     * @return void
     */
    public function testUpdate()
    {
        $projectId1 = 3;
        $projectId2 = 4;
        $projectName1 = 'UnitTest';
        $projectName2 = '2nd UnitTest';
        $projectDescription = 'For unit tests';
        $leadUserId = 1;
        
        // Create the first one.
        $project = new Project;
        $project->id = $projectId1;
        $project->name = $projectName1;
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertTrue($project->isValid());
        $project->save();
        
        // Create the second one (with a different name).
        $project = new Project;
        $project->id = $projectId2;
        $project->name = $projectName2;
        $project->description = $projectDescription;
        $project->lead_user_id = $leadUserId;
        $this->assertTrue($project->isValid(), 'Could create a project with a valid name');
        $project->save();
        $projectId2 = $project->id;
        
        // Now update the 2nd one. Firstly, to a new name, then one the same as the first project.
        $project = Project::find($projectId2);
        $project->name = $project->name . 'x';  // Is valid
        $this->assertTrue($project->isValid(), 'Could not change name to something valid');
        $project->save();
        $project->name = $projectName1; // Is now invalid
        $this->assertFalse($project->isValid(), 'Allowed name to be changed to that of an existing project');
        
    }

}
