<?php
class ClearDown {
    public static function run() {
        DB::table('comments')->delete();
        DB::table('to_do_views_tickets')->delete();
        DB::table('to_do_views')->delete();
        DB::table('tickets_associations')->delete();
        DB::table('tickets_assignments')->delete();
        DB::table('time_spent')->delete();
        DB::table('tickets')->delete();
        DB::table('projects')->delete();
        DB::table('users')->where('id', '>', 1)->delete();
    }
}