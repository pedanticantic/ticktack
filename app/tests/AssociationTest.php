<?php

require_once 'ClearDown.php';

class AssociationTest extends TestCase {
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        
        // We need a project to put our tickets against.
        $project = new Project;
        $project->id = 1;
        $project->name = 'For Tickets';
        $project->description = 'For Tickets';
        $project->lead_user_id = 1;
        $project->save();
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Private method for checking the data for 2 association records.
     *
     * @param int $ticketId1
     *
     * @return TicketAssociation[]
     */
    protected function checkAssocData($ticketId1) {
        $assocRows = TicketAssociation::orderBy('src_sort_order')->get();
        $this->assertEquals(count($assocRows), 2, 'Expected 2 relationship records');
        foreach($assocRows as $assocIndex => $assocRow) {
            $this->assertEquals($assocRow->relationship_type, 'parent', 'Should be parent');
            $this->assertEquals($assocRow->src_ticket_id, $ticketId1, 'Incorrect source ticket id');
            $this->assertEquals($assocRow->src_sort_order, 1 + $assocIndex, 'Incorrect source sort order');
            $this->assertEquals($assocRow->trg_ticket_id, 2 + $assocIndex, 'Incorrect target ticket id');
            $this->assertEquals($assocRow->trg_sort_order, 1, 'Target sort order is not 1');
            $this->assertEquals($assocRow->pinned, $assocIndex, 'Incorrect value for pinned');
        }
        return $assocRows;
    }
    
    /**
     * Test managing related tickets.
     *
     * @return void
     */
    public function testRelated()
    {
        // Define some values.
        $ticketId1 = 1;
        $ticketProjectId = 1; // See setUp above
        $ticketTicketType = 'task';
        $ticketSummary = 'Base ticket';
        $ticketDescription = 'For testing related tickets';
        $ticketCreatedByUserId = 1;
        $ticketPriority = 'high';
        $ticketStatus = 'new';
        
        // Create a ticket that we will reference in the subsequent tickets.
        $ticket = new Ticket();
        $ticket->id = $ticketId1;
        $ticket->project_id = $ticketProjectId;
        $ticket->ticket_type = $ticketTicketType;
        $ticket->summary = $ticketSummary;
        $ticket->description = $ticketDescription;
        $ticket->created_by_user_id = $ticketCreatedByUserId;
        $ticket->priority = $ticketPriority;
        $ticket->status = $ticketStatus;
        $this->assertTrue($ticket->isValid(), 'Ticket should be valid');
        $ticket->save();
        
        // Try to create a relationship between the first ticket and itself.
        // @TODO: Note, this test will fail because the check is not in the model yet.
        $sameRel = new TicketAssociation();
        $sameRel->relationship_type = 'associated';
        $sameRel->src_ticket_id = $ticket->id;
        $sameRel->src_sort_order = 1;
        $sameRel->trg_ticket_id = $ticket->id;
        $sameRel->trg_sort_order = 1;
        $sameRel->pinned = false;
        $sameRel->save();
        $this->assertNull($sameRel->id, 'Should not be able to create a relationship between a ticket and itself');
        $sameRel->delete();
        
        // Loop through all the different relationships and create a new ticket for each,
        // referencing our base ticket in the description with the appropriate relationship.
        // Then check that the right relationship was created.
        $relationships = ['~', '+', '-', '>', '<'];
        $relNames = [0 => 'associated', 1 => 'parent', 2 => 'child', 3 => 'dependency', 4 => 'dependent'];
        foreach($relationships as $relIndex => $relationship) {
            // Create the ticket.
            $ticket = new Ticket();
            $ticket->project_id = $ticketProjectId;
            $ticket->ticket_type = $ticketTicketType;
            $ticket->summary = 'Referencing ticket';
            $ticket->description = 'Referencing ' . $relationship . '#1 in the description';
            $ticket->created_by_user_id = $ticketCreatedByUserId;
            $ticket->priority = $ticketPriority;
            $ticket->status = $ticketStatus;
            $this->assertTrue($ticket->isValid(), 'Ticket should be valid');
            $ticket->save();
            // Read all the associations. Check there is exactly one, that the ticket ids
            // and the relationship types are correct.
            $assocRows = TicketAssociation::all();
            $this->assertEquals(count($assocRows), 1, 'Expecting 1 association');
            $assocRow = $assocRows[0];
            $this->assertEquals($assocRow->relationship_type, $relNames[$relIndex], 'Incorrect relationship type');
            $this->assertEquals($assocRow->src_ticket_id, $ticket->id, 'Incorrect source id');
            $this->assertEquals($assocRow->src_sort_order, 1, 'Incorrect source sort order');
            $this->assertEquals($assocRow->trg_ticket_id, 1, 'Incorrect target id');
            $this->assertEquals($assocRow->trg_sort_order, 1, 'Incorrect target sort order');
            $this->assertEquals($assocRow->pinned, 0, 'Pinned should be zero');
            // Delete the association and ticket we just created.
            $assocRow->delete();
            $ticket->delete();
        }
        
        // The first ticket is still there. Create 2 more, manually adding a relationship
        // from the first to each one. Then do various tests.
        for($relIndex = 0 ; $relIndex < 2 ; $relIndex++) {
            // Ticket
            $ticket = new Ticket();
            $ticket->id = 1 + $ticketId1 + $relIndex;
            $ticket->project_id = $ticketProjectId;
            $ticket->ticket_type = $ticketTicketType;
            $ticket->summary = 'Referencing ticket' . (1 + $relIndex);
            $ticket->description = 'Manual referencing';
            $ticket->created_by_user_id = $ticketCreatedByUserId;
            $ticket->priority = $ticketPriority;
            $ticket->status = $ticketStatus;
            $this->assertTrue($ticket->isValid(), 'Ticket should be valid');
            $ticket->save();
            // Relationship.
            $success = TicketAssociation::createRecord('+', 1, $ticket->id, $relIndex == 1);
            $this->assertTrue($success, 'Relationship should have been created');
        }
        // Now do some tests on the relationships.
        $assocRows = $this->checkAssocData($ticketId1);
        // Move the first one down and check all the numbers are correct.
        $assocRows[0]->handleMove($ticketId1, 'down');
        $assocRows = TicketAssociation::orderBy('src_sort_order', 'DESC')->get();
        $this->assertEquals(count($assocRows), 2, 'Expected 2 relationship records');
        foreach($assocRows as $assocIndex => $assocRow) {
            $this->assertEquals($assocRow->relationship_type, 'parent', 'After move down, should be parent');
            $this->assertEquals($assocRow->src_ticket_id, $ticketId1, 'After move down, incorrect source ticket id');
            $this->assertEquals($assocRow->src_sort_order, 2 - $assocIndex, 'After move down, incorrect source sort order');
            $this->assertEquals($assocRow->trg_ticket_id, 2 + $assocIndex, 'After move down, incorrect target ticket id');
            $this->assertEquals($assocRow->trg_sort_order, 1, 'After move down, target sort order is not 1');
            $this->assertEquals($assocRow->pinned, $assocIndex, 'After move down, incorrect value for pinned');
        }
        // Move the first one back up and check all the numbers are back to how they were.
        $assocRows[0]->handleMove($ticketId1, 'up');
        $assocRows = $this->checkAssocData($ticketId1);
        // Delete the first one and check the sortorder on the 2nd one is updated.
        $assocRows[0]->delete();
        $assocRows = TicketAssociation::orderBy('src_sort_order', 'DESC')->get();
        $this->assertEquals(count($assocRows), 1, 'Expected 1 relationship record after delete');
        $this->assertEquals($assocRows[0]->src_sort_order, 1, 'After delete, expected source sort order to be 1');
        
        // @TODO: Need to check all this for the trg sort order!
        
        // Okay, we have 1 relationship in the DB -> 1 parent of 3...
        // Try to add 3 as a parent of 1.
        $success = TicketAssociation::createRecord('+', $assocRows[0]->trg_ticket_id, $assocRows[0]->src_ticket_id);
        $this->assertFalse($success, 'Should not be able to create dependency loop');
        
        // Create a closed ticket. Then try to make it a parent of the first ticket.
        $ticket = new Ticket();
        $ticket->project_id = $ticketProjectId;
        $ticket->ticket_type = $ticketTicketType;
        $ticket->summary = 'Closed parent';
        $ticket->description = 'Should not be able to make it parent of ticket 1';
        $ticket->created_by_user_id = $ticketCreatedByUserId;
        $ticket->priority = $ticketPriority;
        $ticket->status = 'closed';
        $this->assertTrue($ticket->isValid(), 'Ticket should be valid');
        $ticket->save();
        $success = TicketAssociation::createRecord('-', $ticket->id, $assocRows[0]->src_ticket_id);
        $this->assertFalse($success, 'Should not be able to make closed ticket a parent of open one');
        
        // @TODO: Should handle pinning.
        
   }

}
