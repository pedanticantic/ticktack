<?php

require_once 'ClearDown.php';

class TimeSpentTest extends TestCase {
    
    /**
     * @var int $project
     */
    private $project;
    
    /**
     * @var Ticket[] $tickets
     */
    private $tickets;
    
    /**
     * Various values we're going to use.
     * @var int $invalidTicketId
     * @var int $validUserId
     * @var int $invalidUserId
     * @var string $validStartDatetime
     * @var string $invalidStartDatetime
     * @var string $validDuration
     * @var string $invalidDuration
     * @var string $validNotes
     * @var string $invalidNotes
     */
    protected $invalidTicketId = 1000;
    protected $validUserId = 1;
    protected $invalidUserId = 1000;
    protected $validStartDatetime = '25/07/2015 17:18';
    protected $invalidStartDatetime = '35/07/2015 17:18';
    protected $validDuration = '3h 45m';
    protected $invalidDuration = 'bang!';
    protected $validNotes = 'Valid notes';
    protected $invalidNotes = 'TBA';
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        $this->invalidNotes = str_pad(' long', 251, 'very, ', STR_PAD_LEFT);
        
        // We need a project to put our tickets against.
        $this->project = new Project;
        $this->project->id = 1;
        $this->project->name = 'For Time Spent';
        $this->project->description = 'For Time Spent';
        $this->project->lead_user_id = 1;
        $this->project->save();
        
        // And three tickets to put the time spent on.
        for($i = 1 ; $i <= 3 ; $i++) {
            $ticket = new Ticket();
            $ticket->project_id = $this->project->id;
            $ticket->ticket_type = 'task';
            $ticket->summary = 'Testing time spent - ' . $i;
            $ticket->description = $ticket->summary;
            $ticket->created_by_user_id = 1;
            $ticket->priority = 'high';
            $ticket->status = 'new';
            $ticket->estimated_length_seconds = '1d';
            $this->assertTrue($ticket->isValid(), 'Ticket for time spent should be valid');
            $ticket->save();
            $this->tickets[] = $ticket;
        }
        
        // Make ticket 1 a parent of ticket 2.
        $success = TicketAssociation::createRecord('-', $this->tickets[0]->id, $this->tickets[1]->id, false);
        $this->assertTrue($success, 'Parent relationship should have been created');
        // Make ticket 1 dependant on ticket 3.
        $success = TicketAssociation::createRecord('>', $this->tickets[0]->id, $this->tickets[2]->id, false);
        $this->assertTrue($success, 'Dependent relationship should have been created');
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Method to test recording time spent on a ticket.
     * This does various tests on a single ticket.
     */
    public function testTimeSpent() {
        // Do one that should work.
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, $this->validStartDatetime, $this->validDuration, $this->validNotes);
        $this->assertTrue($success, 'Valid time recording failed');
        
        // Test null values.
        $success = TimeSpent::attemptToCreate(null, $this->validUserId, $this->validStartDatetime, $this->validDuration, $this->validNotes);
        $this->assertEquals($success, [0 => 'The ticket id field is required.'], 'Null ticket id should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, null, $this->validStartDatetime, $this->validDuration, $this->validNotes);
        $this->assertEquals($success, [0 => 'The user id field is required.'], 'Null user id should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, null, $this->validDuration, $this->validNotes);
        $this->assertEquals($success, 'Start Date must be a valid date', 'Null start time should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, $this->validStartDatetime, null, $this->validNotes);
        $this->assertEquals($success, [0 => 'The duration field is required.'], 'Null duration should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, $this->validStartDatetime, $this->validDuration, null);
        $this->assertEquals($success, [0 => 'The notes field is required.'], 'Null notes should have failed');
        
        // Test invalid values.
        $success = TimeSpent::attemptToCreate($this->invalidTicketId, $this->validUserId, $this->validStartDatetime, $this->validDuration, $this->validNotes);
        $this->assertEquals($success, 'Parent ticket does not exist.', 'Invalid ticket id should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->invalidUserId, $this->validStartDatetime, $this->validDuration, $this->validNotes);
        $this->assertEquals($success, 'Parent user does not exist.', 'Invalid user id should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, $this->invalidStartDatetime, $this->validDuration, $this->validNotes);
        $this->assertEquals($success, 'Start Date must be a valid date', 'Invalid start time should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, $this->validStartDatetime, $this->invalidDuration, $this->validNotes);
        $this->assertEquals($success, 'Duration must be a valid interval, eg 2h30m', 'Invalid duration should have failed');
        $success = TimeSpent::attemptToCreate($this->tickets[0]->id, $this->validUserId, $this->validStartDatetime, $this->validDuration, $this->invalidNotes);
        $this->assertEquals($success, [0 => 'The notes may not be greater than 250 characters.'], 'Invalid notes should have failed');
    }
    
    /**
     * Method to test that summing the time recorded across tickets works.
     */
    public function testTimeSpentTotals() {
        // We'll record different times against three tickets, then ask each one for
        // its totals. Remember ticket 1 is a parent of ticket 2; ticket 1 is dependent on ticket 3.
        $durations = ['2h30m', '45m', '7h5m'];
        foreach($this->tickets as $index => $ticket) {
            $success = TimeSpent::attemptToCreate($ticket->id, $this->validUserId, $this->validStartDatetime, $durations[$index], $this->validNotes);
            $this->assertTrue($success, 'Setting up time across tickets failed');
        }
        $expected = [
            [
                // Ticket 2 is a child; ticket 3 is not.
                "total_spent" => 9000,
                "total_spent_inc_children" => 11700,
                "total_remaining" => 77400,
                "total_remaining_inc_children" => 74700
            ],
            [
                // Has no children.
                "total_spent" => 2700,
                "total_spent_inc_children" => 2700,
                "total_remaining" => 83700,
                "total_remaining_inc_children" => 83700
            ],
            [
                // Has no children.
                "total_spent" => 25500,
                "total_spent_inc_children" => 25500,
                "total_remaining" => 60900,
                "total_remaining_inc_children" => 60900
            ]
        ];
        foreach($this->tickets as $index => $ticket) {
            $totals = $ticket->getTimeSpentTotals();
            $this->assertEquals(count($totals), 4, 'Didn\'t get 4 totals back.');
            foreach($totals as $totalType => $totalValue) {
                $this->assertEquals($totalValue, $expected[$index][$totalType], 'Incorrect time spent total: ' . $index . ': actual=' . $totalValue . '; expected=' . $expected[$index][$totalType]);
            }
        }
    }
    
}
