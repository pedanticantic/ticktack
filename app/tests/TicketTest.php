<?php

require_once 'ClearDown.php';

class TicketTest extends TestCase {
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        
        // We need a project to put our tickets against.
        $project = new Project;
        $project->id = 1;
        $project->name = 'For Tickets';
        $project->description = 'For Tickets';
        $project->lead_user_id = 1;
        $project->save();
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test the basic ticket functionality - creating a model.
     *
     * @return void
     */
    public function testCreate()
    {
        // Try creating tickets with missing mandatory fields.
        $ticketId1 = 1;
        $ticketId2 = 2;
        $ticketProjectId = 1; // See setUp above
        $ticketTicketType = 'task';
        $ticketSummary = 'For unit testing';
        $ticketDescription = 'For unit testing';
        $ticketCreatedByUserId = 1;
        $ticketPriority = 'high';
        $ticketStatus = 'new';
        
        // We do it in a loop. On the nth iteration, we leave out the nth field.
        for($fieldIndex = 0 ; $fieldIndex < 6 ; $fieldIndex++) {
            $ticket = new Ticket();
            $ticket->id = $ticketId1;
            if ($fieldIndex != 0) {
                $ticket->project_id = $ticketProjectId;
            }
            if ($fieldIndex != 1) {
                $ticket->ticket_type = $ticketTicketType;
            }
            if ($fieldIndex != 2) {
                $ticket->summary = $ticketSummary;
            }
            if ($fieldIndex != 3) {
                $ticket->description = $ticketDescription;
            }
            if ($fieldIndex != 4) {
                $ticket->created_by_user_id = $ticketCreatedByUserId;
            }
            if ($fieldIndex != 5) {
                $ticket->priority = $ticketPriority;
            }
            if ($fieldIndex != 6) {
                $ticket->status = $ticketStatus;
            }
            $this->assertFalse($ticket->isValid(), 'Null field was allowed (' . $fieldIndex . ')');
        }
        
        // Check the due datetime.
        $ticket = new Ticket();
        $ticket->id = $ticketId1;
        $ticket->project_id = $ticketProjectId;
        $ticket->ticket_type = $ticketTicketType;
        $ticket->summary = $ticketSummary;
        $ticket->description = $ticketDescription;
        $ticket->created_by_user_id = $ticketCreatedByUserId;
        $ticket->priority = $ticketPriority;
        $ticket->status = $ticketStatus;
        $ticket->due_datetime = '2015-13-12 10:10:10'; // Month is not valid.
        $this->assertFalse($ticket->isValid(), 'Due datetime must be of the form YYYY-MM-DD HH24:MI:SS');
        
        // Check the summary length.
        $ticket = new Ticket();
        $ticket->id = $ticketId1;
        $ticket->project_id = $ticketProjectId;
        $ticket->ticket_type = $ticketTicketType;
        $ticket->description = $ticketDescription;
        $ticket->created_by_user_id = $ticketCreatedByUserId;
        $ticket->priority = $ticketPriority;
        $ticket->status = $ticketStatus;
        $ticket->summary = 'aaaa';
        $this->assertFalse($ticket->isValid(), 'Summary should be too short');
        $ticket->summary = '123456789012345678901234567890123456789012345678901234567890a';
        $this->assertFalse($ticket->isValid(), 'Summary should be too long');
        
        // Check for duplicate summaries.
        $ticket = new Ticket();
        $ticket->id = $ticketId1;
        $ticket->project_id = $ticketProjectId;
        $ticket->ticket_type = $ticketTicketType;
        $ticket->summary = $ticketSummary;
        $ticket->description = $ticketDescription;
        $ticket->created_by_user_id = $ticketCreatedByUserId;
        $ticket->priority = $ticketPriority;
        $ticket->status = $ticketStatus;
        $ticket->estimated_length_seconds = '1h 5m';
        $this->assertTrue($ticket->isValid(), 'Ticket should be valid');
        $ticket->save();
        $ticket = new Ticket();
        $ticket->id = $ticketId2;
        $ticket->project_id = $ticketProjectId;
        $ticket->ticket_type = $ticketTicketType;
        $ticket->summary = $ticketSummary;
        $ticket->description = $ticketDescription;
        $ticket->created_by_user_id = $ticketCreatedByUserId;
        $ticket->priority = $ticketPriority;
        $ticket->status = $ticketStatus;
        $this->assertFalse($ticket->isValid(), 'Ticket has a duplicate summary');
        
    }
   
}
