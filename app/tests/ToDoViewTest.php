<?php

require_once 'ClearDown.php';

class ToDoViewTest extends TestCase {
    
    /**
     * var int $project
     */
    private $project;
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        
        // We need a project to put our ticket against.
        $this->project = new Project;
        $this->project->id = 1;
        $this->project->name = 'For Tickets';
        $this->project->description = 'For Tickets';
        $this->project->lead_user_id = 1;
        $this->project->save();
        
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test insert and delete.
     *
     * @return void
     */
    public function testCRUD()
    {
        // Check mandatory values on insert.
        // Iterate through the mandatory columns. On the nth loop, leave the nth field blank.
        for($tdvIndex = 0 ; $tdvIndex < 2 ; $tdvIndex++) {
            $toDoView = new ToDoView();
            if ($tdvIndex != 0) {
                $toDoView->owner_user_id = 1;
            }
            if ($tdvIndex != 1) {
                $toDoView->view_name = 'Test ToDoView';
            }
            $this->assertFalse($toDoView->isValid(), 'ToDoView should not be valid');
        }
        
        // Check view name length.
        $toDoView = new ToDoView();
        $toDoView->owner_user_id = 1;
        $toDoView->view_name = '1234';
        $this->assertFalse($toDoView->isValid(), 'ToDoView name should be too short');
        $toDoView->view_name = '123456789012345678901234567890123456789012345678901234567890a';
        $this->assertFalse($toDoView->isValid(), 'ToDoView name should be too long');
        $toDoView->view_name = 'Just right';
        $this->assertTrue($toDoView->isValid(), 'ToDoView name should be valid');
    }
    
    /**
     * Test updating the position.
     *
     * @return void
     */
    public function testUpdateViewportPosition()
    {
        // Create a To Do View.
        $toDoView = new ToDoView();
        $toDoView->owner_user_id = 1;
        $toDoView->view_name = 'Test ToDoView';
        $this->assertTrue($toDoView->isValid(), 'ToDoView should be valid');
        $toDoView->save();
        
        // Move it to a random position, and make sure the DB is updated.
        $new_x = rand(5, 50);
        $new_y = rand(5, 50);
        ToDoView::updateViewportPosition($toDoView->id, $new_x, $new_y);
        $toDoView2 = ToDoView::find($toDoView->id);
        $this->assertEquals($toDoView2->viewport_x, $new_x, 'Viewport x is invalid');
        $this->assertEquals($toDoView2->viewport_y, $new_y, 'Viewport y is invalid');
        
    }
    
}
