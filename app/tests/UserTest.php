<?php

require_once 'ClearDown.php';

class UserTest extends TestCase {
    
    /**
     * Some valid and invalid values...
     * @var string $validUser
     * @var string $validPassword
     * @var string $shortUser
     * @var string $shortPassword
     * @var string $longUser
     * @var string $longPassword
     * @var string $invalidCharsUser
     * @var string $invalidCharsPassword
     * @var string $mismatchPassword
     */
    protected $validUser = 'a123456';
    protected $validPassword = 'b12345678';
    protected $shortUser = 'a1234';
    protected $shortPassword = 'b123456';
    protected $longUser = 'a123456789012345678901234567890';
    protected $longPassword = 'b123456789012345678901234567890';
    protected $invalidCharsUser = 'a123456!';
    protected $invalidCharsPassword = 'b12345678#';
    protected $mismatchPassword = 'b87654321';
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test registering a user.
     *
     * @return void
     */
    public function testRegister()
    {
        $success = User::attemptToRegister($this->validUser, $this->validPassword, $this->validPassword);
        $this->assertTrue($success, 'Basic registration failed');
        
        $success = User::attemptToRegister($this->shortUser, $this->validPassword, $this->validPassword);
        $this->assertFalse($success, 'Short username should not be valid');
        $success = User::attemptToRegister($this->validUser, $this->shortPassword, $this->shortPassword);
        $this->assertFalse($success, 'Short password should not be valid');
        
        $success = User::attemptToRegister($this->longUser, $this->validPassword, $this->validPassword);
        $this->assertFalse($success, 'Long username should not be valid');
        $success = User::attemptToRegister($this->validUser, $this->longPassword, $this->longPassword);
        $this->assertFalse($success, 'Long password should not be valid');
        
        $success = User::attemptToRegister($this->invalidCharsUser, $this->validPassword, $this->validPassword);
        $this->assertFalse($success, 'Invalid chars in username should not be valid');
        $success = User::attemptToRegister($this->validUser, $this->invalidCharsPassword, $this->invalidCharsPassword);
        $this->assertFalse($success, 'Invalid chars in password should not be valid');
        
        $success = User::attemptToRegister($this->validUser, $this->validPassword, $this->mismatchPassword);
        $this->assertFalse($success, 'Mismatching passwords should not be valid');
        
        $success = User::attemptToRegister($this->validUser, $this->validPassword, $this->validPassword);
        $this->assertFalse($success, 'Duplicate registration should not be valid');
        
    }
    
}
