<?php

require_once 'ClearDown.php';

class AssignmentTest extends TestCase {
    
    /**
     * var int $testTicketId
     */
    private $testTicketId;
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        
        // We need a project to put our ticket against.
        $project = new Project;
        $project->id = 1;
        $project->name = 'For Tickets';
        $project->description = 'For Tickets';
        $project->lead_user_id = 1;
        $project->save();
        
        // Then we need a ticket to put our assignments against.
        $ticketId1 = 1;
        $ticketProjectId = 1; // See setUp above
        $ticketTicketType = 'task';
        $ticketSummary = 'Base ticket';
        $ticketDescription = 'For testing related tickets';
        $ticketCreatedByUserId = 1;
        $ticketPriority = 'high';
        $ticketStatus = 'new';
        
        // Create a ticket that we will reference in the subsequent tickets.
        $ticket = new Ticket();
        $ticket->project_id = $project->id;
        $ticket->ticket_type = 'task';
        $ticket->summary = 'For assignment tests';
        $ticket->description = 'For assignment tests';
        $ticket->created_by_user_id = 1;
        $ticket->priority = 'high';
        $ticket->status = 'new';
        $this->assertTrue($ticket->isValid(), 'Ticket should be valid');
        $ticket->save();
        $this->testTicketId = $ticket->id;
        
        // And we need a couple more users.
        $user = new User();
        $user->id = 2;
        $user->username = 'First Assignee';
        $user->password = 'first';
        $user->save();
        $user = new User();
        $user->id = 3;
        $user->username = 'Second Assignee';
        $user->password = 'second';
        $user->save();
        
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test insert, move and delete.
     *
     * @return void
     */
    public function testCRUD()
    {
        // Assign 2 users.
        $assignmentId = TicketAssignment::assignUserToTicket($this->testTicketId, 1);
        $this->assertNotNull($assignmentId, 'Failed to create ticket assignment');
        $assignmentId = TicketAssignment::assignUserToTicket($this->testTicketId, 2);
        $this->assertNotNull($assignmentId, 'Failed to create ticket assignment');
        
        // Check the sort orders.
        $assignments = TicketAssignment::where('ticket_id', $this->testTicketId)
            ->orderBy('sort_order')
            ->get();
        foreach($assignments as $assignIndex => $assignment) {
            $this->assertEquals($assignment->user_id, 1 + $assignIndex, 'Incorrect assignment user id');
            $this->assertEquals($assignment->sort_order, 1 + $assignIndex, 'Incorrect assignment sort order');
        }
        
        // Assign a 3rd user, but place them in 2nd place.
        $assignmentId = TicketAssignment::assignUserToTicket($this->testTicketId, 3, 2);
        $this->assertNotNull($assignmentId, 'Failed to create ticket assignment');
        // Check the users & sort orders.
        $uidsShouldBe = [1, 3, 2];
        $assignments = TicketAssignment::where('ticket_id', $this->testTicketId)
            ->orderBy('sort_order')
            ->get();
        foreach($assignments as $assignIndex => $assignment) {
            $this->assertEquals($assignment->user_id, $uidsShouldBe[$assignIndex], 'Incorrect assignment user id after 3rd');
            $this->assertEquals($assignment->sort_order, 1 + $assignIndex, 'Incorrect assignment sort order after 3rd');
        }
        
        // Move the 2nd one up, then the old first one down, and the users (in sort order)
        // should be 3, 2, 1, which makes it easy to test.
        $result = TicketAssignment::moveAssignment($assignments[1]->id, 'up');
        $this->assertEquals($result['code'], 0, 'First movement failed');
        $result = TicketAssignment::moveAssignment($assignments[0]->id, 'down');
        $this->assertEquals($result['code'], 0, 'Second movement failed');
        // And test what we have.
        $assignments = TicketAssignment::where('ticket_id', $this->testTicketId)
            ->orderBy('sort_order')
            ->get();
        foreach($assignments as $assignIndex => $assignment) {
            $this->assertEquals($assignment->user_id, 3 - $assignIndex, 'Incorrect assignment user id after moves');
            $this->assertEquals($assignment->sort_order, 1 + $assignIndex, 'Incorrect assignment sort order after moves');
        }
        
        // Attempt to move the first one up and the last one down. Check they both fail.
        $result = TicketAssignment::moveAssignment($assignments[0]->id, 'up');
        $this->assertEquals($result['code'], 3, 'Move up from top did not fail');
        $result = TicketAssignment::moveAssignment($assignments[2]->id, 'down');
        $this->assertEquals($result['code'], 3, 'Move down from bottom did not fail');
        
        // Just check the direction validation
        $result = TicketAssignment::moveAssignment($assignments[0]->id, 'sideways');
        $this->assertEquals($result['code'], 2, 'Move sideways did not fail');
        
        // Check that deleting the middle one updates the sort order correctly.
        $ticketId = TicketAssignment::remove($assignments[1]->id);
        $this->assertEquals($ticketId, $this->testTicketId, 'Remove assignment failed');
        $assignments = TicketAssignment::where('ticket_id', $this->testTicketId)
            ->orderBy('sort_order')
            ->get();
        $this->assertEquals($assignments[0]->user_id, 3, 'Incorrect first user id after delete');
        $this->assertEquals($assignments[0]->sort_order, 1, 'Incorrect first sort order after delete');
        $this->assertEquals($assignments[1]->user_id, 1, 'Incorrect second user id after delete');
        $this->assertEquals($assignments[1]->sort_order, 2, 'Incorrect second sort order after delete');
        
    }
    
    /**
     * Test that we can't assign the same user twice.
     *
     * @return void
     */
    public function testDuplicate()
    {
        // Assign 2 users.
        $assignmentId = TicketAssignment::assignUserToTicket($this->testTicketId, 1);
        $this->assertNotNull($assignmentId, 'Failed to create ticket assignment');
        $assignmentId = TicketAssignment::assignUserToTicket($this->testTicketId, 2);
        $this->assertNotNull($assignmentId, 'Failed to create ticket assignment');
        $assignmentId = TicketAssignment::assignUserToTicket($this->testTicketId, 2);
        $this->assertNull($assignmentId, 'Duplicate assignment should not be allowed');
        
    }
    
}
