<?php

require_once 'ClearDown.php';

class ToDoViewTicketTest extends TestCase {
    
    /**
     * var int $project
     */
    private $project;
    
    /**
     * var int $toDoView
     */
    private $toDoView;
    
    /**
     * Set everything up.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        ClearDown::run();
        
        // We need a project to put our ticket against.
        $this->project = new Project;
        $this->project->id = 1;
        $this->project->name = 'For Tickets';
        $this->project->description = 'For Tickets';
        $this->project->lead_user_id = 1;
        $this->project->save();
        
        // Create a To Do View.
        $this->toDoView = new ToDoView();
        $this->toDoView->owner_user_id = 1;
        $this->toDoView->view_name = 'Test for adding';
        $this->toDoView->save();
        
    }
    
    /**
     * Tidy everything up.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        ClearDown::run();
    }
    
    /**
     * Test adding tickets to a view.
     * Then test removing them.
     *
     * @return void
     */
    public function testAddAndRemove()
    {
        
        // Create four tickets.
        $viewTicketIds = [];
        for($tIndex = 0 ; $tIndex < 4 ; $tIndex++) {
            
            $ticket = new Ticket();
            $ticket->project_id = $this->project->id;
            $ticket->ticket_type = 'task';
            $ticket->summary = 'Add test' . $tIndex;
            $ticket->description = $ticket->summary;
            $ticket->created_by_user_id = 1;
            $ticket->priority = 'high';
            $ticket->status = 'new';
            $this->assertTrue($ticket->isValid(), 'This To Do View ticket should be valid');
            $ticket->save();
            $viewTicketIds[$tIndex] = $ticket->id;
            
            // We also test that it sets the view-specific summary.
            $viewSummaryPrefix = 'Summary for view';
            $addedTicket = ToDoViewsTicket::addLink($this->toDoView->id, $ticket->id, null, $viewSummaryPrefix . $tIndex);
            $this->assertNotNull($addedTicket, 'Adding a ticket to a view should have worked');
            
        }
        
        // Now ask the view for its contents. We can then loop through and test the actual
        // positions it has chosen.
        $allTickets = ToDoViewsTicket::getContentsOfView($this->toDoView->id);
        $this->assertEquals(count($allTickets['tickets']), 4, 'Incorrect number of tickets returned');
        $expected = [
            [0, 0],
            [14, 0],
            [0, 18],
            [28, 0]
        ];
        foreach($allTickets['tickets'] as $ticketIndex => $ticketInfo) {
            $this->assertEquals($ticketInfo['x_pos'], $expected[$ticketIndex][0], 'Incorrect x position');
            $this->assertEquals($ticketInfo['y_pos'], $expected[$ticketIndex][1], 'Incorrect y position');
            $this->assertEquals($ticketInfo['view_summary'], $viewSummaryPrefix . $ticketIndex, 'View summary does not match');
        }
        
        // Move the third ticket up, and make sure all the other tickets are moved to the right.
        // This tests both the update and the scoot methods.
        $result = ToDoViewsTicket::updatePosition($this->toDoView->id, $viewTicketIds[2], 0, 5);
        $this->assertTrue($result['success'], 'Update of ticket position failed');
        $allTickets = ToDoViewsTicket::getContentsOfView($this->toDoView->id);
        $expected = [
            [14, 0],
            [28, 0],
            [0, 5],
            [42, 0],
        ];
        foreach($allTickets['tickets'] as $ticketIndex => $ticketInfo) {
            $this->assertEquals($ticketInfo['x_pos'], $expected[$ticketIndex][0], 'Incorrect x position after update');
            $this->assertEquals($ticketInfo['y_pos'], $expected[$ticketIndex][1], 'Incorrect y position after update');
        }
        
        // Now remove the third ticket and make sure the view says there are 3 left.
        ToDoViewsTicket::removeTicket($this->toDoView->id, $viewTicketIds[2]);
        $allTickets = ToDoViewsTicket::getContentsOfView($this->toDoView->id);
        $this->assertEquals(count($allTickets['tickets']), 3, 'Should be 3 tickets after delete from view');
        
        // Add another ticket, but say it's relative to the 2nd one remaining.
        $ticket = new Ticket();
        $ticket->project_id = $this->project->id;
        $ticket->ticket_type = 'task';
        $ticket->summary = 'Add test' . $tIndex;
        $ticket->description = $ticket->summary;
        $ticket->created_by_user_id = 1;
        $ticket->priority = 'high';
        $ticket->status = 'new';
        $this->assertTrue($ticket->isValid(), 'This To Do View ticket should be valid (2)');
        $ticket->save();
        $addedTicket = ToDoViewsTicket::addLink($this->toDoView->id, $ticket->id, $viewTicketIds[1]);
        $this->assertNotNull($addedTicket, 'Adding a ticket to a view should have worked');
        // Now get all the positions, and check that our new ticket has been positioned at the right place.
        $allTickets = ToDoViewsTicket::getContentsOfView($this->toDoView->id);
        foreach($allTickets['tickets'] as $ticketIndex => $ticketInfo) {
            if ($ticketInfo['id'] == $ticket->id) {
                $this->assertEquals($ticketInfo['x_pos'], 28, 'Incorrect x position after add relative');
                $this->assertEquals($ticketInfo['y_pos'], 18, 'Incorrect y position after add relative');
            }
        }
        
    }
    
    /**
     * Test updating a ticket's description on a view.
     *
     * @return void
     */
    public function testSettingSummary()
    {
        
        // Create a ticket.
        $ticket = new Ticket();
        $ticket->project_id = $this->project->id;
        $ticket->ticket_type = 'task';
        $ticket->summary = 'This is the default summary';
        $ticket->description = $ticket->summary;
        $ticket->created_by_user_id = 1;
        $ticket->priority = 'high';
        $ticket->status = 'new';
        $this->assertTrue($ticket->isValid(), 'This To Do View ticket should be valid');
        $ticket->save();
        // Add it to the view.
        ToDoViewsTicket::addLink($this->toDoView->id, $ticket->id);
        
        // Check that setting the summary works.
        $success = ToDoViewsTicket::setTicketSummary($this->toDoView->id, $ticket->id, 'Test summary');
        $this->assertTrue($success['success'], 'Failed to set ticket summary');
        $updatedTicket = ToDoViewsTicket::getContentsOfViewFiltered($this->toDoView->id, $ticket->id);
        $this->assertEquals($updatedTicket['tickets'][0]['view_summary'], 'Test summary', 'Ticket summary was not set correctly');
        // And check that blanking it out works.
        $success = ToDoViewsTicket::setTicketSummary($this->toDoView->id, $ticket->id, '');
        $this->assertTrue($success['success'], 'Failed to clear ticket summary');
        $updatedTicket = ToDoViewsTicket::getContentsOfViewFiltered($this->toDoView->id, $ticket->id);
        $this->assertEquals($updatedTicket['tickets'][0]['view_summary'], '', 'Ticket summary was not cleared correctly');
        
    }
    
    // @TODO: Other tests we should do:
    // @TODO:   That getContentsOfView returns the right relationships
    
}
