<?php

/**
 * A class to represent my application.
 */
class TickTack {
    
    /**
      * The current version number.
      *
      * @var string
      */
    const VERSION = '0.12.3';
    
    /**
     * Method to return the current version number.
     *
     * @return string The current version number, as held in the class constant.
     */
    public static function getVersion() {
        return static::VERSION;
    }
    
}