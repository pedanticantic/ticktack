<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ToDoView extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * An array of error messages. They have come from the validator, or created "manually"
     * as an array of strings.
     *
     * @var array
     */
    private static $toDoViewErrors;
    
    /**
     * Set certain attributes when creating a model.
     *
     * @var array
     */
    protected $attributes = array(
        'viewport_x' => 0,
        'viewport_y' => 0
    );
    
    /**
     * Fundamental validation rules.
     *
     * @var arrays
     */
    public static $rules = array(
        'owner_user_id' => 'required|integer',
        'view_name' => 'required|min:5|max:60',
        'viewport_x' => 'required|integer',
        'viewport_y' => 'required|integer',
    );
        
    /**
     * Method to test whether the row is valid.
     *
     * @return boolean true if valid; false otherwise.
     */
    public function isValid()
    {
        // Clear any existing errors.
        static::$toDoViewErrors = [];
        // Make a new validator object, using the values in the model.
        $v = Validator::make($this->toArray(), static::$rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            foreach($v->messages()->all() as $message) {
                static::$toDoViewErrors[] = $message;
            }
            return false;
        }
        
        // Make sure the name is unique.
        $exists = static::where('view_name', '=', $this->view_name);
        if ($this->id != null) {
            $exists->where('id', '!=', $this->id);  // Ignore itself, if this is an existing record.
        }
        $exists = $exists->first();
        if (count($exists) > 0) {
            static::$toDoViewErrors = ['The name must be unique.'];
            return false;
        }
        return true;
        
    }
    
    /**
     * Method to return the last validation object.
     *
     * @return mixed The last validation object.
     */
    public static function getLastErrors() {
        return static::$toDoViewErrors;
    }
    
    /**
     * Method to return all the views for the logged-in user.
     *
     * @return array List of to_do_views records.
     */
    public static function getUserViews() {
        return static::where('owner_user_id', '=', Auth::id())->orderBy('id')->get();
    }
    
    /**
     * Method to return all the views for the logged-in user, left-joined to the given ticket.
     *
     * @return array List of to_do_views records.
     */
    public static function getUserViewsForTicket($ticketId) {
        $ticketId = (int) $ticketId;  // Just in case.
        return static::where('owner_user_id', '=', Auth::id())
            ->select('to_do_views.id', 'to_do_views.view_name', 'to_do_views_tickets.ticket_id')
            ->leftJoin('to_do_views_tickets', function($join) use ($ticketId) {
                $join->on('to_do_views.id', '=', 'to_do_views_tickets.to_do_view_id');
                $join->on('to_do_views_tickets.ticket_id', '=', DB::raw($ticketId));
            })
            ->orderBy('id')
            ->get();
    }
    
    /**
     * Method to display the given view.
     *
     * @param integer A view id.
     *
     * @return mixed false if the view is invalid; the view object if valid.
     */
    public static function getView($id) {
        $toDoView = static::find($id);
        if ($toDoView == null) {
            return false;
        }
        return $toDoView;
    }
    
    /**
     * Method to update the viewport position in the given view.
     * The new viewport co-ordinates are posted to the server.
     *
     * @param int $viewId View id.
     */
    public static function updateViewportPosition($viewId, $new_x_pos, $new_y_pos) {
        $toDoView = static::getView($viewId);
        if ($toDoView != null &&$new_x_pos != null && $new_y_pos != null) {
            $toDoView->viewport_x = $new_x_pos;
            $toDoView->viewport_y = $new_y_pos;
            $toDoView->save();
        }
    }
    
    /**
     * Method to "scoot" up any tickets that are "under" the given ticket.
     * It returns the ids of any tickets that had to be moved. It's called when the
     * user drags and drops a ticket on the to-do view - if they drop it on other
     * tickets, we need to move those tickets out of the way.
     * This method is recursive because scooting a ticket might place it on top of
     * another one.
     *
     * @param int $ticketId Ticket id.
     * @return array An array of ticket ids.
     */
    public function scoot($ticketId) {
        // Get the position of the given ticket.
        $thisTicket = $this->to_do_tickets()->where('ticket_id', '=', $ticketId)->first();
        $thisLeft = $thisTicket['x_position'];
        $thisTop = $thisTicket['y_position'];
        $width = 14;
        $height = 18;
        // Get all the tickets that overlap this one. We pick the right-most one up first.
        $overlaps = $this
            ->to_do_tickets()
            ->where('ticket_id', '!=', $ticketId)
            ->where('x_position', '<', $thisLeft + $width)
            ->where('x_position', '>', $thisLeft - $width)
            ->where('y_position', '<', $thisTop + $height)
            ->where('y_position', '>', $thisTop - $height)
            ->orderBy('x_position', 'DESC')
            ->orderBy('y_position')
            ->get();
        // Loop through them (remember, we pick up the rightmost one first), and move them so
        // they're just to the right of our ticket. We then call this routine recursively for
        // that ticket (in case we've now moved it on top of another one).
        // All the time, we remember all the ids & new x-positions of any ticket we've moved.
        $result = [];
        foreach($overlaps as $overlap) {
            $overlap->x_position = $thisLeft + $width;
            $overlap->save();
            $result[$overlap->ticket_id] = $overlap->x_position;
            $result = $this->scoot($overlap->ticket_id) + $result;
        }
        // Return a list of ids that we know we've moved.
        return $result;
    }
    
    public function to_do_tickets() {
        return $this->hasMany('ToDoViewsTicket');
    }
    
}