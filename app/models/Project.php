<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Project extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * Contains error messages when saving a project.
     *
     * @var array
     */
    static $projectErrors;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';
    
    /**
     * Fundamental validation rules.
     *
     * @var arrays
     */
    public static $rules = array(
        'name' => ['required', 'min:5', 'max:30', 'regex:/^[a-zA-Z0-9\- ]*$/'],
        'description' => 'required',
        'lead_user_id' => 'required'
    );
    public static $messages = array(
        'name.regex' => 'The name must only contain letters, numbers, spaces and hyphens.',
    );
    
    public function isValid()
    {
        // Clear any existing errors.
        static::$projectErrors = [];
        // make a new validator object, using the values in the model.
        $v = Validator::make($this->toArray(), static::$rules, static::$messages);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            foreach($v->messages()->all() as $message) {
                static::$projectErrors[] = $message;
            }
            return false;
        }
        
        // If the formats, etc are valid, check that the project doesn't already exist.
        $exists = Project::where('name', '=', $this->name);
        if ($this->id > 0) {
            // If we're saving an existing record, don't compare the name with itself.
            $exists = $exists->where('id', '<>', $this->id);
        }
        $exists = $exists->first();
        if (count($exists) > 0) {
            static::$projectErrors[] = 'This project name is already taken.';
            return false;
        }
        
        // Validation passed.
        return true;
    }
    
    /**
     * Method that returns a simple list of projects in the system.
     * @TODO: It needs to be paginated eventually.
     *
     * @return array List of project records.
     */
    public static function listPaginated() {
        return Project::orderBy('updated_at', 'DESC')->get();
    }
    
    /**
     * Method to return the errors from the last attempt to save a project.
     *
     * @return array The list of errors.
     */
    public static function getSaveErrors() {
        return static::$projectErrors;
    }
    
    /**
     * Method to return all the projects in the system.
     *
     * @return Project All projects.
     */
    public static function getAllProjects() {
        return Project::all();
    }
    
    /**
     * Method to return all the users for a project.
     * For now, it returns all users in the system.
     *
     * @return User All users.
     */
    public static function getAllUsers() {
        return User::all();
    }
    
    /**
     * Method to display the given project.
     *
     * @param integer A project id.
     *
     * @return mixed false if the project is invalid; the project object if valid.
     */
    public static function getProject($id) {
        $project = Project::find($id);
        if ($project == null) {
            return false;
        }
        return $project;
    }
    
    /**
     * Method to define the relationship between a project and the lead user id.
     */
    public function user() {
        return $this->belongsTo('User', 'lead_user_id');
    }
    
    /**
     * Method to define the relationship between a project and its tickets.
     */
    public function tickets() {
        return $this->hasMany('Ticket', 'project_id');
    }
    
    /**
     * Method to return the project description ready to be displayed (in view mode) on screen.
     * It invokes the markdown, etc.
     *
     * @return string The project description after having been parsed by the markdown parser.
     */
    public function descriptionForView() {
        return markdownHandler::toText($this->description);
    }
    
    /**
     * Method to return a count of tickets against this project, with each status. Each element
     * will be a link to the appropriate filter page.
     *
     * @return array Array of statuses, counts and links.
     */
    public function getTicketSummaryByStatus() {
        $counts = $this->tickets()
                    ->select(DB::raw("COUNT(0) AS ticket_count"), 'status')
                    ->orderBy('status')
                    ->groupBy('status')
                    ->get();
        $basicFilter = '?project_id=' . $this->id . '&';
        $summary = [];
        foreach($counts as $count) {
            $summary[ucfirst($count->status)] = [
                'count' => $count->ticket_count,
                'filter' => $basicFilter . 'status=' . str_replace(' ', '_', $count->status)
            ];
        }
        return $summary;
    }
    
    /**
     * Method to return a count of open tickets against this project, with each priority. Each
     * element will be a link to the appropriate filter page.
     *
     * @return array Array of priorities, counts and links.
     */
    public function getOpenTicketSummaryByPriority() {
        $counts = $this->tickets()
                    ->select(DB::raw("COUNT(0) AS ticket_count"), 'priority')
                    ->whereIn('status', ['new', 'assigned', 'in progress'])
                    ->orderBy('priority')
                    ->groupBy('priority')
                    ->get();
        $basicFilter = '?project_id=' . $this->id . '&status1=new&status2=assigned&status3=in_progress&';
        $summary = [];
        foreach($counts as $count) {
            $summary[ucfirst($count->priority)] = [
                'count' => $count->ticket_count,
                'filter' => $basicFilter . 'priority=' . str_replace(' ', '_', $count->priority)
            ];
        }
        return $summary;
    }
    
    /**
     * Method to return a count of open tickets against this project, assigned to the currently
     * logged in user, with the assignee type (primary, other, any). Each element will be a
     * link to the appropriate filter page.
     *
     * @return array Array of assignee types, counts and links.
     */
    public function getMyOpenTicketSummaryByAssignmentType() {
        $counts = $this->tickets()
                    ->join('tickets_assignments', 'tickets.id', '=', 'tickets_assignments.ticket_id')
                    ->select(DB::raw("tickets_assignments.sort_order = 1 AS is_primary"))
                    ->whereIn('status', ['new', 'assigned', 'in progress'])
                    ->where('tickets_assignments.user_id', '=', Auth::id())
                    ->get();
        $basicFilter = '?project_id=' . $this->id . '&assignee_id=' . Auth::id() . '&status1=new&status2=assigned&status3=in_progress&';
        $summary = [
            'Primary' => ['count' => 0, 'filter' => $basicFilter . 'assignee_scope=assignee_primary'],
            'Other' => ['count' => 0, 'filter' => $basicFilter . 'assignee_scope=assignee_other'],
            'Any' => ['count' => count($counts), 'filter' => $basicFilter]  // This last one doesn't need an assignee type filter.
        ];
        foreach($counts as $count) {
            $summary[$count->is_primary ? 'Primary' : 'Other']['count']++;
        }
        return $summary;
    }
    
    /**
     * Method to return this project in a suitable format for the calling process, as determined
     * by the mode. By default the project is converted to an array. If the mode is 'api', we add
     * the lead user's user name to the array.
     *
     * @param string $mode Defines the format that the calling process wants.
     *
     * @return array The project as an array, with any mode-specific modifications.
     */
    public function format($mode = null) {
        $result = $this->toArray();
        if ($mode == 'api') {
            $result['lead_user_username'] = $this->user->username;
            $result['description_markdown'] = $this->descriptionForView();
        }
        return $result;
    }
    
    /**
     * Method to return a set of projects for the API, after applying any user-defined filtering.
     * It uses the Input fields & values for the filter.
     *
     * @return Collection of Projects matching the filter (if any), sorted by id.
     */
    public static function apiFilter() {
        // Order the results by id, then implement any filters.
        $projects = Project::orderBy('id');
        if (Input::has('name')) {
            $projects = $projects->where('name', 'like', '%' . Input::get('name') . '%');
        }
        if (Input::has('description')) {
            $projects = $projects->where('description', 'like', '%' . Input::get('description') . '%');
        }
        if (Input::has('lead_user_id')) {
            $projects = $projects->where('lead_user_id', '=', Input::get('lead_user_id'));
        }
        $projects = $projects->get();
        
        return $projects;
    }
    
    public static function allowed($projectId, $operation) {
        if (App::environment('production') && $projectId == 2) {
            return false;
        }
        return true;
    }
    
}
