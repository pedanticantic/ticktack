<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
    
	use UserTrait, RemindableTrait;
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
    
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
    
    /**
     * The REGEX for the username.
     */
    const USERNAMEREGEX = '/^[a-zA-Z0-9]*$/';
    
    /**
     * The REGEX for the password.
     */
    const PASSWORDREGEX = '/^[a-zA-Z0-9]*$/';
    
    /**
     * An array of registration error messages.
     *
     * @var array
     */
    private static $registrationErrors;
    
    /**
     * Method that validates registration fields. If valid, it creates the user
     * record, logs the user in, and returns true. Returns false otherwise.
     * Error messages can be retrieved via User::getRegistrationErrors
     *
     * @param $username string The user's name.
     * @param $password string The user's password.
     * @param $confirm string Re-entry of the user's password.
     *
     * @return boolean Whether registration was successful.
     */
    public static function attemptToRegister($username, $password, $confirm) {
        
        // Validate them. All fields must be present, contain valid characters,
        // have a minimum length, etc.
        // @TODO: The password validation should be more sophisticated, eg at least 1 lower, at least 1 upper, at least one digit.
        static::$registrationErrors = [];
        if ($username == '' || $password == '' || $confirm == '') {
            static::$registrationErrors[] = 'All fields must be entered';
        } else {
            if (strlen($username) < 6 || strlen($username) > 30) {
                static::$registrationErrors[] = 'Username must be between 6 and 30 characters long';
            }
            if (strlen($password) < 8 || strlen($password) > 30) {
                static::$registrationErrors[] = 'Password must be between 8 and 30 characters long';
            }
            if (! preg_match(static::USERNAMEREGEX, $username)) {
                static::$registrationErrors[] = 'Username must only contain letters and numbers';
            }
            if (! preg_match(static::PASSWORDREGEX, $password)) {
                static::$registrationErrors[] = 'Password must only contain letters and numbers';
            }
            if ($password != $confirm) {
                static::$registrationErrors[] = 'Passwords do not match';
            }
        }
        // If the formats, etc are valid, check that the user doesn't already exist.
        if (count(static::$registrationErrors) == 0) {
            $exists = User::where('username', '=', $username)->first();
            if ($exists) {
                static::$registrationErrors[] = 'This username is already taken';
            }
        }
        
        // If everything is okay, actually create the user record.
        if (count(static::$registrationErrors) == 0) {
            // Create the user account.
            $user = new User();
            $user->username = $username;
            $user->password = Hash::make($password);
            $user->save();
            // Log them in.
            Auth::login($user);
            // Return success!
            return true;
        }
        
        return false;
    }
    
    /**
     * Method that simply returns the registration error messages.
     *
     * @return array The registration errors.
     */
    public static function getRegistrationErrors() {
        return static::$registrationErrors;
    }
    
    /**
     * Method that attempts to log the user in.
     * It invokes the Auth class, which in turn calls the isLoginValid in this class.
     *
     * @param $credentials array The username and password.
     *
     * @return boolean Whether the login was successful.
     */
    public static function attemptLogin($credentials) {
        return Auth::attempt($credentials);
    }
    
    /**
     * Callback function for the Auth class. Is called when the user attempts to log
     * in. It simply checks whether the user exists and then whether the password is correct.
     *
     * @param $username string The username.
     * @param $password string The password.
     *
     * @return boolean Whether the credentials are valid.
     */
    public static function isLoginValid($username, $password) {
        $user = User::where('username', '=', $username)->first();
        if ($user) {
            //die(Hash::make($password));
            // Username is valid. Check that the password is.
            return (Hash::check($user->password, Hash::make($password)));
        } else {
            return false;
        }
    }
    
    /**
     * Method to simply log the user out.
     */
    public static function logOut() {
        Auth::logout();
    }
    
    /**
     * Method to return a set of users for the API, after applying any user-defined filtering.
     * It uses the Input fields & values for the filter.
     *
     * @return Collection of Users matching the filter (if any), sorted by username.
     */
    public static function apiFilter() {
        // Order the results by id, then implement any filters.
        $users = User::orderBy('username');
        if (Input::has('username')) {
            $users = $users->where('username', 'like', '%' . Input::get('username') . '%');
        }
        $users = $users->get();
        
        return $users;
    }
    
    /**
     * Method to return this user in a suitable format for the calling process, as determined
     * by the mode. By default the user is converted to an array.
     *
     * @param string $mode Defines the format that the calling process wants.
     *
     * @return array The user as an array, with any mode-specific modifications.
     */
    public function format($mode = null) {
        $result = $this->toArray();
        if ($mode == 'api') {
            // Nothing to do here!
        }
        return $result;
    }
    
}
