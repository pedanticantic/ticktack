<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ToDoViewsTicket extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
      * The nominal width of a ticket in a To Do View.
      *
      * @var int
      */
    const TICKETWIDTH = 13;
    
    /**
      * The nominal height of a ticket in a To Do View.
      *
      * @var int
      */
    const TICKETHEIGHT = 17;
    
    /**
     * Method to return all the data for the given view.
     * This will have been requested via AJAX from the browser, and the data
     * will be returned in JSON format.
     *
     * @param integer $viewId A view id.
     *
     * @return array The tickets in this view, and their relationships.
     */
    public static function getContentsOfView($viewId) {
        // Call the routine to get the data that is optionally filtered on a ticket, and
        // don't pass a ticket id in (which means we get everything).
        return static::getContentsOfViewFiltered($viewId);
    }
    
    /**
     * Method to return the data for a specific ticket for the given view.
     *
     * @param integer $viewId A view id.
     *
     * @return array The tickets in this view, and their relationships.
     */
    public static function getContentsOfViewFiltered($viewId, $ticketId = null) {
        $toDoView = ToDoView::getView($viewId);
        if ($toDoView === false) {
            return false;
        }
        
        // Pick up all the tickets on this view, and left join to their relationships.
        $tickets = static::where('to_do_view_id', '=', $viewId)
                        ->join('tickets', 'to_do_views_tickets.ticket_id', '=', 'tickets.id');
        if ($ticketId > 0) {
            $tickets = $tickets->where('tickets.id', '=', $ticketId);
        }
        $tickets = $tickets->leftJoin('tickets_associations_view', 'tickets.id', '=', 'tickets_associations_view.src_ticket_id')
                           ->select('ticket_id', 'x_position', 'y_position',
                                    'tickets.summary',
                                    'to_do_views_tickets.view_summary',
                                    'tickets_associations_view.id AS rel_id',
                                    'tickets_associations_view.relationship_type',
                                    'tickets_associations_view.trg_ticket_id'
                           )
                           ->orderByRaw("tickets.id, tickets_associations_view.src_sort_order")
                           ->get();
        // Loop through them and add the (unique) records to the array(s).
        $viewData = [
          'tickets' => [],
          'relationships' => [],
          'viewport' => ['x' => $toDoView->viewport_x, 'y' => $toDoView->viewport_y]
        ];
        $relIdsSeen = [];
        $lastTicketId = -1;
        foreach($tickets as $ticket) {
            if ($lastTicketId != $ticket->ticket_id) {
                // It's a different ticket, so record it.
                $viewData['tickets'][] = [
                    'id' => $ticket->ticket_id,
                    'x_pos' => $ticket->x_position,
                    'y_pos' => $ticket->y_position,
                    'type_image' => $ticket->ticket->getTypeImageObject(),
                    'status_image' => $ticket->ticket->getStatusImageObject(),
                    'summary' => htmlentities($ticket->summary),
                    'view_summary' => htmlentities($ticket->view_summary)
                ];
                $lastTicketId = $ticket->ticket_id;
            }
            if ($ticket->rel_id > 0) {
                // We need to check that the reverse relationship isn't already in the list.
                if (! array_key_exists($ticket->rel_id, $relIdsSeen)) {
                    $viewData['relationships'][] = [
                        'id' => $ticket->rel_id,
                        'src_ticket_id' => $ticket->ticket_id,
                        'trg_ticket_id' => $ticket->trg_ticket_id,
                        'relationship_type' => $ticket->relationship_type
                    ];
                    $relIdsSeen[$ticket->rel_id] = true;
                }
            }
        }
        Log::debug('View contents', $viewData);
        return $viewData;
    }
    
    /**
     * Method to add the given ticket to the given view. It's not quite as
     * straightforward as inserting the link record because we have to make sure
     * it doesn't overlap with any existing tickets on that view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public static function addLink($viewId, $ticketId, $relativeTicketId = null, $viewSummary = null) {
        // Find a "space" on the view to add the ticket.
        $newPosition = static::determineNewTicketPosition($viewId, $relativeTicketId);
        Log::debug('New position', $newPosition);
        
        // Create the link record.
        $Link = new ToDoViewsTicket();
        $Link->to_do_view_id = $viewId;
        $Link->ticket_id = $ticketId;
        $Link->x_position = $newPosition['x_pos'];
        $Link->y_position = $newPosition['y_pos'];
        $Link->width = 0;  // These aren't used yet
        $Link->height = 0;  // These aren't used yet
        $Link->view_summary = $viewSummary;
        $Link->save();
        
        // Return an array of the ticket/relationship data in case it's called from the view itself.
        return static::getContentsOfViewFiltered($viewId, $ticketId);
    }
    
    /**
     * Method to find a space in the view for a new ticket. We return the x and y
     * coordinates. We basically start at the top-left, and search in diagonal stripes
     * until we find a hole big enough!
     *
     * @param int $viewId View id.
     * @param int $relativeTicketId Ticket to position this ticket relative to.
     */
    public static function determineNewTicketPosition($viewId, $relativeTicketId = null) {
        // This is a little complicated. We will retrieve all the tickets currently
        // in the view. For each one, we'll mark off all the points on an imaginary
        // grid where a new ticket would overlap this one (imagine a box 4 times the
        // size of this ticket, with this ticket in the lower right quarter).
        // Then, look in diagonal stripes to find the first position not marked.
        // Start at (0,0), then do (1,0), (0,1), then (2,0), (1,1), (0,2), etc.
        
        // Get all the tickets.
        $viewTickets = static::where('to_do_view_id', '=', $viewId)->get();
        // Loop through and mark off the "out of bounds" box for each one.
        $noGo = [];
        $offset = ['x' => 0, 'y' => 0];
        foreach($viewTickets as $viewTicket) {
            for($y_pos = -1 * static::TICKETHEIGHT ; $y_pos <= static::TICKETHEIGHT ; $y_pos++) {
                $noGoY = $y_pos + $viewTicket->y_position;
                if (! array_key_exists($noGoY, $noGo)) {
                    $noGo[$noGoY] = [];
                }
                for($x_pos = -1 * static::TICKETWIDTH ; $x_pos <= static::TICKETWIDTH ; $x_pos++) {
                    $noGoX = $x_pos + $viewTicket->x_position;
                    if (! array_key_exists($noGoX, $noGo[$noGoY])) {
                        $noGo[$noGoY][$noGoX] = false;  // Any value, it doesn't matter!
                    }
                }
            }
            // If this is the ticket that we're going to position the ticket relative to,
            // remember its position.
            if ($relativeTicketId != null && $relativeTicketId == $viewTicket->ticket_id) {
                $offset['x'] = $viewTicket->x_position;
                $offset['y'] = $viewTicket->y_position;
            }
        }
        
        // Look at each vertical stripe.
        // If a "relative to" ticket was passed in, add it's position to the test position.
        $stripeIdx = 0;
        while (true) {
            for($tripePos = 0 ; $tripePos <= $stripeIdx ; $tripePos++ ) {
                $stripeX = $offset['x'] + $stripeIdx - $tripePos;
                $stripeY = $offset['y'] + $tripePos;
                if (! array_key_exists($stripeY, $noGo)) {
                    // Nothing on this entire line is out of bounds.
                    break 2;
                }
                if (! array_key_exists($stripeX, $noGo[$stripeY])) {
                    // This position is okay for the top-left corner of the ticket.
                    break 2;
                }
            }
            $stripeIdx++;
        }
        // In theory, this will always finish!
        return ['x_pos' => $stripeX, 'y_pos' => $stripeY];
    }
    
    /**
     * Method to update the position of the given ticket on the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public static function updatePosition($viewId, $ticketId, $newXPos, $newYPos) {
        Log::debug('Updating position for ticket', [$ticketId]);
        Log::debug('On view id', [$viewId]);
        Log::debug('New positions', [$newXPos, $newYPos]);
        $result = ['success' => false, 'affected' => []];
        // Find the record, and update it.
        $link = static::where('to_do_view_id', '=', $viewId)
                    ->where('ticket_id', '=', $ticketId)
                    ->first();
        if ($link != null) {
            $link->x_position = $newXPos;
            $link->y_position = $newYPos;
            $link->save();
            $result['success'] = true;
            // We need to see if any tickets need to be moved to accommodate this one.
            // Then return an array of "deltas".
            $result['affected'] = ToDoView::getView($viewId)->scoot($ticketId);
        }
        return $result;
    }
    
    /**
     * Method to remove the given ticket from the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public static function removeTicket($viewId, $ticketId) {
        Log::debug('Removing ticket', [$ticketId]);
        Log::debug('From view id', [$viewId]);
        $result = ['success' => false];
        // Find the record, and delete it.
        $link = static::where('to_do_view_id', '=', $viewId)
                    ->where('ticket_id', '=', $ticketId)
                    ->first();
        $link->delete();
        $result['success'] = true;
        // @TODO: We need to see if any relationships can be removed from the browser's memory.
        //        Then return an array of same.
        return $result;
    }
    
    /**
     * Method to set the view-specific summary of the given ticket in the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public static function setTicketSummary($viewId, $ticketId, $newViewSummary) {
        Log::debug('Setting ticket summary for ticket', [$ticketId]);
        Log::debug('From view id', [$viewId]);
        $result = ['success' => false];
        // If the new value has been supplied (even if blank), find the record, and update it.
        $link = static::where('to_do_view_id', '=', $viewId)
                    ->where('ticket_id', '=', $ticketId)
                    ->first();
        if ($link != null) {
            $link->view_summary = ($newViewSummary == '' ? null : $newViewSummary);
            $link->save();
            $result['success'] = true;
        }
        return $result;
    }
    
    /**
     * Method to define the relationship between a link record and the ticket.
     */
    public function ticket() {
        return $this->belongsTo('Ticket', 'ticket_id');
    }
    
}
