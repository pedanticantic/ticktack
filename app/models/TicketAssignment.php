<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TicketAssignment extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * The underlying table name.
     *
     * @var string
     */
    protected $table = 'tickets_assignments';
    
    /**
     * Defines the relationship to the parent user record.
     */
    public function user() {
        return $this->belongsTo('User', 'user_id');
    }
    
    /**
     * Method to set the sort order of a new assignment record. It will be 1 plus max sort orders
     * for the parent ticket.
     */
    public function setSortOrder() {
    	$this->sort_order = 1;
	    if ($this->id == 0 && $this->ticket_id > 0) {
	    	$ticket = Ticket::find($this->ticket_id);
	    	if ($ticket != null) {
	    		// "assignmentsReverse" is a special version of the relationship where the
                // assignments are sorted in reverse order. Taking the first of these gives
                // us the highest sort order.
                $highest = $ticket->assignmentsReverse()->first();
	    		if ($highest != null) {
			    	$this->sort_order = 1 + $highest->sort_order;
	    		}
	    	}
	    }
	}
    
    /**
     * Method to remove the given assignment record. It will update any sort orders for remaining
     * users assigned to the same ticket.
     *
     * @param $assignmentId int The assignment id.
     *
     * @return int The ticket id.
     */
    public static function remove($assignmentId) {
        // Load the assignment, remember the ticket id and sort order, and delete the assignment.
        $assignment = static::find($assignmentId);
        $ticketId = null;
        if ($assignment != null) {
            $ticketId = $assignment->ticket_id;
            $sortOrder = $assignment->sort_order;
            $assignment->delete();
            
            // Find all the assignments for the same ticket with a higher sort order, and decrement them.
            $afters = static::where('ticket_id', '=', $ticketId)
                        ->where('sort_order', '>', $sortOrder)
                        ->get();
            foreach($afters as $after) {
                $after->sort_order--;
                $after->save();
            }
        }
        
        return $ticketId;
    }
    
    public static function alreadyAssigned($ticket_id, $user_id) {
        $exists = static::where('ticket_id', '=', $ticket_id)
                    ->where('user_id', '=', $user_id)
                    ->first();
        return ($exists != null);
    }
    
    public static function assignUserToTicket($ticket_id, $user_id, $newSortOrder = null) {
        // Check that the user isn't already assigned to the ticket.
        $existing = TicketAssignment::where('ticket_id', $ticket_id)->where('user_id', $user_id)->count();
        if ($existing > 0) {
            return null;
        }
        
        // Create the new record, with the default sort order (ie 1+ max).
        $assignment = new TicketAssignment();
        $assignment->ticket_id = $ticket_id;
        $assignment->user_id = $user_id;
        $assignment->setSortOrder();  // Determines the sort order.
        $assignment->save();
        // If the client specified a sort order, set it now.
        if ($newSortOrder != null) {
            // If the requested sort order is higher than the max, just silently ignore the request.
            if ($newSortOrder < $assignment->sort_order) {
                $newSortOrder = ($newSortOrder < 1 ? 1 : $newSortOrder);
                // Increment the sort order for records against this ticket that have a sort order
                // greater than or equal to the desired sort order.
                TicketAssignment::where('ticket_id', '=', $ticket_id)
                    ->where('sort_order', '>=', $newSortOrder)
                    ->increment('sort_order'); // This is auto-saved
                // And set the desired sort order in the new row.
                $assignment->sort_order = $newSortOrder;
                $assignment->save();
            }
        }
        return $assignment->id;
    }
    
    public static function moveAssignment($assignmentId, $assignmentAction) {
        $result = ['code' => 0, 'new_sort_order' => null, 'message' => 'Unknow error'];
        // Make sure the action is valid.
        if ($assignmentAction == 'up' || $assignmentAction == 'down') {
            
            // Load the assignment record. This will give us the ticket id and sort order.
            $assignmentSrc = TicketAssignment::find($assignmentId);
            if ($assignmentSrc == null) {
                $result['code'] = 1;
                $result['message'] = 'Invalid assignment id';
            } else {
                // Load the assignment record we're going to swap with. It will have the same
                // ticket id, and the sortorder will be +/- 1 from the source one.
                $delta = $assignmentAction == 'up' ? -1 : 1;
                $assignmentTrg = TicketAssignment::where('ticket_id', '=', $assignmentSrc->ticket_id)->where('sort_order', '=', $assignmentSrc->sort_order + $delta)->first();
                if ($assignmentTrg == null) {
                    $result['code'] = 3;
                    $result['message'] = 'Cannot move any further';
                } else {
                    $assignmentSrc->sort_order += $delta;
                    $assignmentSrc->save();
                    $assignmentTrg->sort_order -= $delta;
                    $assignmentTrg->save();
                    $result['code'] = 0;
                    $result['new_sort_order'] = $assignmentSrc->sort_order;
                    $result['message'] = 'Assignment was moved';
                }
            }
        } else {
            $result['code'] = 2;
            $result['message'] = 'Invalid direction';
        }
        
        return $result;
    }
    
    public static function allowed($assignmentId, $operaion, $ticketId = null) {
        if ($ticketId != null) {
            $ticket = Ticket::find($ticketId);
            if (Project::allowed($ticket->project_id, 'activity')) {
                return true;
            }
        }
        return false;
    }
    
}