<?php

/**
 * A model for managing ticket searches. The main functions are providing the values
 * for the search form, executing the search & returning the matching tickets, and
 * remembering the search between pages.
 * In the future, the user will be able to save a search with a name, and return to
 * it at a later date.
 *
 */
class TicketSearch {
    
    /**
     * Method to save the posted search criteria into the user's session.
     *
     * @return bool Whether the save was successful.
     */
    public static function saveSearchCriteria() {
        // Just instantiate a TicketSearch object, and chuck all the posted values at it.
        // Every call saves the current criteria in the session, ready for when the search
        // results need to be produced (usually immediately after).
        $ticketSearch = new TicketSearch();
        // If the user clicked "clear criteria", forget all the criteria and go to the search results.
        if (Input::has('clear')) {
            $ticketSearch->clearCriteria();
            return true;
        }
        
        // If the user is using the quick search, clear the criteria, set a few defaults (and the
        // text criteria, and exit).
        if (Input::has('quick_search')) {
            $ticketSearch->clearCriteria();
            $ticketSearch->setCriteria('text', Input::get('quick_search'));
            $ticketSearch->setCriteria('text_in,summary', 'y');
            $ticketSearch->setCriteria('text_in,description', 'y');
            $ticketSearch->setCriteria('text_in,comments', 'y');
            $ticketSearch->setCriteria('status,new', 'y');
            $ticketSearch->setCriteria('status,assigned', 'y');
            $ticketSearch->setCriteria('status,inprogress', 'y');
            return true;
        }
        
        $ticketSearch->setCriteria('ids', Input::get('ids'));
        
        $ticketSearch->setCriteria('ticket_type', Input::get('ticket_type'));
        
        $ticketSearch->setCriteria('text', Input::get('text'));
        $ticketSearch->setCriteria('text_in,summary', Input::get('text_in_summary'));
        $ticketSearch->setCriteria('text_in,description', Input::get('text_in_description'));
        $ticketSearch->setCriteria('text_in,comments', Input::get('text_in_comments'));
        
        $allStatuses = Ticket::getAllStatuses(false);
        foreach($allStatuses as $id => $dummy) {
            $ticketSearch->setCriteria('status,' . $id, Input::get('status_' . $id));
        }
        
        $ticketSearch->setCriteria('assignee,id', Input::get('assignee'));
        $ticketSearch->setCriteria('assignee,scope', Input::get('assignee_scope'));
        
        $ticketSearch->setCriteria('project,id', Input::get('project'));
        
        $ticketSearch->setCriteria('priority', Input::get('priority'));
        
        // @TODO: We should validate that the dates are valid.
        $ticketSearch->setCriteria('due_date,from', Input::get('due_date_from'));
        $ticketSearch->setCriteria('due_date,to', Input::get('due_date_to'));
        
        // We save the criteria as a number of seconds.
        // @TODO: We should validate that the times are valid.
        $ticketSearch->setCriteria('est_time,from', DatetimeManager::englishToInterval(Input::get('est_time_from')));
        $ticketSearch->setCriteria('est_time,to', DatetimeManager::englishToInterval(Input::get('est_time_to')));
        
        $ticketSearch->setCriteria('created_by,id', Input::get('created_by'));
        
        return true;
    }
    
    /**
     * Method to apply the filters in the URL and save them as search criteria into the
     * user's session.
     *
     * @return bool Whether the save was successful.
     */
    public static function applyFilter() {
        
        // Create a new search instance.
        $ticketSearch = new TicketSearch();
        
        // Clear any existing critieria, and set the defaults.
        $ticketSearch->clearCriteria();
        $ticketSearch->setCriteria('text_in,summary', 'y');
        $ticketSearch->setCriteria('text_in,description', 'y');
        $ticketSearch->setCriteria('text_in,comments', 'y');
        
        // Now go through all the filters passed in and apply them as necessary.
        $allFilters = Input::all();
        Log::debug("\n\n" . 'Processing filters...');
        foreach($allFilters as $oneFilter => $filterValue) {
            Log::debug('Next filter (raw): ' . $oneFilter . ' => ' . $filterValue);
            // We need to manipulate the filters a little.
            $oneFilter = rtrim($oneFilter, '1234567890');  // A hack so that the same filter can appear multiple times.
            switch ($oneFilter) {
                case 'project_id':
                    $oneFilter = 'project,id';
                    break;
                case 'status':
                    $oneFilter .= ',' . str_replace('_', '', $filterValue);
                    $filterValue = 'y';
                    break;
                case 'priority':
                    $filterValue = str_replace('_', ' ', $filterValue);
                    break;
                case 'assignee_id':
                    $oneFilter = 'assignee,id';
                    break;
                case 'assignee_scope':
                    $oneFilter = 'assignee,scope';
                    break;
            }
            Log::debug('This filter (processed): ' . $oneFilter . ' => ' . $filterValue);
            $ticketSearch->setCriteria($oneFilter, $filterValue);
        }
        
        return true;
    }
    
    /**
     * Method to handle the user executing a search, and returning the tickets.
     *
     * @return array The matching ticket records.
     */
    public static function getSearchResults() {
        // @TODO: If no criteria found, default to searching for text in the summary and description.
        // Load the session critieria in.
        $searchCriteria = new TicketSearch();
        // We need to build up all the where clauses for the query, then execute it.
        // We have to start with "everything" before we start adding the clauses.
        $searchQuery = Ticket::whereRaw('TRUE');
        
        if ($searchCriteria->has('ids')) {
            $searchQuery->whereIn('id', explode(',', $searchCriteria->get('ids')));
        }
        
        if ($searchCriteria->has('ticket_type')) {
            $searchQuery->where('ticket_type', '=', $searchCriteria->get('ticket_type'));
        }
        
        if ($searchCriteria->has('text')) {
            // @TODO: Go through the different checkboxes.
            $textIn = [];
            if ($searchCriteria->has('text_in,summary')) {
                $textIn[] = 'summary';
            }
            if ($searchCriteria->has('text_in,description')) {
                $textIn[] = 'description';
            }
            // @TODO: Work out how to do matching in comments
            if (count($textIn) > 0) {
                $textLike = "";
                foreach($textIn as $textInPart) {
                    $textLike .= " OR " . $textInPart . " LIKE '%" . $searchCriteria->get('text') . "%'";
                }
                $textLike = "(" . substr($textLike, 4) . ")";
                $searchQuery->whereRaw($textLike);
            }
        }
        
        $allStatuses = Ticket::getAllStatuses();
        $allowedStatuses = [];
        foreach($allStatuses as $id => $dummy) {
            if ($searchCriteria->has('status,' . str_replace(' ', '', $id))) {
                $allowedStatuses[] = $id;
            }
        }
        if (count($allowedStatuses) > 0) {
            $searchQuery->whereIn('status', $allowedStatuses);
        }
        
        if ($searchCriteria->has('assignee,id')) {
            $searchQuery->whereExists(function($query) {
                // This is horrible. Because we're inside a closure, we don't have access to
                // the variables outside, so we have to instantiate another search object
                // and use that to test/get the values.
                $tempCriteria = new TicketSearch();
                
                // Start by only including ticket who have the given user as an assignee.
                $query = $query->select(DB::raw(1))
                    ->from('tickets_assignments')
                    ->whereRaw('tickets_assignments.ticket_id = tickets.id')
                    ->where('tickets_assignments.user_id', '=', $tempCriteria->get('assignee,id'));
                // If the user is being more specific, check the sort order (1 = primary, greater than 1 = other)
                if ($tempCriteria->has('assignee,scope')) {
                    $scope = $tempCriteria->get('assignee,scope');
                    if ($scope != 'assignee_any') {
                        $operator = $scope == 'assignee_primary' ? '=' : '!=';
                        $query = $query->where('tickets_assignments.sort_order', $operator, 1);
                    }
                }
            });
        }
            
        if ($searchCriteria->has('project,id')) {
            $searchQuery->where('project_id', '=', $searchCriteria->get('project,id'));
        }
            
        if ($searchCriteria->has('priority')) {
            $searchQuery->where('priority', '=', $searchCriteria->get('priority'));
        }
            
        if ($searchCriteria->has('due_date,from')) {
            $searchQuery->where('due_datetime', '>=', DatetimeManager::datetimeUKToString($searchCriteria->get('due_date,from')));
        }
        if ($searchCriteria->has('due_date,to')) {
            $searchQuery->where('due_datetime', '<=', DatetimeManager::datetimeUKToString($searchCriteria->get('due_date,to')));
        }
        
        // Estimated time criteria is in seconds, so no conversions are needed.
        if ($searchCriteria->has('est_time,from')) {
            $searchQuery->where('estimated_length_seconds', '>=', $searchCriteria->get('est_time,from'));
        }
        if ($searchCriteria->has('est_time,to')) {
            $searchQuery->where('estimated_length_seconds', '<=', $searchCriteria->get('est_time,to'));
        }
        
        if ($searchCriteria->has('created_by,id')) {
            $searchQuery->where('created_by_user_id', '=', $searchCriteria->get('created_by,id'));
        }
        
        // @TODO: Need to sort out pagination and sorting by columns.
        return $searchQuery->orderBy('tickets.updated_at', 'DESC')->get();
    }
    
    /**
     * Method to return the ids search criteria from the session.
     *
     * @return string The ids.
     */
    public function getIdsSearchValue() {
        return $this->get('ids');
    }
    
    /**
     * Method to return the Text search criteria from the session.
     *
     * @return string The Text search value.
     */
    public function getTextSearchValue() {
        return $this->get('text');
    }
    
    /**
     * Method to return the all the types, with the one from session "selected".
     *
     * @return array The type search options.
     */
    public function getTypeSearchOptions() {
        $result = [];
        $allTypes = Ticket::allTypes();
        foreach($allTypes as $oneType => $typeDesc) {
            $selected = $this->get('ticket_type') == $oneType ? 'selected="selected"' : '';
            $result[] = ['type' => $oneType, 'desc' => $typeDesc, 'selected' => $selected];
        }
        return $result;
    }
    
    /**
     * Method to return the text location search criteria, with the one(s) from session "selected".
     *
     * @return array The text location options.
     */
    public function getTextSearchOptions() {
        $summaryChecked = $this->has('text_in,summary');
        $descriptionChecked = $this->has('text_in,description');
        $commentsChecked = $this->has('text_in,comments');
        // If none of the checkboxes are checked, assume summary and description.
        if (! ($summaryChecked || $descriptionChecked || $commentsChecked)) {
            $summaryChecked = true;
            $descriptionChecked = true;
        }
        $textSearchOptions = [
            'text_in_summary' => ['prompt' => 'Summary', 'value' => $summaryChecked ? 'checked="checked"' : ''],
            'text_in_description' => ['prompt' => 'Description', 'value' => $descriptionChecked ? 'checked="checked"' : ''],
            'text_in_comments' => ['prompt' => 'Comments (not implemented yet)', 'value' => $commentsChecked ? 'checked="checked"' : ''],
        ];
        return $textSearchOptions;
    }
    
    /**
     * Method to return the status search criteria, with the one(s) from session "selected".
     *
     * @return array The status options.
     */
    public function getStatusSearchOptions() {
        $result = [];
        $allStatuses = Ticket::getAllStatuses(false);
        foreach($allStatuses as $id => $oneStatus) {
            $checked = $this->has('status,' . str_replace(' ', '', $id)) ? 'checked="checked"' : '';
            $result[] = ['id' => 'status_' . $id, 'desc' => $oneStatus, 'selected' => $checked];
        }
        return $result;
    }
    
    /**
     * Method to return the assignee search criteria, with the one from session "selected".
     *
     * @return array The assignee options.
     */
    public function getAssigneeSearchOptions() {
        $result = [];
        $allUsers = User::all();
        // @TODO: We need an option of "me" at the start, with the details of the logged-in user (id of -1).
        foreach($allUsers as $oneUser) {
            $selected = $this->get('assignee,id') == $oneUser->id ? 'selected="selected"' : '';
            $result[] = ['id' => $oneUser->id, 'name' => $oneUser->username, 'selected' => $selected];
        }
        return $result;
    }
    
    /**
     * Method to return the assignee scope search criteria, with the one(s) from session "selected".
     *
     * @return array The assignee scope options.
     */
    public function getAssigneeScopeOptions() {
        $scope = $this->get('assignee,scope');
        $scope = $scope == '' ? 'assignee_any' : $scope;
        return [
            'assignee_primary' => ['desc' => 'Primary', 'selected' => $scope == 'assignee_primary' ? 'checked="checked"' : ''],
            'assignee_other' => ['desc' => 'Other', 'selected' => $scope == 'assignee_other' ? 'checked="checked"' : ''],
            'assignee_any' => ['desc' => 'Any', 'selected' => $scope == 'assignee_any' ? 'checked="checked"' : '']
        ];
    }
    
    /**
     * Method to return the project search criteria, with the one from session "selected".
     *
     * @return array The project options.
     */
    public function getProjectSearchOptions() {
        $result = [];
        $projects = Project::getAllProjects();
        foreach($projects as $project) {
            $selected = $this->get('project,id') == $project->id ? 'selected="selected"' : '';
            $result[] = ['id' => $project->id, 'name' => $project->name, 'selected' => $selected];
        }
        return $result;
    }
    
    /**
     * Method to return the priority search criteria, with the one from session "selected".
     *
     * @return array The priority options.
     */
    public function getPrioritySearchOptions() {
        $result = [];
        $priorities = Ticket::getAllPriorities();
        foreach($priorities as $id => $onePriority) {
            $checked = $id == $this->get('priority') ? 'selected="selected"' : '';
            $result[] = ['id' => $id, 'desc' => $onePriority, 'selected' => $checked];
        }
        return $result;
    }
    
    /**
     * Method to return the due date "from" search criteria, with the one(s) from session "selected".
     *
     * @return string The due date "from" value.
     */
    public function getDueDateFrom() {
        return $this->get('due_date,from');
    }
    
    /**
     * Method to return the due date "to" search criteria, with the one(s) from session "selected".
     *
     * @return string The due date "to" value.
     */
    public function getDueDateTo() {
        return $this->get('due_date,to');
    }
    
    /**
     * Method to return the estimated time "from" search criteria, with the one(s) from session "selected".
     *
     * @return string The estimated time "from" value.
     */
    public function getEstTimeFrom() {
        return DatetimeManager::intervalToEnglish($this->get('est_time,from'));
    }
    
    /**
     * Method to return the estimated time "to" search criteria, with the one(s) from session "selected".
     *
     * @return string The estimated time "to" value.
     */
    public function getEstTimeTo() {
        return DatetimeManager::intervalToEnglish($this->get('est_time,to'));
    }
    
    /**
     * Method to return the created by search criteria, with the one from session "selected".
     *
     * @return array The created by values.
     */
    public function getCreatedBySearchOptions() {
        $result = [];
        $allUsers = User::all();
        // @TODO: We need an option of "me" at the start, with the details of the logged-in user (id of -1).
        foreach($allUsers as $oneUser) {
            $selected = $this->get('created_by,id') == $oneUser->id ? 'selected="selected"' : '';
            $result[] = ['id' => $oneUser->id, 'name' => $oneUser->username, 'selected' => $selected];
        }
        return $result;
    }
    
    /**
     * Constructor function.
     */
    public function __construct() {
        $this->searchCriteria = [];
        if (Session::has('search_criteria')) {
            $this->searchCriteria = Session::get('search_criteria');
        }
    }
    
    /**
     * Method to clear search criteria both in the object and the session.
     */
    private function clearCriteria() {
        $this->searchCriteria = [];
        
        // Update the session with the new (empty) criteria.
        Session::put('search_criteria', $this->searchCriteria);
    }
    
    /**
     * Method to store the given search criteria value against the given "name" in the session.
     *
     * @param string $criteriaPath comma-separated path for the criteria.
     * @param mixed $newValue The new search value (or null).
     */
    private function setCriteria($criteriaPath, $newValue) {
        // Expand the path, and make sure the elements exist.
        $pathParts = explode(',', $criteriaPath);
        $pathLast = array_pop($pathParts);
        $criteriaElement = &$this->searchCriteria;
        foreach($pathParts as $pathPart) {
            if (! array_key_exists($pathPart, $criteriaElement)) {
                $criteriaElement[$pathPart] = [];
            }
            $criteriaElement = &$criteriaElement[$pathPart];
        }
        $criteriaElement[$pathLast] = $newValue;
        
        // Update the session with the new criteria.
        Session::put('search_criteria', $this->searchCriteria);
    }
    
    /**
     * Method to say whether the given search criteria exists in the session, and is not null.
     *
     * @param string $criteriaPath The path of the criteria.
     * @param bool $returnTheValue Whether to return the presence (true) of the criteria; or the value (false).
     *
     * @return mixed true/false if we're only asked whether it exists; the value otherwise.
     */
    private function has($criteriaPath, $returnTheValue = false) {
        // Expand the path, and traverse the tree until we get to the value.
        $pathParts = explode(',', $criteriaPath);
        $criteriaElement = &$this->searchCriteria;
        $found = true;
        foreach($pathParts as $pathPart) {
            if (array_key_exists($pathPart, $criteriaElement)) {
                $criteriaElement = &$criteriaElement[$pathPart];
            } else {
                $found = false;
                break;
            }
        }
        
        if ($returnTheValue === true) {
            return $found ? $criteriaElement : null;
        }
        return $found && $criteriaElement != null;
    }
    
    /**
     * Method to return the given search criteria from the session.
     *
     * @param string $criteriaPath The path of the criteria.
     *
     * @return mixed false if it doesn't exist or is null; the value otherwise.
     */
    private function get($criteriaPath) {
        return $this->has($criteriaPath, true);
    }
    
}
