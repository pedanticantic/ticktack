<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TicketAssociation extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * The underlying table name.
     *
     * @var string
     */
    protected $table = 'tickets_associations';
    
    /**
     * Method to attempt to save a comment.
     *
     * @param int $ticketId The id of the "source" ticket.
     *
     * @return array All the tickets that have a relationship with this one.
     */
    public static function getReferences($ticketId) {
        // Return all the rows where the given ticket id matches either
        // the source or target ticket.
        return static::where('src_ticket_id', '=', $ticketId)->orWhere('trg_ticket_id', '=', $ticketId)->get();
    }
    
    /**
     * Static method to return the relationship codes and their descriptions.
     */
    public static function getRelationshipTypes() {
        return ['~' => 'associated',
                '<' => 'dependent',
                '>' => 'dependency',
                '+' => 'parent',
                '-' => 'child'];
    }
    
    /**
     * Method to return the code for the given relationship type.
     *
     * @param $relationshipType string Relationship type.
     *
     * @return string Null if type is invalid, otherwise the code representing the type.
     */
    public static function relToCode($relationshipType) {
        $conv = ['associated' =>'~',
                 'dependent' => '<',
                 'dependency' => '>',
                 'parent' => '+',
                 'child' => '-'];
        if (! array_key_exists($relationshipType, $conv)) {
            return null;
        }
        return $conv[$relationshipType];
    }
    
    /**
     * Method to return the type for the given relationship code.
     *
     * @param $relationshipCode string Relationship code.
     *
     * @return string Null if code is invalid, otherwise the type representing the code.
     */
    public static function relToType($relationshipCode) {
        Log::debug('rel to type: ' . $relationshipCode);
        $conv = self::getRelationshipTypes();
        Log::debug('Conv - code to type', $conv);
        if (! array_key_exists($relationshipCode, $conv)) {
            Log::debug($relationshipCode . ' was not found in the conversion array', true);
            return null;
        }
        Log::debug($relationshipCode . ' was converted okay');
        return $conv[$relationshipCode];
    }
    
    /**
     * Method to return the "reverse" code for the given relationship type.
     *
     * @param $relationshipType string Reverse relationship type.
     *
     * @return string Null if type is invalid, otherwise the code representing the type.
     */
    public static function relToCodeReverse($relationshipType) {
        $conv = ['associated' =>'~',
                 'dependent' => '>',
                 'dependency' => '<',
                 'parent' => '-',
                 'child' => '+'];
        if (! array_key_exists($relationshipType, $conv)) {
            return null;
        }
        return $conv[$relationshipType];
    }
    
    /**
     * Method to create a tickets-associations record for the given relationship type and ticket ids.
     * The difficult bit is determining the sort orders!.
     * The parameters mean "<source ticket> is a <relationship type> of <target ticket".
     *
     * @param string $relationshipCode The relationship type in coded form.
     * @param int $srcTicketId Id of the "source" ticket.
     * @param int $trgTicketId Id of the "target" ticket.
     * @param bool $isPinned Should the record be pinned (ie only deleted if the user explicitly says so)?
     *
     * @return bool True if all the parameters were valid and the record was inserted; false otherwise.
     */
    public static function createRecord($relationshipCode, $srcTicketId, $trgTicketId, $isPinned = false) {
        Log::debug('Creating record...');
        // Do the basic validation.
        if ($srcTicketId == $trgTicketId) {
            Log::debug('src and trg are same');
            return false;
        }
        $relType = static::relToType($relationshipCode);
        Log::debug('Rel type is now: ' . $relType);
        if ($relType == null) {
            Log::debug('Rel is not valid');
            return false;
        }
        $exist = Ticket::whereIn('id', [$srcTicketId, $trgTicketId])->count();
        if ($exist != 2) {
            Log::debug('ids are not valid');
            return false;
        }
        // Check that adding this relationship won't cause a dependency loop.
        if (! static::checkDependencyLoop($relType, $srcTicketId, $trgTicketId)) {
            return false;
        }
        // Check that adding this relationship won't cause a problem with the hierarchy of statuses.
        if (! static::checkStatusHierarchy($relType, $srcTicketId, $trgTicketId)) {
            return false;
        }
        // Now determine the sort orders.
        $srcSortOrder = static::calculateSortOrder($srcTicketId);
        $trgSortOrder = static::calculateSortOrder($trgTicketId);
        Log::debug('Src/trg sort order: ' . $srcSortOrder . '/' . $trgSortOrder);
        // Now create the record.
        $assoc = new TicketAssociation();
        $assoc->relationship_type = $relType;
        $assoc->src_ticket_id = $srcTicketId;
        $assoc->src_sort_order = $srcSortOrder;
        $assoc->trg_ticket_id = $trgTicketId;
        $assoc->trg_sort_order = $trgSortOrder;
        $assoc->pinned = $isPinned;
        $assoc->save();
        Log::debug('Created');
        return true;
    }
    
    /**
     * Method to return the new sort order for the given ticket in a relationship.
     *
     * @param $ticketId int The ticket id that we need the sort order for.
     *
     * @return int The new sort order.
     */
    public static function calculateSortOrder($ticketId) {
        $newSortOrder = static::where('src_ticket_id', '=', $ticketId)
                            ->orWhere('trg_ticket_id', '=', $ticketId)
                            ->count();
        $newSortOrder = ($newSortOrder == null ? 1 : 1 + $newSortOrder);
        return $newSortOrder;
    }
    
    /**
     * Method to check that adding the given relationship won't cause a dependency loop.
     *
     * @param $relationshipCode string The relationship between the src and trg.
     * @param $srcTicketId int The source ticket id.
     * @param $trgTicketId int The target ticket id.
     *
     * @return bool True if the new relationship won't cause a loop; false otherwise.
     */
    public static function checkDependencyLoop($relationshipCode, $srcTicketId, $trgTicketId) {
        Log::debug('Checking dependency: ' . $relationshipCode . ' - ' . $srcTicketId . '/' . $trgTicketId);
        // The easy one is if the relationship is an association - there is no possibility of
        // a loop.
        if ($relationshipCode == 'associated') {
            Log::debug('associated - returning true');
            return true;
        }
        
        // Now, the relationship could be either way round, so if it is "reversed", switch
        // the relationship type to the opposite, and switch the ids over. Actually, we don't
        // care what the relationship is after this point, so don't worry about it.
        if ($relationshipCode == 'dependent' || $relationshipCode == 'parent') {
            Log::debug('Switching the direction of the relationship');
            $tmp = $srcTicketId;
            $srcTicketId = $trgTicketId;
            $trgTicketId = $tmp;
        }
        // Now, what we do is start at the target id, find all its children, and its children's
        // children and so on. If we arrive at the target record before we run out of descendents,
        // then there will be a loop, and we return false.
        $toCheck = [$trgTicketId];
        while(count($toCheck) > 0) {
            Log::debug('To check', $toCheck);
            // Hopefully the logic is fairly straightforward. Take the first element in the list
            // of ids to check (initially this list is just the target id). Find its children. If
            // any are the source id, it's all over and we return false. Otherwise, add them to
            // the end of the list and remove the one from the start. It's a basic tree traversal.
            Log::debug('Finding children of: ' . $toCheck[0]);
            $children = TicketAssociationView::where('src_ticket_id', '=', $toCheck[0])
                            ->whereIn('relationship_type', ['dependency', 'child'])
                            ->get();
            foreach($children as $childTicket) {
                $thisChildId = $childTicket->trg_ticket_id;
                Log::debug('Next child: ' . $thisChildId);
                if ($thisChildId == $srcTicketId) {
                    // We have found the original parent in the descendants of the target,
                    // so a loop will be created, so return false.
                    Log::debug('Found the parent - returning false');
                    return false;
                }
                // A ticket can have multiple parents, so check whether this child is already
                // in the list before adding to the array.
                if (! in_array($thisChildId, $toCheck)) {
                    Log::debug('Adding to the array');
                    $toCheck[] = $thisChildId;
                }
            }
            // Take this descendant off the start of the array and go round again (if there are
            // more checks to do).
            $dummy = array_shift($toCheck);
        }
        // A loop won't be created, so return true.
        Log::debug('All is fine');
        return true;
    }
    
    /**
     * Method to check that adding the given relationship won't cause a problem with statues.
     *
     * @param $relationshipCode string The relationship between the src and trg.
     * @param $srcTicketId int The source ticket id.
     * @param $trgTicketId int The target ticket id.
     *
     * @return bool True if the new relationship won't cause a status problem; false otherwise.
     */
    public static function checkStatusHierarchy($relationshipCode, $srcTicketId, $trgTicketId) {
        Log::debug('Checking status hierarchy: ' . $relationshipCode . ' - ' . $srcTicketId . '/' . $trgTicketId);
        // The easy one is if the relationship is an association - there is no possibility of
        // a problem.
        if ($relationshipCode == 'associated') {
            Log::debug('associated - returning true');
            return true;
        }
        
        // Check that the statuses are going to be valid.
        $srcTicket = Ticket::find($srcTicketId);
        if ($srcTicket->isOpen()) {
            if ($relationshipCode == 'dependent' || $relationshipCode == 'parent') {
                // Ticket in focus is open and we're adding a parent. Make sure the parent
                // and all its ancestors are open.
                $trgTicket = Ticket::find($trgTicketId);
                if ($trgTicket->isClosed() || !$trgTicket->allAncestorsAreOpen()) {
                    // One of them is closed, so return an error.
                    return false;
                }
            }
        } else {
            if ($relationshipCode == 'dependency' || $relationshipCode == 'child') {
                // Ticket in focus is closed and we're adding a child. Make sure the child
                // and all its descendants are closed.
                $trgTicket = Ticket::find($trgTicketId);
                if ($trgTicket->isOpen() || !$trgTicket->allDescendantsAreClosed()) {
                    // One of them is open, so return an error.
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /**
     * Method to delete an association record. This overrides the Eloquent delete() method.
     * We need to update subsequent sort orders for both source and target.
     */
    public function delete() {
        parent::delete();
        // We now need to update both src and trg sort orders.
        // Simply decrement any for the same ticket that have a higher sort order.
        // @TODO: There's a bug in this code: the sort order column in the $this record
        // @TODO: depends on whether the ticket is the source or target in that record, in
        // @TODO: the where clauses, not on the ticket we're looking for.
        $srcSorts = static::where('src_ticket_id', '=', $this->src_ticket_id)
                        ->where('src_sort_order', '>', $this->src_sort_order)
                        ->get();
        foreach($srcSorts as $srcSort) {
            $srcSort->src_sort_order--;
            $srcSort->save();
        }
        $trgSorts = static::where('trg_ticket_id', '=', $this->trg_ticket_id)
                        ->where('trg_sort_order', '>', $this->trg_sort_order)
                        ->get();
        foreach($trgSorts as $trgSort) {
            $trgSort->trg_sort_order--;
            $trgSort->save();
        }
    }
    
    /**
     * Method to handle pinning and unpinning the relationship.
     *
     * @param $srcTicketId int The id of the "driving" ticket in the relationship.
     * @param $pinAction string Either "pin" or "unpin".
     *
     */
    public function handlePinning($srcTicketId, $pinAction) {
        Log::debug('Handling pinning/unpinning');
        // This is relatively straightforward. Just set the "pinned" attribute and save it.
        $this->pinned = ($pinAction == 'pin' ? 1 : 0);
        $this->save();
    }
    
    /**
     * Method to handle moving the relationship up or down within the given ticket id.
     *
     * @param $srcTicketId int The id of the "driving" ticket in the relationship.
     * @param $moveAction string Either "up" or "down".
     *
     */
    public function handleMove($srcTicketId, $moveAction) {
        Log::debug('--- Handling moving up/down ---');
        Log::debug('Clicked relation id: ' . $this->id);
        // This is a little tricky because we have to find the relation immediately before/after
        // this one, as appropriate.
        $deltaSortOrder = ($moveAction == 'down' ? 1 : -1);
        Log::debug('Delta is: ' . $deltaSortOrder);
        // We could be looking at this relationship from src->trg or trg->src. The srcTicketId
        // passed in is always the LH id in that relationhsip. Therefore we have to try the first
        // one (src->trg), and if we don't find a record, try the second (trg->src).
        $matchingSortOrder = ($this->src_ticket_id == $srcTicketId ? $this->src_sort_order : $this->trg_sort_order)
                             + $deltaSortOrder;
        Log::debug('Matching sort order is: ' . $matchingSortOrder);
        $isSrc = true;
        $swapWith = static::where('src_ticket_id', '=', $srcTicketId)
                        ->where('src_sort_order', '=', $matchingSortOrder)
                        ->first();
        if ($swapWith == null) {
            Log::debug('Next one not found in src');
            $isSrc = false;
            $swapWith = static::where('trg_ticket_id', '=', $srcTicketId)
                            ->where('trg_sort_order', '=', $matchingSortOrder)
                            ->first();
        }
        // If we still don't find anything, the data or parameters are wrong, so give up!
        if ($swapWith != null) {
            Log::debug('Found one to swap with -' . $swapWith->id . '-. The one that was clicked...');
            // Update either the src or trg sort order. We know which one by whether the ticket id
            // passed in matches the src or trg id.
            if ($this->src_ticket_id == $srcTicketId) {
                Log::debug('Changing src order. Was: ' . $this->src_sort_order);
                $this->src_sort_order += $deltaSortOrder;
            } else {
                Log::debug('Changing trg order. Was: ' . $this->trg_sort_order);
                $this->trg_sort_order += $deltaSortOrder;
            }
            $this->save();
            // Update the src or trg sort order of the record we're going to swap it with.
            // We know which column by how we found the record.
            Log::debug('Now the matching record. id: ' . $swapWith->id);
            if ($isSrc) {
                Log::debug('Changing src order. Was: ' . $swapWith->src_sort_order);
                $swapWith->src_sort_order -= $deltaSortOrder;
            } else {
                Log::debug('Changing trg order. Was: ' . $swapWith->trg_sort_order);
                $swapWith->trg_sort_order -= $deltaSortOrder;
            }
            $swapWith->save();
        }
        Log::debug('---');
    }
    
    /**
     * Method to attempt to create a relationship between two tickets.
     * @TODO: I think this can be combined with createRecord above - just need to sort out the return values:
     *        Store the error in an array and use a getter to retrieve them.
     *
     * @param string $relationshipType The type of the relationship.
     * @param int $srcTicketId The source ticket id
     * @param int $trgTicketId The target ticket id
     *
     * @return bool Whether the comment was valid or not.
     */
    public static function attemptToCreate($relationshipType, $srcTicketId, $trgTicketId) {
        // Do a quick sanity check on the arguments.
        if (! ($relationshipType != '' && $trgTicketId > 0)) {
            return 'You must choose a relationship type and a ticket.';
        }
        if ($srcTicketId == $trgTicketId) {
            return 'A ticket can\'t be related to itself.';
        }
        // Check this relationship doesn't already exist. We use the view, which looks at
        // relationships going both ways.
        $exists = TicketAssociationView::where('relationship_type', '=', $relationshipType)
                    ->where('src_ticket_id', '=', $srcTicketId)
                    ->where('trg_ticket_id', '=', $trgTicketId)
                    ->get();
        if (count($exists) > 0) {
            return 'This relationship already exists.';
        }
        // Check that adding this relationship won't cause a dependency loop.
        Log::debug('The relationship type is: ' . $relationshipType);
        if (! static::checkDependencyLoop($relationshipType, $srcTicketId, $trgTicketId)) {
            return 'Adding this relation will cause a circular dependency.';
        }
        // Check that adding this relationship won't cause a problem with the hierarchy of statuses.
        if (! static::checkStatusHierarchy($relationshipType, $srcTicketId, $trgTicketId)) {
            return 'Adding this relation will cause a problem with statuses (a ticket cannot be in a closed state if any of its descendants are in an open state, and vice versa).';
        }
        // Create the record in the DB.
        $srcSortOrder = static::where('src_ticket_id', '=', $srcTicketId)
                            ->orWhere('trg_ticket_id', '=', $srcTicketId)
                            ->count();
        $srcSortOrder = ($srcSortOrder == null ? 1 : 1 + $srcSortOrder);
        $trgSortOrder = static::where('src_ticket_id', '=', $trgTicketId)
                            ->orWhere('trg_ticket_id', '=', $trgTicketId)
                            ->count();
        $trgSortOrder = ($trgSortOrder == null ? 1 : 1 + $trgSortOrder);
        $relation = new TicketAssociation();
        $relation->relationship_type = $relationshipType;
        $relation->src_ticket_id = $srcTicketId;
        $relation->src_sort_order = $srcSortOrder;
        $relation->trg_ticket_id = $trgTicketId;
        $relation->trg_sort_order = $trgSortOrder;
        $relation->pinned = 1;
        $relation->save();
        return true;
    }
    
    public static function allowed($relationshipId, $operation, $ticketId = null) {
        $ticket = Ticket::find($ticketId);
        if (Ticket::allowed($ticketId, 'activity', $ticket->project->id)) {
            return true;
        }
        return false;
    }
    
}
