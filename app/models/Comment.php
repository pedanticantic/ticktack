<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Comment extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * Contains error messages when saving a ticket.
     *
     * @var array
     */
    static $commentErrors;
    
    /**
     * Fundamental validation rules.
     *
     * @var arrays
     */
    public static $rules = array(
        'ticket_id' => 'required|integer',
        'added_by_user_id' => 'required|integer',
        'details' => 'required'
    );
    
    public function isValid()
    {
        // Clear any existing errors.
        static::$commentErrors = [];
        // make a new validator object, using the values in the model.
        $v = Validator::make($this->toArray(), static::$rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            foreach($v->messages()->all() as $message) {
                static::$commentErrors[] = $message;
            }
            return false;
        }
        
        // Validation passed.
        return true;
    }
    
    /**
     * Method to define the relationship between a comment and the user who created it.
     */
    public function user() {
        return $this->belongsTo('User', 'added_by_user_id');
    }
    
    /**
     * Method to return the comment details ready to be displayed (in view mode) on screen.
     * It invokes the markdown, etc.
     *
     * @return string the comments after having been parsed by the markdown parser.
     */
    public function detailsForView() {
        return markdownHandler::toText($this->details);
    }
    
    /**
     * Method to return a set of comments for the API, after applying any user-defined filtering.
     * It uses the Input fields & values for the filter.
     *
     * @param int $ticket_id Optional. Used in the filter if populated.
     *
     * @return Collection of comments matching the filter (if any), sorted by id.
     */
    public static function apiFilter($ticket_id = null) {
        // Order the results by id, then implement any filters.
        $comments = Comment::orderBy('id');
        $ticketFilter = ($ticket_id > 0 ? $ticket_id : (Input::has('ticket_id') ? Input::get('ticket_id') : null));
        if ($ticketFilter != null) {
            $comments = $comments->where('ticket_id', '=', $ticketFilter);
        }
        if (Input::has('added_by_user_id')) {
            $comments = $comments->where('added_by_user_id', '=', Input::get('added_by_user_id'));
        }
        if (Input::has('details')) {
            $comments = $comments->where('details', 'like', '%' . Input::get('details') . '%');
        }
        $comments = $comments->get();
        
        return $comments;
    }
    
    /**
     * Method to return this comment in a suitable format for the calling process, as determined
     * by the mode. By default the comment is converted to an array.
     *
     * @param string $mode Defines the format that the calling process wants.
     *
     * @return array The comment as an array, with any mode-specific modifications.
     */
    public function format($mode = null) {
        $result = $this->toArray();
        if ($mode == 'api') {
            // Include the user name of the "added by" person.
            $result['created_at'] = date('c', strtotime($this->created_at));
            $result['added_by_user_name'] = $this->user->username;
            $result['details_markdown'] = $this->detailsForView();
        }
        return $result;
    }
    
    public static function allowed($commentId, $operation, $ticketId = null) {
        $ticket = Ticket::find($ticketId);
        if (Ticket::allowed($ticketId, 'activity', $ticket->project->id)) {
            return true;
        }
        return false;
    }
    
}
