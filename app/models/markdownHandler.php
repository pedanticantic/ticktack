<?php

/**
 * This is a wrapper around the Parsedown class. It will implement the things we need
 * every time we invoke markdown. It also means Parsedown could easily be swapped out for
 * something else if it were ever necessary.
 */
class markdownHandler{
    
    /**
     * Method to parse the given text using the markdown rules.
     * We assume we are enabling breaks and escaping other markup.
     *
     * @param string $copy The text to convert.
     *
     * @return string The copy after conversion.
     */
    public static function toText($copy) {
		// We get an instance of Parsedown.
		// Then tell it to escape breaks - this means we don't have to output stuff
		// around <pre > tags.
		// Then tell it to escape markup - this means we don't have to apply htmlentities
		// to everything.
		// Then we give it the copy to process, and we return the result.
		return Parsedown::instance()
			->setBreaksEnabled(true)
			->setMarkupEscaped(true)
			->text($copy);
	}

}
