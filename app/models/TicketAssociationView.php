<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * This is a model based on the DB view "tickets_associations_view".
 */
class TicketAssociationView extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * The underlying table name.
     *
     * @var string
     */
    protected $table = 'tickets_associations_view';
    
    /**
     * Method to return an English relationship between the target and source tickets for this record.
     *
     * @return string The text that would replace the dots here: "trg ticker is ... src ticket".
     */
    public function getRelationshipTypeForView() {
        if ($this->relationship_type == '') {
            return '';
        }
        $conv = static::getRelationshipTypesForView();
        return $conv[$this->relationship_type];
    }
    
    /**
     * Method to return an array of English relationships between the target and source tickets.
     *
     * @return array Indexed by relationship type; value is the English version.
     */
    public static function getRelationshipTypesForView() {
        return ['associated' => 'associated with',
                'dependent' => 'a dependency of',
                'dependency' => 'dependent upon',
                'parent' => 'a child of',
                'child' => 'a parent of'];
    }
    
    /**
     * Method to define the relationship between the association table and the source ticket.
     * We have to specify the foreign key column, because there are two relationships from the
     * assoc table to the tickets table (src & trg ticket ids).
     */
    public function srcTicket() {
        return $this->hasOne('Ticket', 'id', 'src_ticket_id');
    }
    
    /**
     * Method to define the relationship between the association table and the target ticket.
     * We have to specify the foreign key column, because there are two relationships from the
     * assoc table to the tickets table (src & trg ticket ids).
     */
    public function trgTicket() {
        return $this->hasOne('Ticket', 'id', 'trg_ticket_id');
    }
    
    /**
     * Method to return the move up/down and pin/unpin options for a related ticket on the
     * ticket view screen.
     *
     * @param $index The index of the line in the list (don't show "move up" for the first line).
     * @param $lineCount The total number of lines (don't show "move down" for the last line).
     *
     * @return string The contents of the options cell.
     */
    public function getTicketRelationOptions($index, $lineCount) {
        $options = '';
        $blank = '<span style="width: 13px; display: inline-block;" >&nbsp;</span>';
        $move_url = '/ticket/relation/' . $this->id . '/' . $this->src_ticket_id . '/';
        
        if ($index > 0) {
            $options .= '<a href="' . $move_url . 'up" title="Move this ticket up" ><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>';
        } else {
            $options .= $blank;
        }
        
        if ($index < $lineCount - 1) {
            $options .= '<a href="' . $move_url . 'down" title="Move this ticket down" ><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>';
        } else {
            $options .= $blank;
        }
        
        // Don't allow unpin if this was explicitly added.
        if ($this->pinManagementAllowed()) {
            if ($this->pinned) {
                $options .= '<a href="' . $move_url . 'unpin" title="Unpin this relationship" ><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></a>';
            } else {
                $options .= '<a href="' . $move_url . 'pin" title="Pin this relationship" ><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></a>';
            }
        } else {
            $options .= $blank;
        }
        
        // Don't allow delete if this was implicitly added (ie referenced in either description).
        // The confirm class is picked up by JavaScript, and pops up a confirmation box before
        // actually deleting it.
        if ($this->deleteAllowed()) {
            $options .= '<a href="' . $move_url . 'delete" title="Delete this relationship" class="confirm-delete-relationship" ><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
        } else {
            $options .= $blank;
        }
        
        return $options;
    }
    
    /**
     * Method to check whether the user can manage pinning this relationship. If the relationship
     * record was entered explicitly, pinning makes no sense. So, if the (exact) relationship
     * is referenced in either ticket's description, return false; otherwise return true.
     *
     * @return bool True if the user can pin/unpin, false otherwise.
     */
    private function pinManagementAllowed() {
        return ! $this->wasAddedExplicitly();
    }
    
    /**
     * Method to check whether the user can delete this relationship. If the relationship
     * record was entered explicitly, deletion is allowed. So, if the (exact) relationship
     * is referenced in either ticket's description, return false; otherwise return true.
     *
     * @return bool True if the user can delete, false otherwise.
     */
    private function deleteAllowed() {
        return $this->wasAddedExplicitly();
    }
    
    /**
     * Method to check whether this relationship record was entered explicitly.
     * If the (exact) relationship is referenced in either ticket's description,
     * it was NOT added explicitly.
     * Note: we don't simply have a flag in the record because it's possible for a record
     * to be added explicitly, then a user to reference it in the description, rendering it
     * a "non-expliclty added relation".
     * @TODO: This could be made more efficient by caching the results.
     *
     * @return bool True if the user can pin/unpin, false otherwise.
     *
     */
    private function wasAddedExplicitly() {
        // We must look for the exact relation. The "trg" one will be the reverse relation, of course.
        // Look in the "src" ticket, then the "trg" one.
        $relatedTickets = Ticket::whereIn('id', [$this->src_ticket_id, $this->trg_ticket_id])->get();
        foreach($relatedTickets as $relatedTicket) {
            // We can simply do "description LIKE '%<ref>%'" in case it matches a reference to a ticket
            // that starts with the same digits. Eg "description LIKE '%>#123%'" will match
            // "blah >#1234 blah".
            // So, we get an array of the exact references, and see if ours is in it.
            $references = $relatedTicket->getReferencedTicketInfo();
            if ($relatedTicket->id == $this->src_ticket_id) {
                $reference = TicketAssociation::relToCode($this->relationship_type) . '#' . $this->trg_ticket_id;
            } else {
                $reference = TicketAssociation::relToCodeReverse($this->relationship_type) . '#' . $this->src_ticket_id;
            }
            if (in_array($reference, $references)) {
                return false;
            }
        }
        // Well, we didn't find it, so return false.
        return true;
    }
    
    /**
     * Method to return the given relationship on the given ticket.
     *
     * @param int $ticketId The source ticket
     * @param int $relationshipId The relationship
     *
     * @return TicketAssociationView.
     */
    public static function getRelationship($ticketId, $relationshipId) {
        return static::where('src_ticket_id', $ticketId)
                ->where('id', $relationshipId)
                ->first();
    }
    
    /**
     * Method to return this relationship in a suitable format for the calling process, as
     * determined by the mode. By default the relationship is converted to an array.
     *
     * @param string $mode Defines the format that the calling process wants.
     *
     * @return array The relationship as an array, with any mode-specific modifications.
     */
    public function format($mode = null) {
        $result = $this->toArray();
        if ($mode == 'api') {
            // I want to change most of the keys, but preserve order.
            $tmp = [];
            $tmp['id'] = $result['id'];
            $tmp['relationship_type'] = $result['relationship_type'];
            $tmp['source_ticket_id'] = $result['src_ticket_id'];
            $tmp['source_sort_order'] = $result['src_sort_order'];
            $tmp['target_ticket_id'] = $result['trg_ticket_id'];
            $tmp['target_sort_order'] = $result['trg_sort_order'];
            $tmp['is_pinned'] = $result['pinned'];
            $result = $tmp;
        }
        return $result;
    }
    
    /**
     * Method to return a set of relationships for the API, after applying any user-defined filtering.
     * It uses the Input fields & values for the filter.
     *
     * @param int $ticket_id Optional. Used in the filter if populated.
     *
     * @return Collection of relationships matching the filter (if any), sorted by id.
     */
    public static function apiFilter($ticket_id = null) {
        // Order the results by source ticket & sort order, then implement any filters.
        $relationships = static::orderBy('src_ticket_id')->orderBy('src_sort_order');
        $ticketFilter = ($ticket_id > 0 ? $ticket_id : (Input::has('ticket_id') ? Input::get('ticket_id') : null));
        if ($ticketFilter != null) {
            $relationships = $relationships->where('src_ticket_id', '=', $ticketFilter);
        }
        if (Input::has('relationship_type')) {
            $relationships = $relationships->where('relationship_type', Input::get('relationship_type'));
        }
        if (Input::has('target_ticket_id')) {
            $relationships = $relationships->where('trg_ticket_id', '=', Input::get('target_ticket_id'));
        }
        if (Input::has('is_pinned')) {
            $relationships = $relationships->where('pinned', (Input::get('is_pinned') ? 1 : 0));
        }
        $relationships = $relationships->get();
        
        return $relationships;
    }
    
}
