<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Ticket extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * Contains error messages when saving a ticket.
     *
     * @var array
     */
    static $ticketErrors;
    
    /**
     * Says whether the due date/time is valid or not.
     *
     * @var boolean
     */
    private $dueDateIsValid = true;
    
    /**
     * Says whether the estimated length is valid or not.
     *
     * @var boolean
     */
    private $estimatedLengthIsValid = true;
    
    /**
     * Users can save a new assignment when saving a ticket. We remember it here while
     * we validate everything.
     *
     * @var integer
     */
    private $assignmentId;
    
    /**
     * Says whether the potential assignment is valid.
     *
     * @var boolean
     */
    private $assignmentIsValid = true;
    
    /**
     * Fundamental validation rules.
     * @TODO: We need to include the allowed values for ticket type, priority, etc.
     *
     * @var arrays
     */
    public static $rules = array(
        'project_id' => 'required|integer',
        'ticket_type' => 'required',
        'summary' => 'required|min:5|max:60',
        'description' => 'required',
        'created_by_user_id' => 'required|integer',
        'priority' => 'required',
        'status' => 'required',
        'estimated_length_seconds' => 'integer'
    );
    public static $messages = array(
        'required' => 'The :attribute must be entered/selected.',
    );
    
    public function isValid()
    {
        // Clear any existing errors.
        static::$ticketErrors = [];
        // make a new validator object, using the values in the model.
        $v = Validator::make($this->toArray(), static::$rules, static::$messages);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            foreach($v->messages()->all() as $message) {
                static::$ticketErrors[] = $message;
            }
            return false;
        }
        
        // Check that the due date/time is valid.
        if (!$this->dueDateIsValid) {
            static::$ticketErrors[] = 'Due Date/Time is not valid.';
            return false;
        }
        
        // Check that the estimated length is valid.
        if (!$this->estimatedLengthIsValid) {
            static::$ticketErrors[] = 'Estimated length is not valid.';
            return false;
        }
        
        // If a new assignment is present and invalid, add the error.
        if ($this->assignmentIsValid === false) {
            static::$ticketErrors[] = 'The ticket is already assigned to that user.';
            return false;
        }
        
        // If the fields are valid, check that the ticket doesn't already exist.
        $exists = Ticket::where('summary', '=', $this->summary);
        if ($this->id > 0) {
            // If we're saving an existing record, don't compare the name with itself.
            $exists = $exists->where('id', '<>', $this->id);
        }
        $exists = $exists->first();
        if (count($exists) > 0) {
            static::$ticketErrors[] = 'Another ticket has the same summary.';
            return false;
        }
        
        // If the ticket already exists, check that its status is valid given the
        // ticket's relationships.
        if ($this->id > 0) {
            // If the status has changed from "open" to "closed", check
            // the statuses of its ancestors/descendants as appropriate.
            if (Ticket::isOpenStatus($this->status)) {
                if ($this->isClosed()) {
                    if (!$this->allAncestorsAreOpen()) {
                        static::$ticketErrors[] = 'Cannot (re-)open this issue unless all its ancestor tickets are open.';
                        return false;
                    }
                }
            } else {
                if ($this->isOpen()) {
                    if (!$this->allDescendantsAreClosed()) {
                        static::$ticketErrors[] = 'Cannot close this issue until all its descendant tickets are closed.';
                        return false;
                    }
                }
            }
        }
        
        // Validation passed.
        return true;
    }
    
    /**
     * Method to set the due date/time. A human-readable value in the form DD/MM/YYYY HH24:MI:SS
     * is passed in, and it's converted to an ISO date/time.
     *
     * @param string The new due date/time.
     */
    public function setDueDatetimeAttribute($newDueDate) {
        $dbDueDate = null;
        $this->dueDateIsValid = false;
        if ($newDueDate == '') {
            $this->dueDateIsValid = true;
        } else {
            if (DatetimeManager::datetimeIsValidUK($newDueDate)) {
                $this->dueDateIsValid = true;
                $dbDueDate = DatetimeManager::datetimeUKToString($newDueDate);
            }
        }
        $this->attributes['due_datetime'] = $dbDueDate;
    }
    
    /**
     * Method to set the estimated length, in seconds. A coded value is passed in,
     * and we convert it into a number of seconds.
     *
     * @param string The estimated length in human readable form.
     */
    public function setEstimatedLengthSecondsAttribute($newLength) {
        $this->estimatedLengthIsValid = true;
        $dbLength = DatetimeManager::englishToInterval($newLength);
        if ($dbLength === false) {
            $this->estimatedLengthIsValid = false;
            $dbLength = null;
        }
        $this->attributes['estimated_length_seconds'] = $dbLength;
    }
    
    /**
     * Method to return an array of all ticket types.
     *
     * @return array An array of ticket types.
     */
    public static function allTypes() {
        $types = ['new feature', 'task', 'bug', 'improvement'];
        $allTypes = [];
        foreach($types as $type) {
            $allTypes[$type] = ucfirst($type);
        }
        return $allTypes;
    }
    
    /**
     * Method to return an array of all valid priorities.
     *
     * @return array An array of priorities.
     */
    public static function getAllPriorities() {
        $priorities = ['very high','high','medium','low','very low'];
        $allPriorities = [];
        foreach($priorities as $priority) {
            $allPriorities[$priority] = ucfirst($priority);
        }
        return $allPriorities;
    }
    
    /**
     * Method to return an array of all valid priorities.
     *
     * @param bool $withSpaces Says whether to return the status codes with or without spaces.
     *
     * @return array An array of priorities.
     */
    public static function getAllStatuses($withSpaces = true) {
        $statuses = ['new','assigned','in progress','resolved','closed'];
        $allStatues = [];
        foreach($statuses as $status) {
            $statusCode = $withSpaces ? $status : str_replace(' ', '', $status);
            $allStatues[$statusCode] = ucfirst($status);
        }
        return $allStatues;
    }
    
    /**
     * Method to set an assignment id for saving when the ticket is saved.
     * The assignents are stored in a separate table, but the edit screen includes
     * a field for assigning a user.
     *
     * @param int $userId The id of the user to whom this ticket will additionally be assigned. Can be null.
     */
    public function setAssignment($userId) {
        $this->assignmentId = $userId;
        $this->assignmentIsValid = true;
        // If the ticket already exists, check that the assignment is valid (ie that the
        // ticket is not already assigned to this user).
        if ($this->id > 0 && $this->assignmentId > 0) {
            if (TicketAssignment::alreadyAssigned($this->id, $this->assignmentId)) {
                $this->assignmentIsValid = false;
            }
        }
    }
    
    /**
     * An override of the Eloquent save method. We do the normal save, but then save
     * any additional assignment, and also check the description for new (or removed)
     * ticket references.
     * @TODO: Improve this using proper event listeners/handlers.
     *
     * @param array $options Options to pass in to the parent save method.
     */
    public function save(array $options = array())
    {
        parent::save($options);
        // If there is an assignment pending, save that now.
        if ($this->assignmentId > 0) {
            TicketAssignment::assignUserToTicket($this->id, $this->assignmentId);
        }
        // Refresh any relationships between tickets referenced in the description.
        $this->processDescriptionTicketReferences();
    }
    
    /**
     * Method to return the errors from the last attempt to save a ticket.
     *
     * @return array The list of errors.
     */
    public static function getSaveErrors() {
        return static::$ticketErrors;
    }
    
    /**
     * Method to display the given ticket.
     *
     * @param int $id A ticket id.
     *
     * @return mixed false if the ticket is invalid; the ticket object if valid.
     */
    public static function getTicket($id) {
        $ticket = Ticket::with('project')->find($id);
        if ($ticket == null) {
            return false;
        }
        return $ticket;
    }
    
    /**
     * Method to define the relationship between a ticket and its creator user.
     */
    public function createdByUser() {
        return $this->belongsTo('User', 'created_by_user_id');
    }
    
    /**
     * Method to define the relationship between a ticket and its project.
     */
    public function project() {
        return $this->belongsTo('Project', 'project_id');
    }
    
    /**
     * Method to return the type in a user-friendly format
     *
     * @return The ticket type with the first letter capitalised.
     */
    public function typeToScreen() {
        return ucfirst($this->ticket_type);
    }
    
    /**
     * Method to return the name of the image icon that represents the ticket status.
     *
     * @return string Relative path to the image.
     */
    public function getTypeImageObject() {
        // @TODO: We should check that the image file exists - if not, return one with a question mark in it.
        return '<img ' .
                 'src="/images/ticket_type/' . str_replace(' ', '', $this->ticket_type) . '.png" ' .
                 'title="' . $this->typeToScreen() . '" ' .
               '/>';
    }
    
    /**
     * Method to return the screen (user-friendly) value of the priority.
     *
     * @return string The priority with first letter capitalised.
     */
    public function priority_db_to_screen() {
        return ucfirst($this->priority);
    }
    
    /**
     * Method to return the screen (user-friendly) value of the status.
     *
     * @return string The status with first letter capitalised.
     */
    public function status_db_to_screen() {
        return ucfirst($this->status);
    }
    
    /**
     * Method to return the screen (user-friendly) value of the due date/time.
     *
     * @return string The due date/time.
     */
    public function due_datetime_to_screen() {
        return DatetimeManager::datetimeISOToUK($this->due_datetime);
    }
    
    /**
     * Method to return the screen (user-friendly) value of the estimated length.
     *
     * @return string The estimated length in the nice coded format.
     */
    public function length_db_to_screen() {
        return DatetimeManager::intervalToEnglish($this->estimated_length_seconds);
    }
    
    /**
     * Method to define the relationship between a ticket and its comments.
     */
    public function comments() {
        return $this->hasMany('Comment', 'ticket_id');
    }
    
    /**
     * Method to define the relationship between a ticket and its assignees.
     */
    public function assignments() {
        // Explicitly order by the sort order, as that's what we'll want pretty-much all the time.
        return $this->hasMany('TicketAssignment', 'ticket_id')->orderBy('sort_order', 'ASC');
    }
    
    /**
     * Method to define the relationship between a ticket and its associated tickets.
     */
    public function associated() {
        return $this->hasMany('TicketAssociationView', 'src_ticket_id')->orderBy('src_sort_order');
    }
    
    /**
     * Method to define the relationship between a ticket and its assignees, but sorted in reverse.
     */
    public function assignmentsReverse() {
        // Explicitly order by the sort order descending.
        return $this->hasMany('TicketAssignment', 'ticket_id')->orderBy('sort_order', 'DESC');
    }
    
    /**
     * Method to define the relationship between a ticket and its time-spent records.
     */
    public function timeSpent() {
        return $this->hasMany('TimeSpent', 'ticket_id');
    }
    
    /**
     * Method to return the ticket description ready to be displayed (in view mode) on screen.
     * It sorts out links to referenced tickets, invokes the markdown, etc.
     *
     * @return string The Ticket type with the first letters initialised.
     */
    public function typeForView() {
        // Take the raw type and make it look nice - basically apply ucwords to it.
        return ucwords($this->ticket_type);
    }
    
    /**
     * Method to return the ticket description ready to be displayed (in view mode) on screen.
     * It sorts out links to referenced tickets, invokes the markdown, etc.
     *
     * @return string The description after having been parsed by the Markdown parser.
     */
    public function descriptionForView() {
        // We take the description, replace any references to other tickets with links
        // (in the parsedown style), then apply the rendering (markdown), and return the
        // result.
        return markdownHandler::toText($this->ticketRefsToLinks());
    }

    /**
     * Method that takes the ticket description, and replaces references to other tickets
     * with fancy link to those tickets. "Fancy" means things like a tooltip with the
     * ticket summary.
     * It assumes the result will be processed by Parsedown, so the links need to be output
     * in that format.
     *
     * @return string The description with ticket references replaced by links.
     */
    public function ticketRefsToLinks() {
        // The format for a reference is:
        //  one of '=', '>', '<', '+', '-'; followed by '#'; followed by the ticker number.
        
        // Ask the private method to do the splitting of the description on ticket reference.
        $parts = $this->splitOnTicketReference();
        
        // "Parts" will be an array of alternating text values: copy, reference, copy, reference, copy.
        // We will look at every odd-indexes element, convert it into a link.
        // We also make sure the ticket exists - if not, we leave the text exactly as it was.
        for($refidx = 1 ; $refidx < count($parts) ; $refidx += 2) {
            $ticketId = substr($parts[$refidx], 2);
            $ticket = Ticket::find($ticketId);
            if ($ticket != null) {
                // The ticket exists. Build the link (using markdown syntax) and include the ticket
                // summary as the title.
                $parts[$refidx] = '[' . substr($parts[$refidx], 0, 2) . $ticketId . ']' .
                                  '(/ticket/' . $ticketId . ' "' . $ticket->summary . '")';
            }
        }

        // Now combine the whole lot back together again into a single string and return it.
        $details = implode('', $parts);
        return $details;
    }
    
    /**
     * Method to return an array of tickets referenced in the description of this ticket.
     *
     * @return array All the ticket references, of the form "<type>#<ticket_id>".
     */
    public function getReferencedTicketInfo() {
        // Call the method to break the description up into text, reference, text, reference, text, etc.
        // Then remove the even numbered entries and return what's left.
        $splitDesc = $this->splitOnTicketReference();
        foreach($splitDesc as $index => $dummy) {
            if ($index % 2 == 0) {
                unset($splitDesc[$index]);
            }
        }
        return $splitDesc;
    }
    
    /**
     * Method to take a ticket description, and split it into an array using ticket references
     * as the delimiter. We want to return the matched ticket reference too.
     *
     * @return array Text, ticket reference, text, ticket reference, text, ticket reference, text...
     */
    private function splitOnTicketReference() {
        // Use preg_split to split the text up using the referencing rules as a "separator".
        return preg_split('/([~|>|<|\+|-]\#[0-9]+)/', $this->description, -1, PREG_SPLIT_DELIM_CAPTURE);
    }
    
    /**
     * Method to output the list of users assigned to this ticket.
     *
     * @param int $includeRemoveOption Whether we should show the "remove" option or not.
     *
     * @return string The appropriate html.
     */
    public function assignedForView($includeRemoveOption = false) {
        $list = '';
        $assignments = $this->assignments;  // They are returned sorted by sort_order.
        if (count($assignments) > 0) {
            $list .= '<table class="assignments" >';
            foreach($assignments as $assidx => $assignment) {
                $list .= '<tr >';
                // Primary/other indicator.
                $list .= '<td >';
                if ($assignment->sort_order == 1) {
                    $list .= '<span class="label label-primary">Primary</span> ';
                } else {
                    $list .= '<span class="label label-info" >Other</span> ';
                }
                $list .= '</td>';
                // The move up/down buttons. I'm being naughty and re-using the "include remove option" flag.
                if ($includeRemoveOption) {
                    $move_url = '/assignment/' . $assignment->id . '/';
                    $list .= '<td >';
                    if ($assidx > 0) {
                        $list .= '<a href="' . $move_url . 'up" title="Move this assignment up" ><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>';
                    } else {
                        $list .= '<br />';
                    }
                    $list .= '</td>';
                    $list .= '<td >';
                    if ($assidx < count($assignments) - 1) {
                        $list .= '<a href="' . $move_url . 'down" title="Move this assignment down" ><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>';
                    } else {
                        $list .= '<br />';
                    }
                    $list .= '</td>';
                }
                // The actual username.
                $list .= '<td >';
                $list .= $assignment->user->username;
                $list .= '</td>';
                // The remove option.
                if ($includeRemoveOption) {
                    $list .= '<td >';
                    $list .= '<a class="confirm-delete-assignment label label-danger" href="/assignment/' . $assignment->id . '/remove" title="Remove this assignment" >X</a>&nbsp;';
                    $list .= '</td>';
                }
                $list .= '</tr>';
            }
            $list .= '</table>';
        }
        return $list;
    }
    
    /**
     * Method to return the username of the primary assignee of this ticket (if any).
     *
     * @return string The username of the primary assignee, if any.
     */
    public function primaryAssignee() {
        $primary = $this->assignments()->orderBy('sort_order')->first();
        if ($primary == null) {
            return null;
        }
        return $primary->user->username;
    }
    
    /**
     * Method to look at the description of this ticket, pick out any referenced tickets (and the
     * relationships), then make sure the link records are updated accordingly.
     */
    private function processDescriptionTicketReferences() {
        // Ask the private method to do the splitting of the description on ticket reference.
        $parts = $this->splitOnTicketReference();
        
        // Look at the odd-numbered elements from the array, and build a new 2-d array indexed by ticket id
        // and relationship type. Yes, we have to take account of the user referencing the
        // same ticket twice with different relationships! We store "false" against each entry. Later
        // we set any to true that already exist in the database. Then for any that are still false,
        // we create the DB record.
        // Note: We still have to do all this even if no tickets are referenced because we might have
        // to delete link records that are now obsolete.
        $referenced = [];
        for($index = 0 ; $index < count($parts) ; $index++) {
            if ($index % 2 == 1) {
                $info = explode('#', $parts[$index]);
                if (! array_key_exists($info[1], $referenced)) {
                    $referenced[$info[1]] = [];
                }
                $referenced[$info[1]][$info[0]] = false;
            }
        }
        
        // Now retrieve all the referenced tickets in the database. Of course, they might be referenced
        // _from_ this ticket, or _by_ another ticket.
        $currentRefs = TicketAssociation::getReferences($this->id);
        
        // Loop through the current records. For each one...
        // If the entry exists in the array generated from the descrition, set it's value to "true". Ie
        // don't create a DB record.
        // If it's not pinned and is referenced from this ticket, and the "other" ticket doesn't exist
        // in the "referenced" list, remove it from the database.
        foreach($currentRefs as $currentRef) {
            $relCode = TicketAssociation::relToCode($currentRef->relationship_type);
            $inDesc = array_key_exists($currentRef->trg_ticket_id, $referenced);
            if ($inDesc) {
                // The ticket was referenced in the description. See if the relationship is the same.
                $inDesc = array_key_exists($relCode, $referenced[$currentRef->trg_ticket_id]);
            }
            if ($inDesc) {
                // The entry in the description exists in the database, so flag it as not needing to
                // be inserted.
                $referenced[$currentRef->trg_ticket_id][$relCode] = true;
            } else {
                // DB record isn't referenced in the desc, so if the DB record is not pinned and this
                // ticket is the source, then delete it!
                if (! $currentRef->pinned && $currentRef->src_ticket_id == $this->id) {
                    // This needs to be a method because we need to update subsequent sort orders.
                    $currentRef->delete();
                }
            }
        }
        
        // Now, any referenced tickets/relationships from the description that are still marked
        // as false are not in the database, so we need to create the records. We call a method
        // in the class because the sort-orders are not trivial to determine.
        foreach($referenced as $trgTicketId => $relationships) {
            foreach($relationships as $relationshipCode => $inDB) {
                if (! $inDB) {
                    Log::debug('Creating relationship: ' . $relationshipCode . ', ' . $this->id . ', ' . $trgTicketId);
                    if (! TicketAssociation::createRecord($relationshipCode, $this->id, $trgTicketId)) {
                        Log::debug('Creation failed');
                        // There was a problem with creating the relationship. Eg it would cause a
                        // dependency loop. So, "tweak" the reference in the ticket description to
                        // indicate something went wrong.
                        $this->description = str_replace($relationshipCode . '#' . $trgTicketId, $relationshipCode . '#[' . $trgTicketId . ']', $this->description );
                        $this->save();
                    }
                }
            }
        }
        Log::debug('All done');
        
    }
    
    /**
     * Method to return the comment data for the ticket view.
     *
     * @return array A set of zero or more comment models.
     */
    public function getCommentsForView() {
        return $this->comments()->orderBy('created_at', 'DESC')->get();
    }
    
    /**
     * Method to return the time spent data for the ticket view.
     *
     * @return array A set of zero or more time-spent models.
     */
    public function getTimesSpentForView() {
        return $this->timeSpent()->orderBy('start_datetime', 'DESC')->get();
    }
    
    /**
     * Method to return the name of the image icon that represents the ticket status.
     *
     * @return string Relative path to the image.
     */
    public function getStatusImageObject() {
        // @TODO: We should check that the image file exists - if not, return one with a question mark in it.
        return '<img ' .
                 'src="/images/ticket_status/' . str_replace(' ', '', $this->status) . '.svg" ' .
                 'title="' . ucfirst($this->status) . '" ' .
               '/>';
    }
    
    /**
     * Method to check that no descendant records are open.
     *
     * @return bool True if all descendant records are closed; false otherwise.
     */
    public function allDescendantsAreClosed() {
        foreach($this->childTickets() as $childTicket) {
            if ($childTicket->isOpen() || !$childTicket->allDescendantsAreClosed()) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Method to check that no ancestor records are closed.
     *
     * @return bool True if all descendant records are closed; false otherwise.
     */
    public function allAncestorsAreOpen() {
        foreach($this->parentTickets() as $parentTicket) {
            if ($parentTicket->isClosed() || !$parentTicket->allAncestorsAreOpen()) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Method to return all the "child" tickets of this ticket.
     *
     * @return array All the child tickets.
     */
    public function childTickets() {
        Log::debug('Get children of: ' . $this->id);
        // We need to go via the relationship view, so it's not particularly efficient.
        $children = TicketAssociationView::where('src_ticket_id', '=', $this->id)
                        ->where('relationship_type', '=', 'child')
                        ->get();
        $result = [];
        foreach($children as $child) {
            Log::debug('Found: ' . $child->trg_ticket_id);
            $result[] = $child->trgTicket;
        }
        return $result;
    }
    
    /**
     * Method to return all the "parent" tickets of this ticket.
     *
     * @return array All the parent tickets.
     */
    public function parentTickets() {
        Log::debug('Get parents of: ' . $this->id);
        // We need to go via the relationship view, so it's not particularly efficient.
        $parents = TicketAssociationView::where('src_ticket_id', '=', $this->id)
                        ->where('relationship_type', '=', 'parent')
                        ->get();
        $result = [];
        foreach($parents as $parent) {
            Log::debug('Found: ' . $parent->trg_ticket_id);
            $result[] = $parent->trgTicket;
        }
        return $result;
    }
    
    // Some methods to do with determining whether a status is deemed to be "open" or "closed".
    
    /**
     * Method to determine whether the given status is open or not.
     *
     * @param string $statusType Status type.
     *
     * @return bool True if the status is an "open" one, false otherwise.
     */
    public static function isOpenStatus($statusType) {
        $openStatuses = ['new','assigned','in progress'];
        return in_array($statusType, $openStatuses);
    }
    
    /**
     * Method to determine whether the given status is closed or not.
     * We simply return the opposite of isOpenStatus().
     *
     * @param string $statusType Status type.
     *
     * @return bool True if the status is a "closed" one, false otherwise.
     */
    public static function isClosedStatus($statusType) {
        return ! static::isOpenStatus($statusType);
    }
    
    /**
     * Method to determine whether the status of this record is open or not.
     *
     * @return bool True if the status is an "open" one, false otherwise.
     */
    public function isOpen() {
        return static::isOpenStatus($this->status);
    }
    
    /**
     * Method to determine whether the status of this record is closed or not.
     *
     * @return bool True if the status is a "closed" one, false otherwise.
     */
    public function isClosed() {
        return static::isClosedStatus($this->status);
    }
    
    /**
     * Method to return all the user's To Do Views, and for each one, an indication as to
     * whether or not this ticket is on that view.
     *
     * @return bool True if the status is a "closed" one, false otherwise.
     */
    public function getUserToDoViews() {
        return ToDoView::getUserViewsForTicket($this->id);
    }
    
    /**
     * Method to return this ticket in a suitable format for the calling process, as determined
     * by the mode. By default the ticket is converted to an array.
     *
     * @param string $mode Defines the format that the calling process wants.
     *
     * @return array The ticket as an array, with any mode-specific modifications.
     */
    public function format($mode = null) {
        $result = $this->toArray();
        if ($mode == 'api') {
            // Evaluate the markdown for the description; get the created-by name.
            $result['description_markdown'] = $this->descriptionForView();
            $result['created_by_name'] = $this->createdByUser->username;
            $result['estimated_length_for_view'] = $this->length_db_to_screen();
            $result['due_datetime'] = $this->due_datetime == null ? '' : date('c', strtotime($this->due_datetime));
            // We want all the assignments for this ticket.
            $result['assignments'] = [];
            $assignments = $this->assignments;
            foreach($assignments as $assignment) {
                $username = $assignment->user->username;
                $assignment = $assignment->toArray();
                unset($assignment['ticket_id']);
                unset($assignment['user']);
                $assignment['user_name'] = $username;
                $result['assignments'][] = $assignment;
            }
            // And we want all the comments.
            $result['comments'] = [];
            $comments = $this->comments;
            foreach($comments as $comment) {
                $comment = $comment->format('api');
                unset($comment['ticket_id']);
                unset($comment['user']);
                $result['comments'][] = $comment;
            }
            // And we want all the relationships.
            $result['associated'] = [];
            $associated = $this->associated()->with('trgTicket')->get();
            foreach($associated as $associate) {
                $associateTemp = $associate->toArray();
                unset($associateTemp['trg_sort_order'], $associateTemp['trg_ticket']);
                $associateTemp['trg_summary'] = $associate->trgTicket->summary;
                $result['associated'][] = $associateTemp;
            }
            // And we want all the time recorded against this ticket. We also want the totals
            // for this ticket and any children.
            $result['timesSpent'] = [];
            $timesSpent = $this->getTimesSpentForView();
            foreach($timesSpent as $timeSpent) {
                $timeSpentTemp = $timeSpent->toArray();
                unset($timeSpentTemp['created_at'], $timeSpentTemp['updated_at']);
                $timeSpentTemp['start_datetime'] = str_replace(' ', 'T', $timeSpentTemp['start_datetime']);
                $timeSpentTemp['username'] = $timeSpent->user->username;
                $timeSpentTemp['duration_formatted'] = $timeSpent->durationForView();
                $result['timesSpent'][] = $timeSpentTemp;
            }
            $result['timeSpentTotals'] = $this->getTimeSpentTotals();
            foreach($result['timeSpentTotals'] as &$oneTotal) {
                $oneTotal = DatetimeManager::intervalToEnglish($oneTotal);
            }
        }
        return $result;
    }
    
    /**
     * Method to return a set of tickets for the API, after applying any user-defined filtering.
     * It uses the Input fields & values for the filter.
     *
     * @param int $project_id Optional. Used in the filter if populated.
     *
     * @return Collection of tickets matching the filter (if any), sorted by id.
     */
    public static function apiFilter($project_id = null) {
        // Order the results by id, then implement any filters.
        $tickets = Ticket::orderBy('id');
        $projectFilter = ($project_id > 0 ? $project_id : (Input::has('project_id') ? Input::get('project_id') : null));
        if ($projectFilter != null) {
            $tickets = $tickets->where('project_id', '=', $projectFilter);
        }
        if (Input::has('ticket_types')) {
            // Restrict the filter values to values that are valid.
            $ticket_types = explode(',', Input::get('ticket_types'));
            $ticket_types = array_intersect(array_keys(static::allTypes()), $ticket_types);
            if (count($ticket_types)) {
                $tickets = $tickets->whereIn('ticket_type', $ticket_types);
            }
        }
        if (Input::has('summary')) {
            $tickets = $tickets->where('summary', 'like', '%' . Input::get('summary') . '%');
        }
        if (Input::has('description')) {
            $tickets = $tickets->where('description', 'like', '%' . Input::get('description') . '%');
        }
        if (Input::has('created_by_user_id')) {
            $tickets = $tickets->where('created_by_user_id', '=', Input::get('created_by_user_id'));
        }
        if (Input::has('priorities')) {
            // Restrict the filter values to values that are valid.
            $priorities = explode(',', Input::get('priorities'));
            $priorities = array_intersect(array_keys(static::getAllPriorities()), $priorities);
            if (count($priorities)) {
                $tickets = $tickets->whereIn('priority', $priorities);
            }
        }
        if (Input::has('statuses')) {
            // Restrict the filter values to values that are valid.
            $statuses = explode(',', Input::get('statuses'));
            $statuses = array_intersect(array_keys(static::getAllStatuses()), $statuses);
            if (count($statuses)) {
                $tickets = $tickets->whereIn('status', $statuses);
            }
        }
        // @TODO: due_datetime - needs before, after, between etc.
        // @TODO: estimated_length_seconds - needs before, after, between etc.
        // Handle assignment filters.
        if (Input::has('assignment_user_ids')) {
            // Only include tickets that are assigned to at least one of the users.
            $tickets = $tickets->whereHas('assignments', function($query) {
                // @TODO: Validate the ids passed in.
                $userIds = explode(',', Input::get('assignment_user_ids'));
                $query->whereIn('user_id', $userIds);
                if (Input::has('assignment_level')) {
                    // And at the given level.
                    $op = (Input::get('assignment_level') == 'primary' ? '=' : '>' );
                    $query->where('sort_order', $op, 1);
                }
            });
        }
        $tickets = $tickets->with('project')->get();
        
        return $tickets;
    }
    
    /**
     * Method to return total time spent with/without including its children, and
     * the estimated time remaining.
     */
    public function getTimeSpentTotals() {
        $timeSpentThisTicket = $this->sumTimeSpent();
        $timeSpentIncChildren = $this->sumTimeSpentIncChildren();
        // The time remaining values are null unless this ticket has an estimate.
        $timeRemThisTicket = null;
        $timeRemIncChildren = null;
        if ($this->estimated_length_seconds > 0) {
            $timeRemThisTicket = $this->estimated_length_seconds - $timeSpentThisTicket;
            $timeRemIncChildren = $this->estimated_length_seconds - $timeSpentIncChildren;
        }
        return [
            'total_spent' => $timeSpentThisTicket,
            'total_spent_inc_children' => $timeSpentIncChildren,
            'total_remaining' => $timeRemThisTicket,
            'total_remaining_inc_children' => $timeRemIncChildren
        ];
    }
    
    /**
     * Method to return the sum of time recorded for this ticket.
     * It looks quite simple, but it's useful to have it in a function for when we
     * want the total for all descendants (children) of a ticket. See sumTimeSpentIncChildren()
     * below.
     */
    public function sumTimeSpent() {
        return $this->timeSpent->sum('duration');
    }
    
    /**
     * Method to return the total time spent on this ticket and its children, and its
     * children's children, etc.
     */
    public function sumTimeSpentIncChildren() {
        // Result is time spent on this ticket...
        $timeSpent = $this->sumTimeSpent();
        // ...plus time spent on its children.
        foreach($this->childTickets() as $childTicket) {
            $timeSpent += $childTicket->sumTimeSpentIncChildren();
        }
        
        return $timeSpent;
    }
    
    public static function allowed($ticketId, $operation, $projectId = null) {
        if (Project::allowed($projectId, 'activity')) {
            return true;
        }
        return false;
    }
    
}
