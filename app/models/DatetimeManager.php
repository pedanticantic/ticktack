<?php

class DatetimeManager {
    
    /**
     * Array Multipliers for weeks, days, hours, minutes. Values are in minutes.
     */
    private static $multipliers = ['w' => 10080, //60*24*7
                                   'd' => 1440, //60*24
                                   'h' => 60,
                                   'm' => 1];
    
    /**
     * Method to simply return the array of multipliers.
     *
     * @return array the multipliers.
     */
    public static function getMultipliers() {
        return static::$multipliers;
    }
    
    /**
     * Method to check that a date is a valid UK date. Ie of the form DD/MM/YYYY.
     *
     * @param $ukdate string The date to be tested.
     *
     * @return boolean True if the date is valid; false otherwise.
     */
    public static function dateIsValidUK($ukdate) {
        if (strlen($ukdate) != 10) {
            return false;  // Simply the wrong length.
        }
        $parts = explode('/', $ukdate);
        if (count($parts) != 3) {
            return false;  // Contains the wrong number of slashes.
        }
        if (! is_numeric($parts[0]) ||
            ! is_numeric($parts[1]) ||
            ! is_numeric($parts[2]) ||
            ! checkdate($parts[1], $parts[0], $parts[2])) {
            return false;  // Date is not valid.
        }
        return true;
    }
    
    /**
     * Method to check that a time is valid. Ie of the form HH24:MI:SS.
     *
     * @param $thetime string The time to be tested.
     * @param $withSeconds bool Whether the "seconds" part must be present (and valid), or absent.
     *
     * @return boolean True if the time is valid; false otherwise.
     */
    public static function timeIsValid($thetime, $withSeconds = false) {
        if (! (($withSeconds && strlen($thetime) == 8) ||
               (!$withSeconds && strlen($thetime) == 5)) ) {
            return false;  // Wrong length
        }
        $parts = explode(':', $thetime);
        if (! ( (count($parts) == 3 && $withSeconds) ||
                (count($parts) == 2 && ! $withSeconds) ) ) {
            return false;  // Wrong number of colons.
        }
        if (! is_numeric($parts[0]) ||
            ! is_numeric($parts[1]) ||
            ($withSeconds && !is_numeric($parts[2])) ) {
            var_dump($withSeconds);
            return false;
        }
        if ($parts[0] < 0 || $parts[0] > 23 ||
            $parts[1] < 0 || $parts[1] > 59 ||
            ( $withSeconds && ($parts[2] < 0 || $parts[2] > 59) ) ) {
            return false;
        }
        return true;
    }
    
    /**
     * Method to check that a UK date/time is valid. Ie of the form DD/MM/YYYY HH24:MI:SS.
     *
     * @param $ukdatetime string The date/time to be tested.
     * @param $withSeconds bool Whether the "seconds" part must be present (and valid), or absent.
     *
     * @return boolean True if the date/time is valid; false otherwise.
     */
    public static function datetimeIsValidUK($ukdatetime, $withSeconds = false) {
        // Do some quick checks, then split around the SPACE and call dateIsValidUK & timeIsValid for each part.
        if (! (($withSeconds && strlen($ukdatetime) == 19) ||
               (! $withSeconds && strlen($ukdatetime) == 16)) ) {
            return false;  // Wrong length.
        }
        $parts = explode(' ', $ukdatetime);
        if (count($parts) != 2) {
            return false;  // Wrong format.
        }
        if (static::dateIsValidUK($parts[0]) && static::timeIsValid($parts[1], $withSeconds)) {
            return true;
        }
        return false;
    }
    
    /**
     * Method to convert a UK date tp ISO format.
     *
     * @param $ukdate string The date in UK format to be converted.
     *
     * @return string The date in ISO format. Returns null is the date is not valid.
     */
   public static function dateUKToString($ukdate) {
        if (static::dateIsValidUK($ukdate)) {
            return substr($ukdate, 6, 4) . '-' .
                   substr($ukdate, 3, 2) . '-' .
                   substr($ukdate, 0, 2);
        }
        return null;
    }
    
    /**
     * Method to convert a time from a screen value to a database value.
     * Yes, I know the formats are the same (HH24:MI:SS), but we also validate it.
     *
     * @param $thetime string The time to be converted.
     * @param $withSeconds bool Whether the "seconds" part must be present (and valid), or absent.
     *
     * @return string The time in DB format. If the time is not valid, it returns null.
     */
   public static function timeToString($thetime, $withSeconds = false) {
        if (static::timeIsValid($thetime, $withSeconds)) {
            return $withSeconds ? $thetime : substr($thetime, 0, 5);
        }
        return null;
    }
    
    /**
     * Method to convert a UK date/time to ISO format.
     *
     * @param $ukdatetime string The date/time to be converted.
     * @param $withSeconds bool Whether the "seconds" part must be present (and valid), or absent.
     *
     * @return string The date/time in ISO format. Returns null if the input is not valid.
     */
   public static function datetimeUKToString($ukdatetime, $withSeconds = false) {
        // Check that it's a valid UK date/time, then split and call dateUKToString & timeToString with the parts.
        if ( static::datetimeIsValidUK($ukdatetime, $withSeconds)) {
            $parts = explode(' ', $ukdatetime);
            return static::dateUKToString($parts[0]) . ' ' . static::timeToString($parts[1], $withSeconds);
        }
        return null;
    }
    
    /**
     * Method to convert a ISO date/time to UK format.
     *
     * @param $isodatetime string The date/time to be converted.
     * @param $withSeconds bool Whether the "seconds" part must be present (and valid), or absent.
     *
     * @return string The date/time in UK format. Returns null if the input is not valid.
     */
   public static function datetimeISOToUK($isodatetime, $withSeconds = false) {
        // @TODO: Check that it's a valid ISO date/time, then split and call dateISOToUK & timeToString with the parts (those methods to be written).
        // if ( static::datetimeIsValidUK($ukdatetime)) {
        //     $parts = explode(' ', $ukdatetime);
        //     return static::dateUKToString($parts[0]) . ' ' . static::timeToString($parts[1]);
        // }
        // return null;
        if ($isodatetime == '') {
            return '';
        }
        $mainparts = explode(' ', $isodatetime);
        $dateparts = explode('-', $mainparts[0]);
        $timeparts = explode(':', $mainparts[1]);
        return $dateparts[2] . '/' . $dateparts[1] . '/' . $dateparts[0] . ' ' .
               $timeparts[0] . ':' . $timeparts[1] . ($withSeconds ? ':' . $timeparts[2] : '');
    }
    
    /**
     * Method to return how long ago the given ISO date/time was.
     *
     * @param $isoDatetime The ISO date/time value.
     *
     * @return string A nice English description of how long ago this was.
     */
    public static function howLongAgo($isoDatetime) {
        return static::intervalToEnglish(time() - strtotime($isoDatetime)) . ' ago';
    }
    
    /**
     * Method to return return the given number of seconds as an English expression (kind of).
     *
     * @param $timeInterval int The interval in seconds.
     * @param $allowNegative bool If the interval is negative, then if true, return '(<English value>)', otherwise null.
     *
     * @return string An English representation of the interval..
     */
    public static function intervalToEnglish($timeInterval, $allowNegative = false) {
        if ($timeInterval === 0) {
            return '0m';
        } else {
            if ($timeInterval === null) {
                return '';
            } else {
                if ($timeInterval > 0) {
                    $englishValue = '';
                    $intervalRem = $timeInterval / 60;
                    foreach(static::$multipliers as $letter => $factor) {
                        $thisMultiplier = floor($intervalRem / $factor);
                        if ($thisMultiplier > 0) {
                            $englishValue .= ' ' . $thisMultiplier . $letter;
                            $intervalRem -= $thisMultiplier * $factor;
                        }
                    }
                    $englishValue = substr($englishValue, 1);
                    return $englishValue;
                } else {
                    if ($allowNegative) {
                        return '(' . static::intervalToEnglish(-1 * $timeInterval) . ')';
                    } else {
                        return '';
                    }
                }
            }
        }
    }
    
    /**
     * Method to convert an estimated length from a screen value to a database value.
     *
     * @param string $screenValue The screen (user friendly) time interval.
     *
     * @return mixed null if the value is null; false if it is invalid; the integer value otherwise.
     */
    public static function englishToInterval($screenValue) {
        if ($screenValue == '') {
            return null;
        }
        // Remove any spaces.
        $screenValue = str_replace(' ', '', $screenValue);
        // Now split on 'w', 'd', 'h' and 'm', and capture the letter we split on.
        $parts = preg_split('/(w|d|h|m)/', $screenValue, -1, PREG_SPLIT_DELIM_CAPTURE);
        $last = array_pop($parts);
        if ($last != '') {
            return false;
        }
        $total = 0; // We'll calculate it in minutes because it's easier, and convert to seconds at the end.
        $multipliers = DatetimeManager::getMultipliers();
        for($pidx = 0 ; $pidx < count($parts) ; $pidx+=2) {
            if (!is_numeric($parts[$pidx])) {
                return false;
            }
            $total += $parts[$pidx] * $multipliers[$parts[$pidx+1]];
        }
        $total *= 60;
        return $total;
    }
    
}