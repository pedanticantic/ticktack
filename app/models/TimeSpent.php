<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TimeSpent extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait, RemindableTrait;
    
    /**
     * Constructor function.
     */
    public function __construct() {
        parent::__construct();
        $this->table = 'time_spent'; // This table is not plural because it would sound weird.
    }
    
    /**
     * Contains error messages when saving time spent.
     *
     * @var array
     */
    static $timeSpentErrors;
    
    /**
     * Fundamental validation rules.
     *
     * @var arrays
     */
    public static $rules = array(
        'ticket_id' => 'required|integer',
        'user_id' => 'required|integer',
        'start_datetime' => 'required|date_format:Y-m-d H:i',
        'duration' => 'required|integer',
        'notes' => 'required|max:250'
    );
    
    public function isValid()
    {
        // Clear any existing errors.
        static::$timeSpentErrors = [];
        // Make a new validator object, using the values in the model.
        $v = Validator::make($this->toArray(), static::$rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            foreach($v->messages()->all() as $message) {
                static::$timeSpentErrors[] = $message;
            }
            return false;
        }
        
        // Validation passed.
        return true;
    }
    
    /**
     * Method to define the relationship between time spent and the user who created it.
     */
    public function user() {
        return $this->belongsTo('User', 'user_id');
    }
    
    public static function allowed($timeSpentId, $operation, $ticketId = null) {
        $ticket = Ticket::find($ticketId);
        if (Ticket::allowed($ticketId, 'activity', $ticket->project->id)) {
            return true;
        }
        return false;
    }
    
    /**
     * Method to try to create a record from the values passed in.
     * If it fails, it returns the reason(s) why.
     */
    public static function attemptToCreate($ticketId, $userId, $startDatetime, $duration, $notes) {
        // Convert the start date/time from UK to ISO format.
        if (!DatetimeManager::datetimeIsValidUK($startDatetime)) {
            return 'Start Date must be a valid date';
        }
        $startDatetime = DatetimeManager::datetimeUKToString($startDatetime);
        // Convert the duration from human format to a number of seconds.
        $duration = DatetimeManager::englishToInterval($duration);
        if ($duration === false) {
            return 'Duration must be a valid interval, eg 2h30m';
        }
        
        // Do the basic validation on the values.
        $timeSpent = new TimeSpent();
        $timeSpent->ticket_id = $ticketId;
        $timeSpent->user_id = $userId;
        $timeSpent->start_datetime = $startDatetime;
        $timeSpent->duration = $duration;
        $timeSpent->notes = $notes;
        if ($timeSpent->isValid()) {
            // Check the ticket and user exist.
            $parentTicket = Ticket::find($timeSpent->ticket_id);
            if ($parentTicket == null) {
                return 'Parent ticket does not exist.';
            }
            $parentUser = User::find($timeSpent->user_id);
            if ($parentUser == null) {
                return 'Parent user does not exist.';
            }
            $timeSpent->save();
            return true;
        }
        // Row is not valid. Return the reasons.
        return static::$timeSpentErrors;
    }
    
    /**
     * Method to return the duration value in "English" format.
     */
    public function durationForView() {
        return DatetimeManager::intervalToEnglish($this->duration);
    }
    
}
