<?php

class AssignmentController extends BaseController {
    
    /**
     * Method to remove the given assignment record.
     *
     * @param $assignmentId int The assignment record id.
     */
    public function removeAssignment($assignmentId) {
    	// Ask the assignment model to delete the record.
    	$TicketId = TicketAssignment::remove($assignmentId);
    	
    	// Now display the ticket.
    	return Redirect::to('ticket/' . $TicketId)->with('message', 'The assignment was removed.');
    }
    
    /**
     * Method to perform the given action on the given assignment record.
     *
     * @param $assignmentId int The assignment record id.
     * @param $assignmentAction string Either "up" or "down"
     */
    public function actionAssignment($assignmentId, $assignmentAction) {
        TicketAssignment::moveAssignment($assignmentId, $assignmentAction);
        $assignmentSrc = TicketAssignment::find($assignmentId);
        
        // Now display the ticket.
        return Redirect::to('ticket/' . $assignmentSrc->ticket_id)->with('message', 'The assignment was moved.');
    }
    
    /**
     * Method to handle the client assigning the given user to the given ticket.
     *
     * @param $ticket_id int The ticket id.
     * @param $user_id int The user id.
     */
    public function assignment($ticket_id, $user_id) {
        // Check that the user is allowed to make assignments to this ticket.
        if (TicketAssignment::allowed(null, 'update', $ticket_id)) {
            // If the user is already assigned to this ticket, return an error.
            if (TicketAssignment::alreadyAssigned($ticket_id, $user_id)) {
                return Response::json(
                    [
                        'success' => false,
                        'assignment_id' => null,
                        'message' => 'User is already assigned to this ticket',
                    ],
                    409
                );
            }
            $newSortOrder = (Input::has('sort_order') ? Input::get('sort_order') : null);
            $assignmentId = TicketAssignment::assignUserToTicket($ticket_id, $user_id, $newSortOrder);
            return Response::json(
                [
                    'success' => true,
                    'assignment_id' => $assignmentId,
                    'message' => 'User has been assigned to this ticket',
                ],
                200
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'assignment_id' => null,
                    'message' => 'Assigning users to this ticket is forbidden',
                ],
                403
            );
        }
    }
    
    /**
     * Method to handle the client removing the given assignment.
     *
     * @param $id int The assignment id (points to a ticket-user combo).
     */
    public function destroy($id) {
        $assignment = TicketAssignment::find($id);
        // Check that the user is allowed to remove assignments from this ticket.
        if (TicketAssignment::allowed($id, 'delete', $assignment->ticket_id)) {
            $fromTicketId = TicketAssignment::remove($id);
            if ($fromTicketId == null) {
                return Response::json(
                    [
                        'success' => false,
                        'message' => 'Assignment does not exist',
                    ],
                    404
                );
            }
            return Response::json(
                [
                    'success' => true,
                    'message' => 'Assignment has been removed',
                ],
                200
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Removing an assignment from this ticket is forbidden',
                ],
                200
            );
        }
    }
    
    /**
     * Method to handle the client moving the given assignment up or down within the ticket.
     *
     * @param $id int The assignment id (points to a ticket-user combo).
     * @param $direction string The direction of the move (either 'up' or 'down').
     */
    public function moveAssignment($id, $direction) {
        $result = [
            'success' => false,
            'error_code' => null,
            'new_sort_order' => null,
            'message' => 'Assignment does not exist or direction is invalid'
        ];
        $assignment = TicketAssignment::find($id);
        // Check that the user is allowed to remove assignments from this ticket.
        if (TicketAssignment::allowed($id, 'update', $assignment->ticket_id)) {
            // The result will be an appropriate error message, or null if successful.
            $moveResult = TicketAssignment::moveAssignment($id, $direction);
            $result['error_code'] = $moveResult['code'];
            $result['message'] = $moveResult['message'];
            if ($moveResult['code'] == 0) {
                $result['success'] = true;
                $result['new_sort_order'] = $moveResult['new_sort_order'];
                return Response::json(
                    $result,
                    200
                );
            }
        } else {
            $result['message'] = 'Moving assignments on this ticket is forbidden';
        }
        return Response::json(
            $result,
            ($result['error_code'] == 1 ? 404 : 403 )
        );
    }
    
}
