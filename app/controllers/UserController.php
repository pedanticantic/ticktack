<?php

class UserController extends BaseController {
    
    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';
    
    /**
     * Handles user's attempt to register.
     *
     * @return mixed The routes/views to display, depending on success or failure.
     */
    public function handleRegistration() {
        
        // Retrieve the form values.
        $username = Input::get('username');
        $password = Input::get('password');
        $confirm = Input::get('confirm_password');
        
        if (User::attemptToRegister($username, $password, $confirm)) {
            // Registration was successful and user was created. Redirect to
            // the project list page.
            return Redirect::to('projects');
        } else {
            // Note we don't pass the password(s) back to the page.
            // @TODO: I couldn't get "with('err', <errors>)" working, hence passing them in as data.
            return View::make('register', ['username' => $username, 'err' => User::getRegistrationErrors()]);
        }
        
    }
    
    /**
     * Handles user's attempt to log in.
     *
     * @return mixed The routes/views to display, depending on success or failure.
     */
    public function handleLogIn() {
        
        // Retrieve the login form values.
        $username = Input::get('username');
        $password = Input::get('password');
        
        if (User::attemptLogin(['username' => $username, 'password' => $password])) {
            return Redirect::intended('projects');
        }
        
        return View::make('login', ['username' => $username, 'err' => ['Username and/or password was invalid. Please try again.']]);
        
    }
    
    /**
     * Handles user's attempt to log out.
     *
     * @return View The view to display after logout (the welcome page).
     */
    public function handleLogOut() {
        User::logOut();
        return View::make('welcome');
    }
    
    /**
     * Handles an attempted login from the API.
     */
    public function login() {
        
        // Retrieve the passed in login credentials.
        $username = Input::get('username');
        $password = Input::get('password');
        
        // See if the credentials represent a valid user.
        $isValid = (User::attemptLogin(['username' => $username, 'password' => $password]));
        // Return the outcome.
        return Response::json(
            array(
                'success' => $isValid
            ),
            200
        );
    }
    
    /**
     * Method to return all users in the system (that the logged-in user has access to)
     */
    public function index() {
        $users = User::apiFilter();
        $formattedUsers = [];
        foreach($users as $user) {
            $formattedUsers[] = $user->format('api');
        }
        return Response::json(
            array(
                'success' => true,
                'results' => count($formattedUsers),
                'users' => $formattedUsers
            ),
            200
        );
    }
    
    /**
     * Method to allow a new user to register.
     */
    public function store() {
        // Initialise the return values.
        $result = array(
            'success' => false,
            'errors' => array('Unknown error')
        );
        // Retrieve the form values.
        $username = Input::get('username');
        $password = Input::get('password');
        $confirm = Input::get('confirm_password');
        // Try to register.
        if (User::attemptToRegister($username, $password, $confirm)) {
            $result['success'] = true;
            $result['errors'] = null;
        } else {
            $result['errors'] = User::getRegistrationErrors();
        }
        return Response::json(
            $result,
            200
        );
    }
    
}
