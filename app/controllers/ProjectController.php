<?php

class ProjectController extends BaseController {
    
    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';
    
    /**
     * Method to handle the user saving a record.
     *
     * @param int $id The id of the project.
     */
    public function handleSave($id = null) {
        
        // Retrieve the form values and start creating/updating the project with them.
        if ($id > 0) {
            $project = Project::find($id);
        } else {
            $project = new Project();
        }
        $project->name = Input::get('projectname');
        $project->description = Input::get('description');
        $project->lead_user_id = Input::get('projectuser');
        
        if ($project->isValid()) {
            // Project is valid and can be saved. Then redirect to
            // the project details page.
            $project->save();
            return Redirect::to('project/' . $project->id)
                ->with('message', 'Your project was saved.');
        } else {
            // Pass the project data back to the view with appropriate messages.
            // @TODO: I couldn't get "with('err', <errors>)" working, hence passing them in as data.
            return View::make('projects.project_form', ['project' => $project, 'allUsers' => Project::getAllUsers(), 'err' => Project::getSaveErrors()]);
        }
    }
    
    /**
     * Method to display a project.
     *
     * @param int $id Project id.
     *
     */
    public function showProject($id) {
        $project = Project::getProject($id);
        if ($project === FALSE) {
            return Redirect::to('projects');
        }
        return View::make('projects.project', ['project' => $project]);
    }
    
    /**
     * Method to edit a project. The id is passed in. We retrieve the data and if valid, show the edit
     * screen; otherwise we return to the project list.
     *
     * @param int $id Project id.
     */
    public function editProject($id) {
        $project = Project::getProject($id);
        if ($project === FALSE) {
            return Redirect::to('projects');
        }
        return View::make('projects.project_form', ['project' => $project, 'allUsers' => Project::getAllUsers()]);
    }
    
    /* Methods for the API. */
    
    /**
     * The GET handler.
     *
     * @return Response All the projects (filter to be added)
     */
    public function index() {
        $projects = Project::apiFilter();
        $formattedProjects = [];
        foreach($projects as $project) {
            $formattedProjects[] = $project->format('api');
        }
        return Response::json(
            array(
                'success' => true,
                'results' => count($formattedProjects),
                'projects' => $formattedProjects
            ),
            200
        );
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id The project id
     * @return Response The project, if we found one.
     */
    public function show($id) {
        // Load the given project, if it exists.
        $project = Project::getProject($id);

        return Response::json(
            array(
                'success' => true,
                'results' => 1,
                'projects' => array($project->format('api'))
            ),
            200
        );
    }
    
    /**
     * The POST handler.
     *
     * @return Response Information on whether the save was a success. Includes the new id if successful.
     */
    public function store() {
        
        // Prepare the return value. Assume the worst!
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
            'id' => null
        );
        $returnCode = 400;
        
        // Start a new project.
        $project = new Project;
        // Get the name, description and lead user for the project they are creating.
        $project->name = Request::input('name');
        $project->description = Request::input('description');
        $lead_user_id = Request::input('lead_user_id');
        $lead_user_id = $lead_user_id > 0 ? $lead_user_id : Auth::user()->id;
        $project->lead_user_id = $lead_user_id;
        // Check it's valid.
        if ($project->isValid()) {
            // Attempt to save it.
            $project->save();
            $projectId = $project->id;
            if ($projectId > 0) {
                // Successful.
                $result['success'] = true;
                $result['message'] = null;
                $result['id'] = $projectId;
                $returnCode = 201;
            } else {
                $result['message'] = 'Unknown error when saving the project';
            }
        } else {
            $result['message'] = 'Failed to save the new project. Make sure name and description are present.';
        }
        
        return Response::json(
            $result,
            $returnCode
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id The project id
     * @return Response
     */
    public function destroy($id) {
        // Prepare the return value.
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
        );
        $returnCode = 400;
        
        // Check that the user is allowed to delete this project.
        if (Project::allowed($id, 'delete')) {
            // Load the given project, if it exists.
            $project = Project::getProject($id);
            if ($project == null) {
                // Something went wrong. The project id probably wasn't valid.
                $result['message'] = 'Unable to delete that project. Maybe the id was invalid?';
            } else {
                $project->delete();
                $result['success'] = true;
                $result['message'] = 'Project was deleted';
                $returnCode = 204;
            }
        } else {
            // Delete is not allowed.
            $result['message'] = 'Delete is forbidden on this project.';
            $returnCode = 403;
        }
        
        return Response::json(
            $result,
            $returnCode
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id The project id
     * @return Response
     */
    public function update($id) {
        // Prepare the return value.
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
        );
        $returnCode = 400;
        
        // Check that the user is allowed to update this project.
        if (Project::allowed($id, 'update')) {
            // Load the given project, if it exists.
            $project = Project::getProject($id);
            if ($project == null) {
                // Something went wrong. The project id probably wasn't valid.
                $result['message'] = 'Unable to update that project. Maybe the id was invalid?';
            } else {
                // Now update anything that's passed in.
                if (Request::has('name')) {
                    $project->name = Request::input('name');
                }
                if (Request::has('description')) {
                    $project->description = Request::input('description');
                }
                if (Request::has('lead_user_id')) {
                    $project->lead_user_id = Request::input('lead_user_id');
                }
                // Check it is valid.
                if ($project->isValid()) {
                    // Save the project.
                    $project->save();
                    
                    $result['success'] = true;
                    $result['message'] = 'Project was updated';
                    $returnCode = 200;
                } else {
                    $result['message'] = 'Unable to update that project. Make sure the name and description are present and the name is unique.';
                }
            }
        } else {
            // Update is not allowed.
            $result['message'] = 'Update is forbidden on this project.';
            $returnCode = 403;
        }
     
        return Response::json(
            $result,
            $returnCode
        );
    }
    
}
