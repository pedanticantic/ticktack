<?php

class ToDoViewController extends BaseController {
    
    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';
    
    /**
     * Method to handle the user saving a view.
     * The view may or may not already exist.
     *
     */
    public function handleSave() {
        $id = Input::has('id') ? Input::get('id') : null;
        $name = Input::get('view_name');
        // Create/update the record.
        if ($id == null) {
            $toDoView = new ToDoView();
            $toDoView->owner_user_id = Auth::id();
        } else {
            $toDoView = ToDoView::find($id);
        }
        $toDoView->view_name = $name;
        if ($toDoView->isValid()) {
            $toDoView->save();
            // Save was successful and To Do View was created/updated. Redirect to
            // the view details page.
            return Redirect::to('todoview/' . $toDoView->id)
                ->with('message', 'Your view was saved.');
        } else {
            return View::make('to_do_views.form', ['toDoView' => $toDoView, 'messages' => ToDoView::getLastErrors()]);
        }
    }
    
    /**
     * Method to display a view.
     *
     * @param int $id View id.
     */
    public function showView($id) {
        if (Request::ajax()) {
            return Response::json(ToDoViewsTicket::getContentsOfView($id));
        }
        $toDoView = ToDoView::getView($id);
        if ($toDoView === FALSE) {
            return Redirect::to('todoviews');
        }
        return View::make('to_do_views.todoview', ['toDoView' => $toDoView]);
    }
    
    /**
     * Method to add the given ticket to the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     * @param int $relativeTicketId Relative to this ticket id.
     * @param int $viewSummary View-specific summary for this ticket.
     */
    public function addTicketToView($viewId, $ticketId) {
        $relativeTicketId = (Input::has('relative_ticket') ? Input::get('relative_ticket') : null);
        $viewSummary = (Input::has('view_summary') ? Input::get('view_summary') : null);
        $ticketData = ToDoViewsTicket::addLink($viewId, $ticketId, $relativeTicketId, $viewSummary);
        if (Request::ajax()) {
            return Response::json($ticketData);
        }
        return Redirect::to('/ticket/' . $ticketId);
    }
    
    /**
     * Method to update the position of the given ticket on the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public function updateTicketPosition($viewId, $ticketId) {
        if (Request::ajax() && Input::has('x_pos') && Input::has('y_pos')) {
            return Response::json(ToDoViewsTicket::updatePosition($viewId, $ticketId, Input::get('x_pos'), Input::get('y_pos')));
        }
        return Redirect::to('/');
    }
    
    /**
     * Method to remove the given ticket from the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public function removeTicket($viewId, $ticketId) {
        if (Request::ajax()) {
            return Response::json(ToDoViewsTicket::removeTicket($viewId, $ticketId));
        }
        return Redirect::to('/');
    }
    
    /**
     * Method to update the view-specific summary of the given ticket in the given view.
     *
     * @param int $viewId View id.
     * @param int $ticketId Ticket id.
     */
    public function setTicketSummary($viewId, $ticketId) {
        if (Request::ajax() && Input::has('cmd')) {
            $newViewSummary = Input::get('view_summary');
            return Response::json(ToDoViewsTicket::setTicketSummary($viewId, $ticketId, $newViewSummary));
        }
        return Redirect::to('/');
    }
    
    /**
     * Method to update the viewport position in the given view.
     * The new viewport co-ordinates are posted to the server.
     *
     * @param int $viewId View id.
     */
    public function updateViewportPosition($viewId) {
        if (Request::ajax()) {
            $new_x_pos = (Input::has('viewport_x') ? Input::get('viewport_x') : null);
            $new_y_pos = (Input::has('viewport_y') ? Input::get('viewport_y') : null);
            if ($new_x_pos == null || $new_y_pos == null) {
                return Response::json(false);
            }
            // @TODO: We should handle errors.
            ToDoView::updateViewportPosition($viewId, $new_x_pos, $new_y_pos);
            return Response::json(true);
        }
        return Redirect::to('/');
    }
    
}
