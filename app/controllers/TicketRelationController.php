<?php

class TicketRelationController extends BaseController {
    
    /**
     * Method to handle the users request to change an attribute of a relation.
     *
     * @param int $relationId The id of the relation record.
     * @param int $srcTicketId The id of the source ticket.
     * @param string $action Which attribute to change.
     */
    public function handleAction($relationId, $srcTicketId, $action) {
        Log::debug($relationId . ', ' . $srcTicketId . ', ' . $action);
        
        // Read in the underlying relation record. The src ticket id is needed because
        // it's equally likely that the "trg" ticket is the "driving" ticket.
        $relation = TicketAssociation::find($relationId);
        
        // Call the approriate method to hande the action. If the action isn't valid, don't
        // do anything (except re-load the page).
        $confirmMessage = 'The relation was updated.';
        if ($action == 'pin' || $action == 'unpin') {
            $relation->handlePinning($srcTicketId, $action);
        } elseif ($action == 'up' || $action == 'down') {
            $relation->handleMove($srcTicketId, $action);
        } elseif ($action == 'delete') {
            // Just do a normal delete. Note that the delete method in the class is overridden
            // so it can adjust the sort orders.
            $relation->delete();
            $confirmMessage = 'The relation was deleted.';
        }
        
        // Now redisplay the ticket. We want to scroll down to where the relationships are displayed.
        return Redirect::to('ticket/' . $srcTicketId . '#ticketRelatedTickets_link')->with('messageRelationship', $confirmMessage);
    }
    
    /**
     * Method to handle a relation being created. Most of the values are posted via a form.
     *
     * @param int $srcTicketId The id of the source ticket.
     */
    public function handleCreate($srcTicketId) {
        $relationshipType = Input::get('relationship_type');
        $trgTicketId = Input::get('trg_ticket_id');
        $result = TicketAssociation::attemptToCreate($relationshipType, $srcTicketId, $trgTicketId);
        if ($result !== true) {
            return Redirect::to('ticket/' . $srcTicketId . '#ticketRelatedTickets_link')
                ->with('errorRelationship', $result)
                ->with('relationshipTypeFromForm', $relationshipType)
                ->with('trgTicketIdFromForm', $trgTicketId);
        }
        
        // Now redisplay the ticket. We want to scroll down to where the relationships are displayed.
        return Redirect::to('ticket/' . $srcTicketId . '#ticketRelatedTickets_link')->with('messageRelationship', 'The relation was created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($ticketId, $relationshipId)
    {
        // Load the given relationship on the given ticket, if it exists.
        $relationship = TicketAssociationView::getRelationship($ticketId, $relationshipId);

        return Response::json(
            array(
                'success' => true,
                'results' => 1,
                'relationships' => array($relationship->format('api'))
            ),
            200
        );
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param int $ticketId Optional ticket id. Used in the filter if populated.
     *
     * @return Response
     */
    public function index($ticketId = null)
    {
        $relationships = TicketAssociationView::apiFilter($ticketId);
        $formattedRelationships = [];
        foreach($relationships as $relationship) {
            $formattedRelationships[] = $relationship->format('api');
        }
        return Response::json(
            array(
                'success' => true,
                'results' => count($formattedRelationships),
                'relationships' => $formattedRelationships
            ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $srcTicketId Optional ticket id. Used in the filter if populated.
     *
     * @return Response
     */
    public function store($srcTicketId = null)
    {
        
        // Prepare the return value. Assume the worst!
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
            'id' => null
        );
        $returnCode = 400;
        
        $srcTicketId = ($srcTicketId > 0 ? $srcTicketId : Request::input('source_ticket_id'));
        $trgTicketId = Request::input('target_ticket_id');
        // Check that the user is allowed to create this ticket.
        if (TicketAssociation::allowed(null, 'insert', $srcTicketId) &&
            TicketAssociation::allowed(null, 'insert', $trgTicketId)) {
            // Check the ticket is not being associated with itself.
            // @TODO: This code, and all the other checks should be moved into the model and checked on save(). Calling code will have to cope with save() failing.
            if ($srcTicketId == $trgTicketId) {
                $result['message'] = 'A ticket cannot be related to itself.';
                $returnCode = 403;
            } else {
                // Check for a dependency loop.
                $relationshipType = Request::input('relationship_type');
                if (TicketAssociation::checkDependencyLoop($relationshipType, $srcTicketId, $trgTicketId)) {
                    // We're okay...
                    // Check that adding this relationship won't cause a problem with the hierarchy of statuses.
                    if (TicketAssociation::checkStatusHierarchy($relationshipType, $srcTicketId, $trgTicketId)) {
                        // Start a new relationship.
                        $relationship = new TicketAssociation;
                        // Get the field values for the relationship they are creating.
                        $relationship->relationship_type = $relationshipType;
                        $relationship->src_ticket_id = $srcTicketId;
                        $relationship->src_sort_order = TicketAssociation::calculateSortOrder($relationship->src_ticket_id);
                        $relationship->trg_ticket_id = $trgTicketId;
                        $relationship->trg_sort_order = TicketAssociation::calculateSortOrder($relationship->trg_ticket_id);
                        $relationship->pinned = (Request::has('is_pinned') ? (Request::input('is_pinned') ? 1 : 0) : 0);
                        // Save the new relationship.
                        $relationship->save();
                        $relationshipId = $relationship->id;
                        if ($relationshipId > 0) {
                            // Successful.
                            $result['success'] = true;
                            $result['message'] = null;
                            $result['id'] = $relationshipId;
                            $returnCode = 201;
                        } else {
                            $result['message'] = 'Failed to save the new relationship. Make sure mandatory fields are present.';
                        }
                    } else {
                        $result['message'] = 'This relationship will cause a problem with the status hierarchy';
                        $returnCode = 403;
                    }
                } else {
                    $result['message'] = 'This relationship will create a dependency loop';
                    $returnCode = 403;
                }
            }
        } else {
            $result['message'] = 'Creating relationships on tickets is forbidden on this project';
            $returnCode = 403;
        }
        
        return Response::json(
            $result,
            $returnCode
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $ticketId the source ticket id
     * @param  int  $relationshipId the relationship id
     * @param  string $action what the client wants to do with it (pin,unpin,up,down)
     * @return Response
     */
    public function update($ticketId, $relationshipId, $action)
    {
        // Prepare the return value.
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
        );
        $returnCode = 400;
        
        // Load the given relationship, if it exists. Check that the ticket id is valid.
        $relationship = TicketAssociation::find($relationshipId);
        if ($relationship == null || ($ticketId != $relationship->src_ticket_id && $ticketId != $relationship->trg_ticket_id)) {
            // Something went wrong. The ticket id probably wasn't valid.
            $result['message'] = 'Unable to update that relationship. Maybe one of the ids was invalid, or that relationship is not against that ticket?';
        } else {
            // Check that the user is allowed to update this ticket.
            if (TicketAssociation::allowed(null, 'insert', $relationship->src_ticket_id) &&
                TicketAssociation::allowed(null, 'insert', $relationship->trg_ticket_id)) {
                // Perform the action (if possible).
                switch ($action) {
                    case 'pin':
                    case 'unpin':
                        $relationship->pinned = ($action == 'pin');
                        $relationship->save();
                        // Tell the user everything was okay.
                        $result['success'] = true;
                        $result['message'] = 'Relationship was ' . $action . 'ned';
                        $returnCode = 200;
                        break;
                    case 'up':
                    case 'down':
                        // Find the row to swap with.
                        $curr_sort_order = ($relationship->src_ticket_id == $ticketId ? $relationship->src_sort_order : $relationship->trg_sort_order);
                        $newSortOrder = $curr_sort_order + ($action == 'up' ? -1 : 1);
                        $swapRelView = TicketAssociationView::where('src_ticket_id', $ticketId)
                            ->where('src_sort_order', $newSortOrder)
                            ->first();
                        if ($swapRelView == null) {
                            // Something went wrong. Error message depends on whether we were trying
                            // to go up or down.
                            if ($action == 'up') {
                                $result['message'] = 'Relationship is already at the top';
                            } else {
                                $result['message'] = 'Relationship is already at the bottom';
                            }
                            $returnCode = 403;
                        } else {
                            // Okay, swap the sort orders over. The relationship we're going to
                            // swap with comes from the view, so we need to read the underlying
                            // record and update the appropriate column (depends on whether this
                            // ticket is the source or target in the record).
                            $swapRelActual = TicketAssociation::find($swapRelView->id);
                            if ($swapRelActual->src_ticket_id == $ticketId) {
                                $swapRelActual->src_sort_order = $curr_sort_order;
                            } else {
                                $swapRelActual->trg_sort_order = $curr_sort_order;
                            }
                            $swapRelActual->save();
                            if ($relationship->src_ticket_id == $ticketId) {
                                $relationship->src_sort_order = $newSortOrder;
                            } else {
                                $relationship->trg_sort_order = $newSortOrder;
                            }
                            $relationship->save();
                            // Tell the user everything was okay.
                            $result['success'] = true;
                            $result['message'] = 'Relationship was moved';
                            $returnCode = 200;
                        }
                        break;
                    default:
                        // No idea what they're trying to do!
                        $result['message'] = 'Invalid action';
                        $returnCode = 403;
                        break;
                }
            } else {
                $result['message'] = 'Updating relationships on one or both of its tickets is forbidden';
                $returnCode = 403;
            }
        }
     
        return Response::json(
            $result,
            $returnCode
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $srcTicketId source ticket id
     * @param  int  $relationshipId relationship id
     *
     * @return Response
     */
    public function destroy($srcTicketId, $relationshipId)
    {
        // Prepare the return value.
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
        );
        $returnCode = 400;
        
        // Load the record in.
        $relationship = TicketAssociation::find($relationshipId);
        if ($relationship == null || ($srcTicketId != $relationship->src_ticket_id && $srcTicketId != $relationship->trg_ticket_id)) {
            // Something went wrong. The ticket id probably wasn't valid.
            $result['message'] = 'Unable to update that relationship. Maybe one of the ids was invalid, or that relationship is not against that ticket?';
        } else {
            // Check that this operation on this relationship is valid.
            if (TicketAssociation::allowed($relationshipId, 'delete', $relationship->src_ticket_id) &&
                TicketAssociation::allowed($relationshipId, 'delete', $relationship->trg_ticket_id)) {
                // Okay, delete this record.
                // Note that it updates all the sort orders on both tickets as necessary.
                $relationship->delete();
                $result['success'] = true;
                $result['message'] = 'Relationship was deleted';
                $returnCode = 200;
            } else {
                // Operation not allowed.
                $result['message'] = 'Removing relationships on one or both of these tickets is forbidden';
                $returnCode = 403;
            }
        }
     
        return Response::json(
            $result,
            $returnCode
        );
    }
                
}
