<?php

class TicketController extends BaseController {
    
    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';
    
    // @TODO: I think we don't need handleSaveNew and handleSaveUpdate any more as the project
    //        id is always submitted in the form. Can simplify the routes, too.
    
    /**
     * Method to handle the user saving a new record.
     *
     * @param int $projectId The id of the project we are saving this new ticket against.
     */
    public function handleSaveNew($projectId = null) {
        return $this->handleSave('project', $projectId);
    }
    
    /**
     * Method to handle the user saving an existing record.
     *
     * @param int $id The id of the ticket we are saving.
     */
    public function handleSaveUpdate($id) {
        return $this->handleSave('ticket', $id);
    }
    
    /**
     * Method to handle the user saving a record.
     *
     * @param int $idType The type of record (project or ticket).
     * @param int $id The id of the given type of record.
     */
    public function handleSave($idType, $id) {
        
        // Sort out what the id means.
        $projectId = null;
        if ($idType == 'project') {
            $projectId = $id;
            $id = null;
        }
        
        // Retrieve the form values and get ready to create/update the record.
        if ($id > 0) {
            $ticket = Ticket::find($id);
        } else {
            $ticket = new Ticket();
        }
        $ticket->ticket_type = Input::get('ticket_type');
        $ticket->summary = Input::get('summary');
        $ticket->project_id = Input::get('project_id');
        $ticket->description = Input::get('description');
        $ticket->created_by_user_id = Input::get('created_by_user_id');
        $ticket->priority = Input::get('priority');
        $ticket->status = Input::get('status');
        $ticket->due_datetime = Input::get('due_datetime');
        $ticket->estimated_length_seconds = Input::get('estimated_length_seconds');
        $ticket->setAssignment(Input::get('assigned_to_user_id'));
        if ($ticket->isValid()) {
            $ticket->save();
            // Save was successful and ticket was created/updated. Redirect to
            // the ticket details page.
            return Redirect::to('ticket/' . $ticket->id)
                ->with('message', 'Your ticket was saved.');
        } else {
            // Build the record, but don't save it. Just makes it easier to pass the data around.
            // @TODO: I couldn't get "with('err', <errors>)" working, hence passing them in as data.
            return View::make('tickets.ticket_form', ['redisplay' => true, 'ticket' => $ticket, 'due_datetime' => Input::get('due_datetime'), 'estimated_length_seconds' => Input::get('estimated_length_seconds'), 'assigned_to_user_id' => Input::get('assigned_to_user_id'), 'allProjects' => Project::getAllProjects(), 'allUsers' => Project::getAllUsers(), 'allPriorities' => Ticket::getAllPriorities(), 'allStatuses' => Ticket::getAllStatuses(), 'err' => Ticket::getSaveErrors()]);
        }
    }
    
    /**
     * Method to display a ticket.
     *
     * @param int $id Ticket id.
     */
    public function showTicket($id) {
        $ticket = Ticket::getTicket($id);
        if ($ticket === FALSE) {
            return Redirect::to('tickets');
        }
        return View::make('tickets.ticket', ['ticket' => $ticket]);
    }
    
    /**
     * Method to edit a ticket. The id is passed in. We retrieve the data and if valid, show the edit
     * screen; otherwise we return to the ticket list.
     *
     * @param int $id Ticket id.
     */
    public function editTicket($id) {
        $ticket = Ticket::getTicket($id);
        if ($ticket === FALSE) {
            return Redirect::to('tickets');
        }
        return View::make('tickets.ticket_form', ['ticket' => $ticket, 'allProjects' => Project::getAllProjects(), 'allPriorities' => Ticket::getAllPriorities(), 'allStatuses' => Ticket::getAllStatuses(), 'allUsers' => Project::getAllUsers()]);
    }
    
    /**
     * Method to show the basic ticket search/list page.
     */
    public function ticketSearch() {
        $tickets = TicketSearch::getSearchResults();
        return View::make('tickets.search_results', ['tickets' => $tickets]);
    }
    
    /**
     * Method to show the basic ticket search/list page.
     */
    public function ticketSearchApply() {
        // There are two separate parts to this:
        // Remember the search critieria.
        TicketSearch::saveSearchCriteria();
        // Go to the search results page, which uses the saved critieria to match the tickets.
        return Redirect::to('tickets');
    }
    
    /**
     * Method to apply the filter(s) passed in on the command line and display the
     * resulting tickets.
     */
    public function ticketFilterApply() {
        // There are two separate parts to this:
        // Process the filter (search critieria).
        TicketSearch::applyFilter();
        // Go to the search results page, which uses the saved critieria to match the tickets.
        return Redirect::to('tickets');
    }
    
    /**
     * Method to return an array of arrays of static reference data.
     * This is things like ticket types, priorities, etc.
     */
    public function referenceData() {
        $rawData = [
            'ticket_types' => Ticket::allTypes(),
            'ticket_priorities' => Ticket::getAllPriorities(),
            'ticket_statuses' => Ticket::getAllStatuses(),
            'relationship_type' => TicketAssociation::getRelationshipTypes()
        ];
        // We need to convert the data so it has an explicit sequence number.
        $referenceData = [];
        foreach($rawData as $refType => $rawDatum) {
            $referenceData[$refType] = [];
            foreach($rawDatum as $code => $desc) {
                $referenceData[$refType][] = ['code' => $code, 'desc' => $desc];
            }
        }
        return Response::json(
            array(
                'success' => true,
                'referenceData' => $referenceData
            ),
            200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $id Optional project id. Used in the filter if populated.
     *
     * @return Response
     */
    public function index($id = null)
    {
        $tickets = Ticket::apiFilter($id);
        $formattedTickets = [];
        foreach($tickets as $ticket) {
            $formattedTickets[] = $ticket->format('api');
        }
        return Response::json(
            array(
                'success' => true,
                'results' => count($formattedTickets),
                'tickets' => $formattedTickets
            ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $project_id Optional project id. Used in the filter if populated.
     *
     * @return Response
     */
    public function store($project_id = null)
    {
        
        // Prepare the return value. Assume the worst!
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
            'id' => null
        );
        $returnCode = 400;
        
        $project_id = ($project_id > 0 ? $project_id : Request::input('project_id'));
        // Check that the user is allowed to create this ticket.
        if (Ticket::allowed(null, 'insert', $project_id)) {
            // Start a new ticket.
            $ticket = new Ticket;
            // Get the field values for the ticket they are creating.
            $ticket->project_id = $project_id;
            $ticket->ticket_type = Request::input('ticket_type');
            $ticket->summary = Request::input('summary');
            $ticket->description = Request::input('description');
            $created_by_user_id = Request::input('created_by_user_id');
            $created_by_user_id = $created_by_user_id > 0 ? $created_by_user_id : Auth::user()->id;
            $ticket->created_by_user_id = $created_by_user_id;
            $priority = Request::input('priority');
            $priority = ($priority == '' ? 'medium' : $priority);
            $ticket->priority = $priority;
            $status = Request::input('status');
            $status = ($status == '' ? 'new' : $status);
            $ticket->status = $status;
            $due_datetime = Request::input('due_datetime');
            if ($due_datetime != '') {
                $ticket->due_datetime = $due_datetime;
            }
            $estimated_length_seconds = Request::input('estimated_length_seconds');
            if ($estimated_length_seconds != '') {
                $ticket->estimated_length_seconds = $estimated_length_seconds; // There is a setter that converts the screen value to the DB value.
            }
            // Attempt to save it.
            if ($ticket->isValid()) {
                $ticket->save();
                $ticketId = $ticket->id;
                if ($ticketId > 0) {
                    // Successful.
                    $result['success'] = true;
                    $result['message'] = null;
                    $result['id'] = $ticketId;
                    $returnCode = 201;
                } else {
                    $result['message'] = 'Failed to save the new ticket. Make sure mandatory fields are present.';
                }
            } else {
                $result['message'] = 'Ticket is not valid. Make sure mandatory fields are present.';
            }
        } else {
            $result['message'] = 'Creating tickets is forbidden on this project';
            $returnCode = 403;
        }
        
        return Response::json(
            $result,
            $returnCode
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // Load the given ticket, if it exists.
        $ticket = Ticket::getTicket($id);

        return Response::json(
            array(
                'success' => true,
                'results' => 1,
                'tickets' => array($ticket->format('api'))
            ),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Prepare the return value.
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
        );
        $returnCode = 400;
        
        // Load the given ticket, if it exists.
        $ticket = Ticket::getTicket($id);
        if ($ticket == null) {
            // Something went wrong. The ticket id probably wasn't valid.
            $result['message'] = 'Unable to update that ticket. Maybe the id was invalid?';
        } else {
            // Check that the user is allowed to update this ticket.
            if (Ticket::allowed(null, 'update', $ticket->project_id)) {
                // Now update anything that's passed in.
                if (Request::has('project_id')) {
                    $ticket->project_id = Request::input('project_id');
                }
                if (Request::has('ticket_type')) {
                    $ticket->ticket_type = Request::input('ticket_type');
                }
                if (Request::has('summary')) {
                    $ticket->summary = Request::input('summary');
                }
                if (Request::has('description')) {
                    $ticket->description = Request::input('description');
                }
                if (Request::has('created_by_user_id')) {
                    $ticket->created_by_user_id = Request::input('created_by_user_id');
                }
                if (Request::has('priority')) {
                    $ticket->priority = Request::input('priority');
                }
                if (Request::has('status')) {
                    $ticket->status = Request::input('status');
                }
                if (Request::has('due_datetime')) {
                    $due_datetime = Request::input('due_datetime');
                    $ticket->due_datetime = ($due_datetime == '-' ? null : $due_datetime);
                }
                if (Request::has('estimated_length_seconds')) {
                    $estimated_length_seconds = Request::input('estimated_length_seconds');
                    $ticket->estimated_length_seconds = ($estimated_length_seconds == '-' ? '' : $estimated_length_seconds);
                }
                // Save the ticket.
                if ($ticket->isValid()) {
                    $ticket->save();
                    
                    $result['success'] = true;
                    $result['message'] = 'Ticket was updated';
                    $returnCode = 200;
                } else {
                    $result['message'] = 'Ticket is not valid. Check mandatory fields and field formats.';
                }
            } else {
                $result['message'] = 'Updating tickets is forbidden on this project';
                $returnCode = 403;
            }
        }
     
        return Response::json(
            $result,
            $returnCode
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
}
