<?php

class CommentController extends BaseController {
    
    /**
     * Method to handle the user saving a new comment.
     *
     * @param $ticketId int The (parent) ticket id.
     *
     * @return mixed The appropriate page to go to.
     */
    public function handleSave($ticketId) {
        
        // Retrieve the form values.
        $details = Input::get('details');
        
        // Create the comment record.
        $comment = new Comment();
        $comment->ticket_id = $ticketId;
        $comment->added_by_user_id = Auth::id();
        $comment->details = $details;
        
        if ($comment->isValid()) {
            $comment->save();
            // Save was successful. Return to the ticket view page.
            return Redirect::to('ticket/' . $ticketId);
        } else {
            // Save failed. Return to the ticket view page with a message.
            // @TODO: The message is shown as a "success!" - should be an error.
            return Redirect::to('ticket/' . $ticketId)
                ->with('message', 'You must enter something in the comment!');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // Load the given comment, if it exists.
        $comment = Comment::getComment($id);

        return Response::json(
            array(
                'success' => true,
                'results' => 1,
                'comments' => array($comment->format('api'))
            ),
            200
        );
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param int $id Optional ticket id. Used in the filter if populated.
     *
     * @return Response
     */
    public function index($id = null)
    {
        $comments = Comment::apiFilter($id);
        $formattedComments = [];
        foreach($comments as $comment) {
            $formattedComments[] = $comment->format('api');
        }
        return Response::json(
            array(
                'success' => true,
                'results' => count($formattedComments),
                'comments' => $formattedComments
            ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $ticket_id Optional ticket id. Used in the filter if populated.
     *
     * @return Response
     */
    public function store($ticket_id = null)
    {
        
        // Prepare the return value. Assume the worst!
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
            'id' => null
        );
        $returnCode = 400;
        
        $ticket_id = ($ticket_id > 0 ? $ticket_id : Request::input('ticket_id'));
        // Check that the user is allowed to create this ticket.
        if (Comment::allowed(null, 'insert', $ticket_id)) {
            // Start a new comment.
            $comment = new Comment;
            // Get the field values for the comment they are creating.
            $comment->ticket_id = $ticket_id;
            $added_by_user_id = Request::input('added_by_user_id');
            $added_by_user_id = $added_by_user_id > 0 ? $added_by_user_id : Auth::user()->id;
            $comment->added_by_user_id = $added_by_user_id;
            $comment->details = Request::input('details');
            // Save the new comment.
            if ($comment->isValid()) {
                $comment->save();
                $commentId = $comment->id;
                if ($commentId > 0) {
                    // Successful.
                    $result['success'] = true;
                    $result['message'] = null;
                    $result['id'] = $commentId;
                    $returnCode = 201;
                } else {
                    $result['message'] = 'Failed to save the new comment. Make sure mandatory fields are present.';
                }
            } else {
                $result['message'] = 'Comment is not valid. Make sure mandatory fields are present.';
            }
        } else {
            $result['message'] = 'Creating comments on tickets is forbidden on this project';
            $returnCode = 403;
        }
        
        return Response::json(
            $result,
            $returnCode
        );
    }
    
}
