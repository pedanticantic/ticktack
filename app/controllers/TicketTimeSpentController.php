<?php

class TicketTimeSpentController extends BaseController {
    
    /**
     * Method to handle time being recorded against a ticket.
     * Most of the values are posted via a form.
     * We assume the time is being recorded for the logged-in user.
     *
     * @param int $ticketId The id of source ticket.
     */
    public function handleCreate($ticketId) {
        $startDatetime = Input::get('start_datetime');
        $duration = Input::get('duration');
        $notes = Input::get('notes');
        $result = TimeSpent::attemptToCreate($ticketId, Auth::id(), $startDatetime, $duration, $notes);
        if ($result !== true) {
            if (is_array($result)) {
                $result = $result[0];
            }
            return Redirect::to('ticket/' . $ticketId . '#ticketTimeSpent_link')
                ->with('errorTimeSpent', $result)
                ->with('startDatetimeFromForm', $startDatetime)
                ->with('durationFromForm', $duration)
                ->with('notesFromForm', $notes);
        }
        
        // Now redisplay the ticket. We want to scroll down to where the relationships are displayed.
        return Redirect::to('ticket/' . $ticketId . '#ticketTimeSpent_link')->with('messageTimeSpent', 'Your time was recorded.');
    }
    
    /**
     * Method to allow people to record time spent via the API.
     * @param int $ticketId The id of the ticket that we're recording time against.
     *
     * @return Response
     */
    public function store($ticketId) {
        
        // Prepare the return value. Assume the worst!
        $result = array(
            'success' => false,
            'message' => 'Unknown error',
            'id' => null
        );
        $returnCode = 400;
        // Check that the user is allowed to create this time record.
        if (TimeSpent::allowed(null, 'insert', $ticketId)) {
            $startDatetime = Input::get('start_datetime');
            $duration = Input::get('duration');
            $notes = Input::get('notes');
            $timeResult = TimeSpent::attemptToCreate($ticketId, Auth::id(), $startDatetime, $duration, $notes);
            if ($timeResult === true) {
                // Successful.
                $result['success'] = true;
                $result['message'] = null;
                $result['id'] = -1; // @TODO: We need to return the new id.
                $returnCode = 201;
            } else {
                $result['message'] = 'Failed to save the new time spent record. Error: ' . (is_array($timeResult) ? $timeResult[0] : $timeResult);
            }
        } else {
            $result['message'] = 'Recording time on tickets is forbidden on this project';
            $returnCode = 403;
        }
        
        return Response::json(
            $result,
            $returnCode
        );
    }
                
}
