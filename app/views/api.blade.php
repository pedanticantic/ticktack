@extends('layouts.master')

@section('content')
<h2 >API Specification - v1</h2>

<p>Ticktack has a RESTful API that any authorised user can use.</p>
<p>Below is the specification</p>

<h3>Calling the API</h3>

<p>You need to authenticate with a user. You can use the same user that you log in with,
or you can use apiuser/apipassword for a slightly restricted result set (not implemented yet).</p>

<p>You can use any application to call the API, but I will assume command-line cURL requests:</p>
<pre >$ curl --user apiuser:apipassword http://ticktack.pedanticantic.click/api/v1/&lt;resource&gt;</pre>

<p>On the production system, the Project "TickTack" and all descendant records are completely
read-only for all users. You will not even be able to add a comment to a ticket in that project.</p>

<h3>Resources</h3>

<h4>/api/v1/referenceData</h4>

GET: /api/v1/referenceData to return the static reference data - various types, priorities, etc<br />

<h4>/api/v1/user</h4>

GET: /api/v1/user to return all users that are visible to you. You can filter on:<br />
<ul>
    <li>username: contains the text</li>
</ul>

<h4>/api/v1/project</h4>

GET: /api/v1/project/{id} to return the project with that id<br />

GET: /api/v1/project to return all projects, with optional filters:
<ul>
    <li>name: contains the text</li>
    <li>description: contains the text</li>
    <li>lead_user_id: exact match on user id</li>
</ul>

POST: /api/v1/project to create a project:
<ul>
    <li>name: mandatory</li>
    <li>description: mandatory</li>
    <li>lead_user_id: optional. Defaults to the authorisation user id.</li>
</ul>

PUT: /api/v1/project/{id} to update the project with that id.<br />
See POST above for valid fields.<br /><br />

DELETE: /api/v1/project/{id} to delete the project with that id.<br />

GET: /api/v1/project/{id}/ticket to return all tickets, with optional filters<br />
See "GET: /api/v1/ticket" below, with tickets filtered on the given project_id.<br /><br />

POST: /api/v1/project/{id}/ticket to create a ticket in the given project<br />
See "POST: /api/v1/ticket" below, with the project_id deemed to be the one in the URL.

<h4>/api/v1/ticket</h4>

GET: /api/v1/ticket/{id} to return the ticket with that id<br />
GET: /api/v1/ticket to return all tickets, with optional filters:
<ul>
    <li>project_id: exact match on project_id</li>
    <li>ticket_types: match any of the ticket types (comma-separated list) (invalid values are silently ignored)</li>
    <li>summary: contains the text</li>
    <li>description: contains the text</li>
    <li>created_by_user_id: exact match on created_by_user_id</li>
    <li>priorities: match any of the priorities (comma-separated list) (invalid values are silently ignored)</li>
    <li>statuses: match any of the statuses (comma-separated list) (invalid values are silently ignored)</li>
    <li>Assignments:</li>
    <ul>
        <li>assignment_user_ids: comma-separated list of user ids. Returns only tickets assigned to any of these users.</li>
        <li>assignment_level: either "primary" or "other". Ignored if assignment_user_ids is not present. Further filters on assignment level.</li>
    </ul>
</ul>
Returned sets of tickets are sorted by id and include any assignments, comments and relationships to other tickets<br /><br />

POST: /api/v1/ticket to create a ticket:
<ul>
    <li>project_id: mandatory</li>
    <li>ticket_type: mandatory</li>
    <li>summary: mandatory</li>
    <li>description: mandatory</li>
    <li>created_by_user_id: optional. If not present, defaults to authentication user.</li>
    <li>priority: optional. If not present, defaults to "medium"</li>
    <li>status: optional. If not present, defaults to "new"</li>
    <li>due_datetime: optional. Must be of the form "YYYY-MM-DD HH24:MI:SS"</li>
    <li>estimated_length_seconds: optional. Only accepts formatted values, such as "4h", "1w 3d 12h", "30m"</li>
</ul>

PUT: /api/v1/ticket/{id} to update the ticket with that id.<br />
See POST above for valid fields.<br />
Use "-" to clear optional fields (due_datetime and estimated_length_seconds)

DELETE: /api/v1/ticket/{id} to delete the ticket with that id.

<h4>/api/v1/ticket/{id}/assignment/{user_id}</h4>

POST: ticket/{id}/assignment/{user id} to assign the given user to the given ticket. Fields:<br />
<ul>
    <li>sort_order: optional. Inserts the assignment at this position in the list. Goes at the bottom if not populated.
</ul>
Returns an error if the user is already assigned.<br />
Returns the new assignment id on success.

<h4>/api/v1/assignment/{id}[/{direction}]</h4>

DELETE: /api/v1/assignment/{id} to remove the given assignment<br />
Returns an error if assignment does not exist.<br /><br />

PUT: /api/v1/assignment/{id}/{direction} to move the assignment up or down within the ticket.
<ul >
    <li>id: the id of the assignment record (returned by the the "create" endpoint above).</li>
    <li>direction: either "up" or "down".</li>
</ul>
Various error messages are returned. If successful, the new sort order is returned.

<h4>/api/v1/ticket/{id}/comment</h4>

GET: /api/v1/ticket/{id}/comment/{id} to retrieve a specific comment<br />
GET: /api/v1/ticket/{id}/comment to retrieve all comments on the ticket, with optional filters<br />
GET: /api/v1/comment to retrieve all comments in the system, with optional filters<br />
Filters:
<ul>
    <li>ticket_id: not needed when the ticket is identified the URL.</li>
    <li>added_by_user_id: filters on the user who created the comment</li>
    <li>details: contains the text</li>
</ul>

POST: /api/v1/ticket/{id}/comment to create a comment against the given ticket<br />
<ul>
    <li>added_by_user_id: optional. Defaults to authorised user</li>
    <li>details: the comment itself</li>
</ul>

Currently, comments cannot be modifed nor deleted.

<h4>/api/v1/ticket/{id}/associated</h4>

Note: Relationships are always associated with 2 tickets. When accessing a relationship, you need
to specify which ticket is the "source". It is important because, for example, if ticket A is a parent
of ticket B, and you retrieve the relationship stating that B is the source, the relationship type
will be "child". Also, if you change the sort order, it's the one as seen from the ticket that you
say is the source.<br /><br />

GET: /api/v1/ticket/{id}/associated/{id} to retrieve a specific relationship<br />
GET: /api/v1/ticket/{id}/associated to retrieve all relationship on the ticket, with optional filters<br />
Filters:
<ul>
    <li>relationship_type</li>
    <li>target_ticket_id: the "other" ticket</li>
    <li>is_pinned: whether the relationship is pinned or not</li>
</ul>

POST: /api/v1/ticket/{id}/associated to create a relationship against the given ticket<br />
Fields as as per the filters above. is_pinned defaults to no.<br /><br />

DELETE: /api/v1/ticket/{id}/associated/{id} to delete the relationship. All sort orders will be updated as necessary.<br /><br />

PUT: /api/v1/ticket/{id}/associated/{relationship_id}/{action}: perform the action on the given relationship, where the ticket is the source ticket. action can be one of:
<ul>
    <li>pin: prevents it from being deleted until it is unpinned</li>
    <li>unpin: makes delete possible</li>
    <li>up: move the relationship up in the list of relationships against the source ticket</li>
    <li>down: move the relationship down in the list of relationships against the source ticket</li>
</ul>
@stop
