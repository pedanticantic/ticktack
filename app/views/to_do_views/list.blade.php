@extends('layouts.master')

@section('content')
<h3 >
    To Do Views
    <a href="#" data-toggle="modal" data-target="#toDoViewsHelp" title="Get help on To Do Views" style="vertical-align: top;" >
        <span class="glyphicon glyphicon-question-sign" ></span>
    </a>
</h3>

<p ><a href="/todoview/form"><span class="label label-primary">Add</span></a></p>

<?php
$toDoViews = ToDoView::getUserViews();
Log::debug('My views', $toDoViews->toArray());

if (count($toDoViews) == 0) {?>
    <p >You have no views</p>
<?php
} else {?>
    <table class="table table-striped" >
        <tr >
            <th >Your Views</th>
        </tr><?php
    foreach($toDoViews as $toDoView) {?>
        <tr >
            <td ><a href="todoview/{{{ $toDoView->id }}}" >{{{ htmlentities($toDoView->view_name) }}}</a></td>
        </tr><?php
    }?>
    </table><?php
}?>

<div class="modal fade" id="toDoViewsHelp" tabindex="-1" role="dialog" aria-labelledby="toDoViewsHelp" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">To Do Views</h4>
            </div>
            <div class="modal-body">
                <p >
                    To Do Views allow you to position and manage your tasks in a 2-dimensional GUI way.
                    You can have multiple views. You might use it for:
                    <ul >
                        <li>A Focus Priority List (a small set of tasks you must do today).</li>
                        <li>A "Friday Club", where you spend one day a week doing your own thing.</li>
                        <li>A complex task with many sub-tasks and relationships.</li>
                    </ul>
                </p>
                <p >
                    To add a ticket to a view, go to the ticket view page, click on the "To Do Views" tab,
                    then click "Add". Once you've added the ticket, you can click "Show" to navigate to the view.
                    A ticket can appear on multiple views.
                </p>
                <h3 >On the view screen</h3>
                <p >
                    You can drag and drop the canvas to move the viewport.
                    You can drag an individual ticket to move its position relative to the others.
                    You can remove a ticket from the view by click the red X.
                </p>
                <p >
                    Related ticket ids are shown. If the related ticket is also on this view,
                    a relationship line is shown. The lines have a style associated with the
                    relationship type. You can double-click the related ticket id.
                    <ul >
                        <li>If the related ticket is not on the view, the system will add it.</li>
                        <li>If the related ticket is on the view, it will navigate to it, centring it in the viewport.</li>
                    </ul>
                </p>
                <p ></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@stop
