@extends('layouts.master')

@section('content')
<h3 >To Do View</h3>

<?php
if (isset($messages)) {
    Log::debug('All messages', $messages);
    if (count($messages) > 0) {
        foreach($messages as $message) {?>
            <div class="alert alert-danger" role="alert">
                <strong>Error: </strong>{{{ $message }}}
            </div><?php
        }
    }
}?>

<?php
$id = isset($toDoView) ? $toDoView->id : '';
$name = isset($toDoView) ? $toDoView->view_name : '';
?>
<form method="post" action="/todoview/form"><?php
    if ($id != null) {?>
        <input type="hidden" name="id" value="{{{ htmlentities($id) }}}" /><?php
    }?>
    Name: <input type="text" name="view_name" id="view_name" size="60" maxlength="60" value="{{{ htmlentities($name) }}}" />
    <button class="btn btn-lg btn-primary pull-right" type="submit">Save View</button>
</form>

<br />
<a href="/todoviews"><span class="label label-primary">Cancel</span></a>
@stop
