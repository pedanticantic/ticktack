@extends('layouts.master')

@section('content')
<h3 >To Do View</h3>

<a href="/todoview/form/{{{ $toDoView->id }}}" class="pull-right" ><span class="label label-primary">Rename</span></a>

<h4 >{{{ htmlentities($toDoView->view_name) }}}</h4>

<p ><a href="/todoviews" ><span class="label label-primary">Back to List</span></a></p>

<div id="tdv_viewport" >
    <div id="tdv_canvas_container" >
        <div id="tdv_canvas" data-view-id="{{{ $toDoView->id }}}" title="Click and drag to move the view around" >
            <!-- The magic will happen here! -->
        </div>
    </div>
</div>

<!-- Javascript and CSS specific to this page. -->
<link rel="stylesheet" href="/css/todoview.css">
<script src="/js/todoview.js"></script>

<div class="modal fade" id="toDoViewsEditSummary" tabindex="-1" role="dialog" aria-labelledby="toDoViewsEditSummary" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Ticket Summary</h4>
            </div>
            <div class="modal-body">
                <h2 >Global Summary</h2>
                <p id="ticket_global_summary" class="alert alert-info" role="alert" ></p>
                <h2 >View-specific Summary</h2>
                <p >
                    Enter the summary you want for this To-Do View. Leave blank to use the global summary.
                </p>
                <textarea id="ticket_view_summary" rows="5" ></textarea>
                <button type="button" class="btn btn-default pull-right" id="clear_summary" >Clear</button>
                <button type="button" class="btn btn-primary" id="save_summary" data-dismiss="modal">Save</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@stop
