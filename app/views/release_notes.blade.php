@extends('layouts.master')

@section('content')
<h3 >Release Notes</h3>

<p >Click here for the <a href="https://github.com/pedanticantic/ticktack/commits/master" target="_blank" title="Click to see commit history" >commit log</a> in github.</p>

<table class="table table-striped" >
    <tr >
        <th >Full Release</th>
        <th >Hotfix</th>
        <th >Date</th>
        <th >Details</th>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.12.3</td>
        <td >04/08/2015</td>
        <td >
            Ticket view now shows total time spent, even if no time recorded against that ticket.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.12.2</td>
        <td >01/08/2015</td>
        <td >
            Correction to the "Delete associated" endpoint in the API documentation.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.12.1</td>
        <td >01/08/2015</td>
        <td >
            Temporary fix to stop the API allowing you to create a ticket that is related to itself.
        </td>
    </tr>
    <tr >
        <td >0.12.0</td>
        <td ><br /></td>
        <td >26/07/2015</td>
        <td >
            Added API endpoints for recording time.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.11.3</td>
        <td >25/07/2015</td>
        <td >
            Another fix to the display of time remaining.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.11.2</td>
        <td >25/07/2015</td>
        <td >
            Now correctly displays a time remaining of zero.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.11.1</td>
        <td >25/07/2015</td>
        <td >
            Forgot to include a "notes" field for time recording.
        </td>
    </tr>
    <tr >
        <td >0.11.0</td>
        <td ><br /></td>
        <td >25/07/2015</td>
        <td >
            Allowed time to be recorded against tickets; time recorded "bubbles up" to parent ticket.
        </td>
    </tr>
    <tr >
        <td >0.10.0</td>
        <td ><br /></td>
        <td >24/07/2015</td>
        <td >
            Added unit tests for user registration.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.9.1</td>
        <td >23/07/2015</td>
        <td >
            Changing some wording.
            
        </td>
    </tr>
    <tr >
        <td >0.9.0</td>
        <td ><br /></td>
        <td >23/07/2015</td>
        <td >
            Adding API endpoints and general tweaks for the AngularJS version.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.8.1</td>
        <td >29/03/2015</td>
        <td >
            Bug in the Project droplist in Ticket search.
        </td>
    </tr>
    <tr >
        <td >0.8.0</td>
        <td ><br /></td>
        <td >29/03/2015</td>
        <td >
            Amended the logging to use the provided Log class.<br />
            Improved the security of some of the database queries.<br />
            Converted the views to use Blade.<br />
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.7.2</td>
        <td >29/03/2015</td>
        <td >
            Fixed bug when validating a blank due date - it was flagged as invalid.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.7.1</td>
        <td >29/03/2015</td>
        <td >
            Fixed a bug in assignment re-ordering.
        </td>
    </tr>
    <tr >
        <td >0.7.0</td>
        <td ><br /></td>
        <td >29/03/2015</td>
        <td >
            Wrote the initial suite of Unit Tests.<br />
            Updated controllers and models as a result of Unit Tests.<br />
            Small improvement to the API code.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.6.2</td>
        <td >15/03/2015</td>
        <td >
            I had used the dev URL in the API documentation. It's now the live one.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.6.1</td>
        <td >15/03/2015</td>
        <td >
            Noticed the main screen said the API wasn't implemented - it is!
        </td>
    </tr>
    <tr >
        <td >0.6.0</td>
        <td ><br /></td>
        <td >15/03/2015</td>
        <td >
            Implemented the API. It covers projects, tickets, assignments, comments and relationships.
        </td>
    </tr>
    <tr >
        <td >0.5.0</td>
        <td ><br /></td>
        <td >15/03/2015</td>
        <td >
            Allowed a ticket summary to be tailored for a specific To-Do View.
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.4.1</td>
        <td >24/01/2015</td>
        <td >
            Made the animations on "To do views" compatible with all browsers.
        </td>
    </tr>
    <tr >
        <td >0.4.0</td>
        <td ><br /></td>
        <td >17/01/2015</td>
        <td >
            Added "To do views" so users can organise tickets in an intuitive way
        </td>
    </tr>
    <tr >
        <td >0.3.0</td>
        <td ><br /></td>
        <td >09/12/2014</td>
        <td >
            Added these release notes.<br />
            Fixed issue 31 re. double quotes in the ticket summary field.<br />
            App version number is now stored in a class, and is accessed via a "getter" method.<br />
            In project view, replaced list of its tickets, with counts of its tickets in various states as links to those tickets (ticket 19).
        </td>
    </tr>
    <tr >
        <td ><br /></td>
        <td >0.2.1</td>
        <td >08/12/2014</td>
        <td >Corrected the version number link in the nav bar (now goes to the commit log).</td>
    </tr>
    <tr >
        <td >0.2.0</td>
        <td ><br /></td>
        <td >08/12/2014</td>
        <td >Lots of tidying up and adding small features ready for the first "public" release.</td>
    </tr>
    <tr >
        <td >0.1.0</td>
        <td ><br /></td>
        <td >29/11/2014</td>
        <td >Basic system working.</td>
    </tr>
</table>
@stop
