@extends('layouts.master')

@section('content')
<form role="form" method="post" >
    
    <?php
    $projectid = isset($project) ? $project->id : '';
    ?>
    <h2 >{{{ $projectid ? 'Edit' : 'Add' }}} Project</h2>
    
    <?php
    $projectname = isset($project) ? $project->name : '';
    $description = isset($project) ? $project->description : '';
    $projectuser = isset($project) ? $project->lead_user_id : '';
    if (isset($err) && is_array($err)) {
        foreach($err as $err_msg) {
            ?><div role="alert" class="alert alert-danger">
                <strong >Error: </strong>{{{ $err_msg }}}
              </div><?php
        }
    }
    ?>
    
    <table class="table" >
        
        <tbody >
            <tr >
                <th class="text-right">Name</th>
                <td><input type="text" name="projectname" value="{{{ $projectname }}}" maxlength="30" /></td>
            </tr>
            <tr >
                <th class="text-right">Description</th>
                <td><textarea name="description" rows="10">{{{ htmlentities($description) }}}</textarea></td>
            </tr>
            <tr >
                <th class="text-right">Lead User</th>
                <td><select name="projectuser" projectuser>
                    <option value="" >Select...</option>
                    <?php
                        foreach($allUsers as $oneUser) {
                            ?><option value="{{{ $oneUser->id }}}" {{{ ($oneUser->id == $projectuser) ? 'selected="selected" ' : '' }}}>
                            {{{ $oneUser->username }}}
                            </option><?php
                        }
                    ?>
                </select> (will use AJAX in future)</td>
            </tr>
            <tr >
                <td colspan="2" class="text-center" ><button type="submit" class="btn btn-lg btn-primary">Save</button></td>
            </tr>
        </tbody>
        
    </table>
    
</form>

<p >
    <?php
    if ($projectid > 0) {?>
        <a href="/project/{{{ $projectid }}}" ><span class="label label-primary">Cancel</span></a><?php
    }?>
    <a href="/projects" ><span class="label label-primary">Back to projects</span></a>
</p>
@stop