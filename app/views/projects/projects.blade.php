@extends('layouts.master')

@section('content')
<h1 >Projects</h1>

<?php
if (count($projects) == 0) {
    ?>
    <p >There are currently no projects</p>
    <?php
} else {
    ?>
    <table class="table table-striped" >
        <tbody >
            <tr >
                <th >Id</id>
                <th >Name</id>
                <th >Lead User</id>
                <th >Options</id>
            </tr>
            <?php
            foreach($projects as $project) {?>
                <tr >
                    <td >{{{ $project->id }}}</td>
                    <td >{{{ $project->name }}}</td>
                    <td >{{{ $project->user->username }}}</td>
                    <td >
                        <a href="/project/{{{ $project->id }}}" ><span class="label label-primary">View</span></a>
                        <a href="/project/edit/{{{ $project->id }}}" ><span class="label label-primary">Edit</span></a>
                        <a href="/project/{{{ $project->id }}}/ticket/create" ><span class="label label-primary">Add Ticket</span></a>
                    </td>
                </tr><?php
            }
            ?>
        </tbody>
    </table>
    <?php
}
?>

<p ><a href="/project/create" ><span class="label label-primary">Create</span></a></p>
@stop
