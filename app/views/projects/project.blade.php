@extends('layouts.master')

@section('content')
<h1 >Project</h1>

<?php
if (Session::has('message')) {?>
    <div role="alert" class="alert alert-success">
        <strong>Success!</strong> {{{ Session::get('message') }}}
      </div><?php
}
?>

<h2 >{{{ $project->name }}}</h2>

<table class="table" >
    <tbody >
        <tr >
            <th >Description</th>
            <td >{{ $project->descriptionForView() }}</td>
        </tr>
        <tr >
            <th >Lead User</th>
            <td >{{{ $project->user->username }}}</td>
        </tr>
    </tbody>
</table>

<p >
    <a href="/projects" ><span class="label label-primary">Back to projects</span></a>
    <a href="/project/edit/{{{ $project->id }}}" class="pull-right" ><span class="label label-primary">Edit</span></a>
</p>

<h2 >Project Tickets</h2>

<?php
$tickets = $project->tickets;
if (count($tickets) == 0) {?>
    <p >This project has no tickets</p><?php
} else {?>
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">By Status</h3>
        </div>
        <div class="panel-body">
            <?php $ticketSummary = $project->getTicketSummaryByStatus(); ?>
            <?php foreach($ticketSummary as $status => $summaryLine) {?>
                <a href="/tickets/filter/{{{ $summaryLine['filter'] }}}" ><span class="badge">{{{ $summaryLine['count'] }}}</span> {{{ $status }}}</a><br /><?php
            } ?>
        </div>
    </div>
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Open Tickets By Priority</h3>
        </div>
        <div class="panel-body">
            <?php $ticketSummary = $project->getOpenTicketSummaryByPriority(); ?>
            <?php foreach($ticketSummary as $priority => $summaryLine) {?>
                <a href="/tickets/filter/{{{ $summaryLine['filter'] }}}" ><span class="badge">{{{ $summaryLine['count'] }}}</span> {{{ $priority }}}</a><br /><?php
            } ?>
        </div>
    </div>
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Your Open Tickets By Assignment Type</h3>
        </div>
        <div class="panel-body">
            <?php $ticketSummary = $project->getMyOpenTicketSummaryByAssignmentType(); ?>
            <?php foreach($ticketSummary as $assignmentType => $summaryLine) {?>
                <a href="/tickets/filter/{{{ $summaryLine['filter'] }}}" ><span class="badge">{{{ $summaryLine['count'] }}}</span> {{{ $assignmentType }}}</a><br /><?php
            } ?>
        </div>
    </div>
    
<?php
}?>

<p >
    <a href="/project/{{{ $project->id }}}/ticket/create" ><span class="label label-primary">Add Ticket</span></a>
</p>
@stop
