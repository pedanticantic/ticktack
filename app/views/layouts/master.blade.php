<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TickTack - The Ticket Tracking System</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:700);
        .center-block {
          display: block;
          margin-left: auto;
          margin-right: auto;
        }
    </style>
</head>
<body>
    @section('sidebar')
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">TickTack</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse"><?php
            if (Auth::check()) {
                // Only show certain links if the user is logged in.
                ?>
                <div id="navbar-left" class="">
                    <ul class="nav navbar-nav">
                        <li><a href="/projects">Projects</a></li>
                        <li><a href="/tickets">Tickets</a></li>
                        <li><a href="/ticket/create">Add Ticket</a></li>
                        <li><a href="/todoviews">To Do Views</a></li>
                    </ul>
                </div><?php
                }?>
                <div id="navbar-right" class="navbar-right">
                    <ul class="nav navbar-nav"><?php
                    if (Auth::check()) {
                        // If logged in, show the ticket search box and "log out" link.
                        ?>
                        <li style="padding-top: 10px" ><form action="/tickets/search" method="post" ><input type="text" name="quick_search" placeholder="Search..." title="Search within open tickets" ></form></li>
                        <li><a href="/logout">Log out ({{{ Auth::user()->username }}})</a></li><?php
                    } else {
                        // Otherwise, show "Log In" and "Register"
                        ?>
                        <li><a href="/login">Log in</a></li>
                        <li><a href="/register">Register</a></li><?php
                    }?>
                        <li><a href="/api" title="The API specification" >API</a></li>
                        <li><a href="/release_notes" title="Click to see release notes" >v{{{ TickTack::getVersion() }}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    @show
    
    <div class="container theme-showcase" role="main">
        
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <?php
            if (Session::has('error')) {?>
                <br />
                <div role="alert" class="alert alert-danger">
                    <strong>Error!</strong> {{{ Session::get('error') }}}
                </div><?php
            }
            ?>
            
            @yield('content')
        </div>
    </div>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <!-- JQuery UI -->
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
    <!-- The javascript and CSS for this project -->
    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/ticktack.js"></script>
    <link rel="stylesheet" href="/css/ticktack.css">
    <!-- The javascript and CSS for the datetime picker -->
    <link rel="stylesheet" type="text/css" href="/datetimepicker-master/jquery.datetimepicker.css"/ >
    <script src="/datetimepicker-master/jquery.datetimepicker.js"></script>
</body>
</html>
