@extends('layouts.master')

@section('content')
<form role="form" method="post" >
    
    <h2 >Log In</h2>
    
    <?php
    if (isset($err) && is_array($err)) {
        foreach($err as $err_msg) {
            ?><div role="alert" class="alert alert-danger">
                <strong >Error: </strong>{{{ $err_msg }}}
              </div><?php
        }
    }
    ?>
    
    <table class="table" >
        
        <tbody >
            <tr >
                <th class="text-right">User name</th>
                <td><input type="text" name="username" value="{{{ $username }}}" maxlength="30" /></td>
            </tr>
            <tr >
                <th class="text-right">Password</th>
                <td><input type="password" name="password" maxlength="30" /></td>
            </tr>
            <tr >
                <td colspan="2" class="text-center" ><button type="submit" class="btn btn-lg btn-primary">Log in</button></td>
            </tr>
        </tbody>
        
    </table>
    
</form>
@stop
