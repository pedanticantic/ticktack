@extends('layouts.master')

@section('content')
<h1>TickTack</h1>
<p>The task tracking system, by <a href="http://uk.linkedin.com/in/glennjenkinswebdeveloper/" target="new" >Glenn Jenkins</a></p>
<h2 >The aims</h2>
<P ><span class="label label-warning">Warning</span> This is not a commercial product!</p>
<p >It exists to allow me to:</p>
<ul>
    <li >hone my skills</li>
    <li >learn new skills</li>
    <li >demonstrate my abilities to potential employers, if I ever look for a new job</li>
</ul>
<h2 >Be Aware</h2>
<p ><span class="label label-danger">Danger</span> Please assume all data you enter on this site will be publicly visible</p>
<p ><span class="label label-danger">Danger</span> I reserve the right to delete any data on this system, including users, projects, tickets, comments, etc</p>
<P ><span class="label label-info">Note</span> I have used JIRA in the past, so any resemblance is unintentional</p>
<h2 >Now, have some fun</h2>
<p >Please register and/or log in and have some fun</p>

<!-- Now a couple of panels for highlighting main technical features and major items to do -->

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Main Technical Features</h3>
    </div>
    <div class="panel-body">
        <ul >
            <li ><a href="http://laravel.com/" target="_blank" >Laravel MVC framework</a> and <a href="http://getbootstrap.com/" target="_blank" >Twitter Bootstrap GUI</a>.</li>
            <li >Ticket descriptions &amp; comments support <a href="http://en.wikipedia.org/wiki/Markdown" target="_blank" >markdown</a>; used <a href="http://parsedown.org/" target="_blank" >Parsedown</a> to display</li>
            <li >Tickets can be related to other tickets and have:
                <ul >
                    <li >multiple parents</li>
                    <li >multiple children</li>
                    <li >Unlimited levels of parent-child relationships</li>
                    <li >dependencies</li>
                    <li >associations (ie "See also")</li>
                    <li >Can be defined in the ticket description, or explicitly</li>
                </ul>
            <li >Date/time chooser courtesy of <a href="https://github.com/xdan/datetimepicker" target="_blank" >datetimepicker</a>.</li>
            <li >Project "TickTack" in this application contains tickets for future fixes/enhancements. Register/log in to see them.</li>
        </ul>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">What I Want You To Do</h3>
    </div>
    <div class="panel-body">
        <p >I want to be as good as I possibly can be at all aspects of (LAMP) web development. My skills have become a little out of date, hence this project.</p>
        <p >I need your help to point out where I can improve, things I should investigate, etc. I don't mind if you focus on one or two areas and give me feedback on them. Please use this application and its <a href="https://github.com/pedanticantic/ticktack" target="_blank" >GitHub repository</a>. Be as frank as you like - I'm very keen to improve. For your feedback, either add tickets or comments, or email me.</p>
        <p >Areas to consider (ie where I want to improve):
            <ul >
                <li >MVC generally</li>
                <li >Security</li>
                <li >Unit Tests (not implemented yet)</li>
                <li >API</li>
                <li >JavaScript, JQuery (and AJAX, DOM manipulation) (not much in there at the moment)</li>
                <li >Appearance, RWD</li>
                <li >Ease of use, quality of features</li>
                <li >Readability of code (PHP, JavaScript)</li>
                <li >Automation - SASS, Composer, etc (not started yet)</li>
                <li >Smartphone App (not started yet)</li>
                <li >Anything else you can think of!</li>
            </ul>
        </p>
    </div>
</div>
@stop
