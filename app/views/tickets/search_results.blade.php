@extends('layouts.master')

@section('content')
<h2 >Tickets</h2>

<p >
    <a href="#" data-toggle="modal" data-target="#searchCriteria" >
        <button class="btn btn-lg btn-primary">Enter/change/clear search criteria</button>
    </a>
</p>

<?php
if (isset($tickets) && count($tickets) > 0) {?>
    <p >Found {{{ count($tickets) }}} ticket(s)</p>
    <table class="table table-striped" >
        <tbody >
            <tr >
                <th >Id</th>
                <th >Project</th>
                <th >Type</th>
                <th >Summary</th>
                <th >Primary Assignee</th>
                <th >Priority</th>
                <th >Status</th>
                <th >Options</th>
            </tr><?php
            foreach($tickets as $ticket) {?>
                <tr >
                    <td >{{{ $ticket->id }}}</td>
                    <td >{{{ htmlentities($ticket->project->name) }}}</td>
                    <td >{{ $ticket->getTypeImageObject() }}</td>
                    <td >{{{ htmlentities($ticket->summary) }}}</td>
                    <td >{{{ $ticket->primaryAssignee() }}}</td>
                    <td >{{{ $ticket->priority_db_to_screen() }}}</td>
                    <td >{{ $ticket->getStatusImageObject() }}</td>
                    <td >
                        <a href="/ticket/{{{ $ticket->id }}}" ><span class="label label-primary">View</span></a>
                        <a href="/ticket/edit/{{{ $ticket->id }}}" ><span class="label label-primary">Edit</span></a>
                    </td>
                </tr><?php
            }?>
        </tbody>
    </table>
    <?php
} else {?>
    <p >No tickets found</p><?php
}?>

<div class="modal fade" id="searchCriteria" tabindex="-1" role="dialog" aria-labelledby="searchCriteria" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/tickets/search" method="post" id="search_criteria" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Enter search criteria</h4>
                </div>
                <div class="modal-body">
                    <?php
                    // We need an instance of the ticket search class so that it knows what values to prefill/pre-select, etc.
                    $ticketSearch = new TicketSearch();
                    ?>
                    <div style="height: 500px; overflow: auto;" >
                        <table class="table" >
                            <tr >
                                <th >Id(s) (comma-separated)</th>
                                <td ><input type="text" name="ids" value="{{{ $ticketSearch->getIdsSearchValue() }}}" /></td>
                            </tr>
                            <tr >
                                <th >Type</th>
                                <td >
                                    <select name="ticket_type" >
                                        <option value="" >Select...</option><?php
                                        foreach($ticketSearch->getTypeSearchOptions() as $option) {?>
                                            <option value="{{{ $option['type'] }}}" {{{ $option['selected'] }}} >{{{ $option['desc'] }}}</option><?php
                                        }?>
                                    </select>
                                </td>
                            </tr>
                            <tr >
                                <th >Text</th>
                                <td >
                                	<input type="text" name="text" value="{{{ $ticketSearch->getTextSearchValue() }}}" /><br />
                                	in... <?php
                                	foreach($ticketSearch->getTextSearchOptions() as $id => $option) {?>
                                		<label for="{{{ $id }}}"><input type="checkbox" name="{{{ $id }}}" id="{{{ $id }}}" value="y" {{{ $option['value'] }}} /> {{{ $option['prompt'] }}}</label><?php
                                	}?>
                                </td>
                            </tr>
                            <tr >
                                <th >Status</th>
                                <td ><?php
                                	foreach($ticketSearch->getStatusSearchOptions() as $option) {?>
                                		<label for="{{{ $option['id'] }}}"><input type="checkbox" name="{{{ $option['id'] }}}" id="{{{ $option['id'] }}}" value="y" {{{ $option['selected'] }}} /> {{{ $option['desc'] }}}</label>
                                		<br /><?php
                                	}?>
                                	(ask the model for the open/closed, hierarchy, and whether each one was selected)
                                </td>
                            </tr>
                            <tr >
                                <th >Assignee</th>
                                <td >
                                	<select name="assignee" >
                                		<option value="" >Select...</option><?php
    	                            	foreach($ticketSearch->getAssigneeSearchOptions() as $option) {?>
                                			<option value="{{{ $option['id'] }}}" {{{ $option['selected'] }}} >{{{ $option['name'] }}}</option><?php
                                		}?>
                                	</select>
                                	<br />
                                	as... <?php
    	                            foreach($ticketSearch->getAssigneeScopeOptions() as $id => $option) {?>
                                		<label for="{{{ $id }}}"><input type="radio" name="assignee_scope" id="{{{ $id }}}" value="{{{ $id }}}" {{{ $option['selected'] }}} /> {{{ $option['desc'] }}}</label><?php
                                	}?>
                                </td>
                            </tr>
                            <tr >
                                <th >Project</th>
                                <td >
                                    <select name="project" >
                                        <option value="" >Select...</option><?php
                                        foreach($ticketSearch->getProjectSearchOptions() as $option) {?>
                                            <option value="{{{ $option['id'] }}}" {{{ $option['selected'] }}} >{{{ $option['name'] }}}</option><?php
                                        }?>
                                    </select>
                                </td>
                            </tr>
                            <tr >
                                <th >Priority</th>
                                <td >
                                    <select name="priority" >
                                        <option value="" >Select...</option><?php
                                        foreach($ticketSearch->getPrioritySearchOptions() as $option) {?>
                                            <option value="{{{ $option['id'] }}}" {{{ $option['selected'] }}} >{{{ $option['desc'] }}}</option><?php
                                        }?>
                                    </select>
                                    (add "or higher", "only", "or lower")
                                </td>
                            </tr>
                            <tr >
                                <th >Due Date</th>
                                <td >
                                    Between <input type="text" name="due_date_from" value="{{{ $ticketSearch->getDueDateFrom() }}}" class="datetimepicker" /><br />
                                    and <input type="text" name="due_date_to" value="{{{ $ticketSearch->getDueDateTo() }}}" class="datetimepicker" />
                                </td>
                            </tr>
                            <tr >
                                <th >Estimated Time</th>
                                <td >
                                    Between <input type="text" name="est_time_from" value="{{{ $ticketSearch->getEstTimeFrom() }}}" /><br />
                                    and <input type="text" name="est_time_to" value="{{{ $ticketSearch->getEstTimeTo() }}}" />
                                </td>
                            </tr>
                            <tr >
                                <th >Created By</th>
                                <td >
                                    <select name="created_by" >
                                        <option value="" >Select...</option><?php
                                        foreach($ticketSearch->getCreatedBySearchOptions() as $option) {?>
                                            <option value="{{{ $option['id'] }}}" {{{ $option['selected'] }}} >{{{ $option['name'] }}}</option><?php
                                        }?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-primary pull-right" >Ok</button>
                    <button type="cancel" class="btn btn-primary" data-dismiss="modal" >Cancel</button>&nbsp;
                    <button type="submit" class="btn btn-primary pull-left" name="clear" value="clear" >Clear Criteria</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
