@extends('layouts.master')

@section('content')
<h1 >Ticket</h1>

<?php
if (Session::has('message')) {?>
    <div role="alert" class="alert alert-success">
        <strong>Success!</strong> {{{ Session::get('message') }}}
      </div><?php
}
?>

<table class="table" >
    
    <tbody >
        <tr >
            <th class="text-right">Type</th>
            <td>{{{ $ticket->typeForView() }}}</td>
        </tr>
        <tr >
            <th class="text-right">Summary</th>
            <td>{{ htmlentities($ticket->summary) }}</td>
        </tr>
        <tr >
            <th class="text-right">Project</th>
            <td>{{{ htmlentities($ticket->project->name) }}}</td>
        </tr>
        <tr >
            <th class="text-right">Description</th>
            <td>{{ $ticket->descriptionForView() }}</td>
        </tr>
        <tr >
            <th class="text-right">Created By User</th>
            <td>{{{ $ticket->createdByUser->username }}}</td>
        </tr>
        <tr >
            <th class="text-right">Priority</th>
            <td>{{{ $ticket->priority_db_to_screen() }}}</td>
        </tr>
        <tr >
            <th class="text-right">Status</th>
            <td>{{{ $ticket->status_db_to_screen() }}}</td>
        </tr>
        <tr >
            <th class="text-right">Due Date/Time</th>
            <td>{{{ DatetimeManager::datetimeISOToUK($ticket->due_datetime) }}}</td>
        </tr>
        <tr >
            <th class="text-right">Estimated Length</th>
            <td>{{{ $ticket->length_db_to_screen() }}}</td>
        </tr>
        <tr >
            <th class="text-right">Assigned To</th>
            <td><?php
                $assignments = $ticket->assignments;
                if (count($assignments) == 0) {?>
                    No-one<?php
                } else {
                    echo $ticket->assignedForView(true);
                }?>
            </td>
        </tr>
    </tbody>
</table>

<p >
    <a href="/project/{{{ $ticket->project_id }}}" ><span class="label label-primary">Back to project</span></a>
    <a href="/ticket/edit/{{{ $ticket->id }}}" class="pull-right" ><span class="label label-primary">Edit</span></a>
</p>

<?php
    // Populate objects containing the related records.
    $comments = $ticket->getCommentsForView();
    $related = TicketAssociationView::where('src_ticket_id', '=', $ticket->id)->get();
    $totalTimeSpent = $ticket->getTimeSpentTotals(); // Returns an array containing the various values.
?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#ticketComments" id="ticketComments_link" >Comments <span class="badge">{{{ count($comments) }}}</span></a></li>
    <li><a data-toggle="tab" href="#ticketRelatedTickets" id="ticketRelatedTickets_link" >Related Tickets <span class="badge">{{{ count($related) }}}</span></a></li>
    <li><a data-toggle="tab" href="#ticketToDoView" id="ticketToDoView_link" >To Do Views</a></li>
    <li>
        <a data-toggle="tab" href="#ticketTimeSpent" id="ticketTimeSpent_link" >Time Spent
            <span class="badge">
                {{{ DatetimeManager::intervalToEnglish($totalTimeSpent['total_spent']) }}}
                |
                {{{ DatetimeManager::intervalToEnglish($totalTimeSpent['total_spent_inc_children']) }}}
            </span>
        </a>
    </li>
</ul>

<div class="tab-content">
    
    <div id="ticketComments" class="tab-pane fade in active panel panel-default">
        <div class="panel-body">
            <?php
            if (count($comments) == 0) {?>
                <p >There are no comments for this ticket.</p><?php
            }?>

            <p >
                <span class="label label-primary" id="toggle_add_comment" data-tt-add="Add Comment" data-tt-cancel="Cancel" >Add Comment</span>
            </p>
            <div id="add_comment" class="hide" style="margin-bottom: 4em;" >
                <form action="/ticket/{{{ $ticket->id }}}/comment/create" method="post" >
                    <textarea name="details" id="comment_box" rows="10" style="width: 98%" ></textarea>
                    <a href="#" data-toggle="modal" data-target="#commentHelp" title="Get help on this field" style="vertical-align: top;" >
                        <span class="glyphicon glyphicon-question-sign" ></span>
                    </a>
                    <button type="submit" class="btn btn-lg btn-primary pull-right">Save Comment</button>
                </form>
            </div>

            <?php
            if (count($comments) > 0) {
                foreach($comments as $comment) {?>
                    <div class="panel panel-default" >
                        <div class="panel-heading" >
                            <?php
                            echo $comment->user->username;?>
                            <span class="pull-right" title="{{{ DatetimeManager::howLongAgo($comment->created_at) }}}" >
                                {{{ DatetimeManager::datetimeISOToUK($comment->created_at) }}}
                            </span>
                        </div>
                        <div class="panel-body">
                            {{ $comment->detailsForView() }}
                        </div>
                    </div><?php
                }
            }?>
        </div>
    </div>
    
    <div id="ticketRelatedTickets" class="tab-pane fade panel panel-default">
        <div class="panel-body">
            <a href="#" data-toggle="modal" data-target="#RelatedTicketsHelp" class="pull-right" title="Get help on related tickets" >
                <span class="glyphicon glyphicon-question-sign" ></span>
            </a><?php
            if (Session::has('messageRelationship')) {?>
                <div role="alert" class="alert alert-success">
                    <strong>Success!</strong> {{{ Session::get('messageRelationship') }}}
                  </div><?php
            }
            if (Session::has('errorRelationship')) {?>
                <div role="alert" class="alert alert-danger">
                    <strong>Error!</strong> {{{ Session::get('errorRelationship') }}}
                  </div><?php
            }
            
            if (count($related) == 0) {?>
                <p >There are no tickets related to this ticket.</p><?php
            } else {?>
                <table class="table" >
                    <tbody >
                        <tr >
                            <th >This ticket is...</th>
                            <th >Ticket Id</th>
                            <th >Type</th>
                            <th >Ticket Summary</th>
                            <th >Status</th>
                            <th >Options</th>
                        </tr><?php
                        foreach($related as $index => $assoc) {?>
                            <tr >
                                <td >{{{ $assoc->getRelationshipTypeForView() }}}</td><?php
                                $ticketId = $assoc->trg_ticket_id;?>
                                <td >{{{ $ticketId }}}</td>
                                <td >{{ $assoc->trgTicket->getTypeImageObject() }}</td>
                                <td ><a href="/ticket/{{{ $ticketId }}}" >{{{ $assoc->trgTicket->summary }}}</a></td>
                                <td >{{ $assoc->trgTicket->getStatusImageObject() }}</td>
                                <td >{{ $assoc->getTicketRelationOptions($index, count($related)) }}</td>
                            </tr><?php
                        }?>
                    </tbody>
                </table><?php
            }
            // If there was an error, pretend the user clicked "Add Relation", in order to show
            // the form again with the entries from before. We do this by adding a class to the
            // "Add" button - the default Javascript will pick this up.
            $clickOnLoadClass = Session::has('errorRelationship') ? ' click-on-load' : '';
            $defaultRelationship = Session::has('relationshipTypeFromForm') ? Session::get('relationshipTypeFromForm') : '';
            $defaultTicketId = Session::has('trgTicketIdFromForm') ? Session::get('trgTicketIdFromForm') : '';?>
            <p >
                <span class="label label-primary{{{ $clickOnLoadClass }}}" id="toggle_add_relation" data-tt-add="Add Relation" data-tt-cancel="Cancel" >Add Relation</span>
            </p>
            <div id="add_relation" class="hide" style="margin-bottom: 4em;" >
                <form action="/ticket/{{{ $ticket->id }}}/relation/create" method="post" >
                    <p >This ticket is
                        <select name="relationship_type" ><?php
                            $types = TicketAssociationView::getRelationshipTypesForView();
                            foreach($types as $relationshipCode => $relationshipDesc) {
                                $selected = ($relationshipCode == $defaultRelationship) ? ' selected="selected"' : '';?>
                                <option value="{{{ $relationshipCode }}}" {{{ $selected }}}>{{{ $relationshipDesc }}}</option><?php
                            }?>
                        </select><?php
                        $selectTickets = Ticket::all();?>
                        <select name="trg_ticket_id" >
                            <option value="" >Select a ticket...</option><?php
                            foreach($selectTickets as $selectTicket) {
                                if ($selectTicket->id != $ticket->id) {
                                    $selected = ($selectTicket->id == $defaultTicketId) ? ' selected="selected"' : '';?>
                                    <option value="{{{ $selectTicket->id }}}" {{{ $selected }}}>{{{ $selectTicket->id . ': ' . htmlentities($selectTicket->summary) }}}</option><?php
                                }
                            }?>
                        </select>
                    </p>
                    <button type="submit" class="btn btn-lg btn-primary pull-right">Save Relation</button>
                </form>
            </div>
        </div>
    </div>
    
    <div id="ticketToDoView" class="tab-pane fade panel panel-default">
        <div class="panel-body"><?php
            // Get a list of all the user's To Do Views, and an indicator as to whether
            // this ticket is on each view.
            $toDoViews = $ticket->getUserToDoViews();
            if (count($toDoViews) == 0) {?>
                <p >You don't have any To Do Views</p><?php
            } else {?>
                <table class="table table-striped" >
                    <tr >
                        <th >Your view</th>
                        <th >Option</th>
                    </tr><?php
                foreach($toDoViews as $toDoView) {?>
                    <tr >
                        <td >{{{ htmlentities($toDoView->view_name) }}}</td>
                        <td ><?php
                    if ($toDoView->ticket_id == '') {?>
                        <a href="/todoview/{{{ $toDoView->id }}}/ticket/{{{ $ticket->id }}}/add" ><span class="label label-primary">Add</span><a><?php
                    } else {?>
                        <a href="/todoview/{{{ $toDoView->id }}}" ><span class="label label-primary">Show</span><a><?php
                    }?>
                        </td>
                    </tr><?php
                }?>
                </table><?php
            }?>
        </div>
    </div>
    
    <div id="ticketTimeSpent" class="tab-pane fade panel panel-default">
        <div class="panel-body"><?php
            // Get a list of all the time spent on this ticket.
            $timesSpent = $ticket->getTimesSpentForView();
            $defaultStartDatetime = Session::has('startDatetimeFromForm') ? Session::get('startDatetimeFromForm') : date('d/m/Y H:i');
            $defaultDuration = Session::has('durationFromForm') ? Session::get('durationFromForm') : '';
            $defaultNotes = Session::has('notesFromForm') ? Session::get('notesFromForm') : '';
            // If there was an error, pretend the user clicked "Record Time Spent", in order to show
            // the form again with the entries from before. We do this by adding a class to the
            // "Record" button - the default Javascript will pick this up.
            $clickOnLoadClass = Session::has('errorTimeSpent') ? ' click-on-load' : '';
            ?>
            <table class="table table-striped" >
                <tr >
                    <th >Start Time</th>
                    <th >Duration</th>
                    <th >User</th>
                    <th >Notes</th>
                </tr><?php
            if (count($timesSpent) == 0) {?>
                <tr ><td colspan="4" >No time has been recorded againt this ticket</td></tr><?php
            } else {
                foreach($timesSpent as $timesSpent) {?>
                <tr >
                    <td >{{{ DatetimeManager::datetimeISOToUK($timesSpent->start_datetime) }}}</td>
                    <td >{{{ $timesSpent->durationForView() }}}</td>
                    <td >{{{ $timesSpent->user->username }}}</td>
                    <td >{{{ $timesSpent->notes }}}</td>
                </tr><?php
                }
            }?>
                <tr >
                    <td ><br /></td>
                    <th >This Ticket</th>
                    <th colspan="2" >This Ticket and Children</th>
                </tr>
                <tr >
                    <th >Total Time Spent</th>
                    <td >{{{ DatetimeManager::intervalToEnglish($totalTimeSpent['total_spent']) }}}</td>
                    <td colspan="2" >{{{ DatetimeManager::intervalToEnglish($totalTimeSpent['total_spent_inc_children']) }}}</td>
                </tr>
                <tr >
                    <th >Estimated Time Remaining</th>
                    <td >{{{ DatetimeManager::intervalToEnglish($totalTimeSpent['total_remaining'], true) }}}</td>
                    <td colspan="2" >{{{ DatetimeManager::intervalToEnglish($totalTimeSpent['total_remaining_inc_children'], true) }}}</td>
                </tr>
            </table><?php
            
            if (Session::has('messageTimeSpent')) {?>
                <div role="alert" class="alert alert-success">
                    <strong>Success!</strong> {{{ Session::get('messageTimeSpent') }}}
                  </div><?php
            }
            if (Session::has('errorTimeSpent')) {?>
                <div role="alert" class="alert alert-danger">
                    <strong>Error!</strong> {{{ Session::get('errorTimeSpent') }}}
                  </div><?php
            }?>
            
            <p >
                <span class="label label-primary{{{ $clickOnLoadClass }}}" id="toggle_record_time_spent" data-tt-add="Record Time Spent" data-tt-cancel="Cancel" >Record Time Spent</span>
            </p>
            <div id="record_time_spent" class="hide" style="margin-bottom: 4em;" >
                <form action="/ticket/{{{ $ticket->id }}}/time_spent/create" method="post" >
                    <table >
                        <tbody>
                            <tr >
                                <th >Start Date/time</th>
                                <th >
                                    Duration
                                    <a href="#" data-toggle="modal" data-target="#durationHelp" title="Get help on the format of this field" >
                                        <span class="glyphicon glyphicon-question-sign" ></span>
                                    </a>
                                </th>
                                <th >Notes</th>
                            </tr>
                            <tr >
                                <td ><input type="datetime" name="start_datetime" id="start_datetime" value="{{{ $defaultStartDatetime }}}" class="datetimepicker" size="16" maxlength="16" /></td>
                                <td ><input type="text" name="duration" id="duration" value="{{{ $defaultDuration }}}" size="10" maxlength="20" /></td>
                                <td ><input type="text" name="notes" id="notes" value="{{{ $defaultNotes }}}" size="90" maxlength="250" placeholder="Tell me what you did..." /></td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-lg btn-primary pull-right">Save Time</button>
                </form>
            </div>
        </div>
    </div>
    
</div>

<div class="modal fade" id="commentHelp" tabindex="-1" role="dialog" aria-labelledby="commentHelp" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Comments</h4>
            </div>
            <div class="modal-body">
                <p >
                    This is for general comments relating to the ticket, eg notes on how to do the change, things to watch out for, etc.
                </p>
                <p >
                    You can use any of the <a href="http://en.wikipedia.org/wiki/Markdown" target="_blank" >markdown</a> syntax.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="RelatedTicketsHelp" tabindex="-1" role="dialog" aria-labelledby="RelatedTicketsHelp" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Related Tickets</h4>
            </div>
            <div class="modal-body">
                <p >You can create and maintain associations between records. They range from "See also" to full parent-child relationships.</p>
                <ul >
                    <li >You add relationships either by referencing another ticket in the description, or by explicitly creating one via the "Add Relation" button.</li>
                    <li >You delete a relationship either by removing it from the description, or (subject to some rules) clicking the "x" button in the list.</li>
                    <li >Types of relationship
                        <ul >
                            <li >"Associated with" just means "See also".</li>
                            <li >"dependent upon"/"dependency of" is for your use. The dependent record should be completed first, but the system does not enforce this.</li>
                            <li >"parent of"/"child of" is more strict. The system will not allow you to close a ticket until all its child, grandchild, etc tickets are closed. Time recorded against a ticket is propagated up to its parent, and its parent's parent, etc.</li>
                        </ul>
                    </li>
                    <li >If you reference a ticket in the description, you can "pin" the relationship, which will keep the relationship even if it's later removed from the description.</li>
                    <li >You cannot create a circular dependency and/or parent-child relationship, eg this is not allowed: A is a parent of B, B is dependent on C, C is a parent of A.</li>
                    <li >You can create as many levels of dependencies and parent-children as you like.</li>
                    <li >One ticket can have multiple parents.</li>
                    <li >When you add a relationship to a ticket, the (opposite) relationship is automatically applied to that other ticket back to this one.</li>
                    <li >The order of the tickets in the list is purely for your benefit. It might show the order you want to complete them in, or indicate the relative priorities, for example.</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Ok</button>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="durationHelp" tabindex="-1" role="dialog" aria-labelledby="estimatedLengthHelp" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Duration</h4>
            </div>
            <div class="modal-body">
                <p >This is the length of time you spent on this ticket (specifically a number of seconds)</p>
                <p >To make it easier, you enter the number of different units of time (minutes, hours, etc).
                    Enter the number, then the unit. You can enter more than one, and the times will be added.</p>
                <p >The units are:
                    <ul >
                        <li >m = minutes</li>
                        <li >h = hours</li>
                        <li >d = days</li>
                        <li >w = weeks</li>
                    </ul>
                </p>
                <p >The system will interpret your entries intelligently, so if you enter 48h, it will be deemed to be 2d. Also "30m 15m" will be deened to be "45m".</p>
                <p >Examples:
                    <ul >
                        <li >"30m" = 30 minutes"</li>
                        <li >"4h" = 4 hours"</li>
                        <li >"1d 12h" = 1 day, 12 hours (ie 1.5 days)</li>
                    </ul>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@stop
