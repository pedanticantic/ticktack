@extends('layouts.master')

@section('content')
<form role="form" method="post" >

    <?php
    $redisplay = isset($redisplay) ? $redisplay : false;
    $ticketId = isset($ticket) ? $ticket->id : '';
    $ticket_type = isset($ticket) ? $ticket->ticket_type : '';
    $summary = isset($ticket) ? $ticket->summary : '';
    $project_id = isset($ticket) ? $ticket->project_id : (Session::has('projectid') ? Session::get('projectid') : '' );
    $description = isset($ticket) ? $ticket->description : '';
    $created_by_user_id = isset($ticket) ? $ticket->created_by_user_id : (isset($current_user_id) ? $current_user_id : '');
    $priority = isset($ticket) ? $ticket->priority : '';
    $status = isset($ticket) ? $ticket->status : '';
    $due_datetime = $redisplay ? $due_datetime : (isset($ticket) ? $ticket->due_datetime_to_screen() : '');
    $estimated_length_seconds = $redisplay ? $estimated_length_seconds : (isset($ticket) ? $ticket->length_db_to_screen() : '');
    $assigned_to_user_id = isset($assigned_to_user_id) ? $assigned_to_user_id : '';
    ?>
    
    <h2 >{{{ $ticketId > 0 ? 'Edit' : 'Add' }}} Ticket</h2>
    
    <?php
    if (isset($err) && is_array($err)) {
        foreach($err as $err_msg) {
            ?><div role="alert" class="alert alert-danger">
                <strong >Error: </strong>{{{ $err_msg }}}
              </div><?php
        }
    }
    ?>
    
    <table class="table" >
        
        <tbody >
            <tr >
                <th class="text-right">Type</th>
                <td>
                    <select name="ticket_type" ><?php
                        foreach(Ticket::allTypes() as $oneType => $typeDesc) {?>
                            <option value="{{{ $oneType }}}" {{{ ($oneType == $ticket_type) ? 'selected="selected" ' : '' }}}>
                                {{{ $typeDesc }}}
                            </option><?php
                        }?>
                    </select>
                </td>
            </tr>
            <tr >
                <th class="text-right">Summary</th>
                <td><input type="text" name="summary" value="{{{ htmlentities($summary) }}}" maxlength="60" size="60" /></td>
            </tr>
            <tr >
                <th class="text-right">Project</th>
                <td>
                    <select name="project_id" ><?php
                        foreach($allProjects as $oneProject) {?>
                            <option value="{{{ $oneProject->id }}}" {{{ ($oneProject->id == $project_id) ? 'selected="selected" ' : '' }}}>
                                {{{ $oneProject->name }}}
                            </option><?php
                        }?>
                    </select>
                </td>
            </tr>
            <tr >
                <th class="text-right">Description</th>
                <td>
                    <textarea name="description" rows="10" cols="40" >{{{ $description }}}</textarea>
                    <a href="#" data-toggle="modal" data-target="#descriptionHelp" title="Get help on this field" style="vertical-align: top;" >
                        <span class="glyphicon glyphicon-question-sign" ></span>
                    </a>
                </td>
            </tr>
            <tr >
                <th class="text-right">Created By User</th>
                <td>
                    <select name="created_by_user_id" >
                        <option value="" >Select a user...</option>
                        <?php
                            foreach($allUsers as $oneUser) {
                                ?><option value="{{{ $oneUser->id }}}" {{{ ($oneUser->id == $created_by_user_id) ? 'selected="selected" ' : '' }}}>
                                {{{ $oneUser->username }}}
                                </option><?php
                            }
                        ?>
                    </select>
                    (Will be done with AJAX, etc eventually)
                </td>
            </tr>
            <tr >
                <th class="text-right">Priority</th>
                <td>
                    <select name="priority" >
                        <?php
                            foreach($allPriorities as $onePriority => $priorityDescription) {
                                ?><option value="{{{ $onePriority }}}" {{{ ($onePriority == $priority) ? 'selected="selected" ' : '' }}}>
                                {{{ $priorityDescription }}}
                                </option><?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr >
                <th class="text-right">Status</th>
                <td>
                    <select name="status" >
                        <?php
                            foreach($allStatuses as $oneStatus => $statusDescription) {
                                ?><option value="{{{ $oneStatus }}}" {{{ ($oneStatus == $status) ? 'selected="selected" ' : '' }}}>
                                {{{ $statusDescription }}}
                                </option><?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr >
                <th class="text-right">Due Date/Time</th>
                <td ><input type="text" name="due_datetime" class="datetimepicker" value="{{{ $due_datetime }}}" /></td>
            </tr>
            <tr >
                <th class="text-right">Estimated Length</th>
                <td><input type="text" name="estimated_length_seconds" value="{{{ $estimated_length_seconds }}}" />
                    <a href="#" data-toggle="modal" data-target="#estimatedLengthHelp" title="Get help on the format of this field" >
                        <span class="glyphicon glyphicon-question-sign" ></span>
                    </a>
                </td>
            </tr>
            <tr >
                <th class="text-right">Assigned To</th>
                <td><?php
                    // Show existing assignees - if the ticket exists!.
                    if (isset($ticket) && $ticket->id > 0) {
                        echo $ticket->assignedForView();
                    }?>
                    <select name="assigned_to_user_id" >
                        <option value="" >Select a user...</option>
                        <?php
                            foreach($allUsers as $oneUser) {
                                ?><option value="{{{ $oneUser->id }}}" {{{ ($oneUser->id == $assigned_to_user_id) ? 'selected="selected" ' : '' }}}>
                                {{{ $oneUser->username }}}
                                </option><?php
                            }
                        ?>
                    </select>
                    (Will be done with AJAX, etc eventually)
                </td>
            </tr>
            <tr >
                <td colspan="2" class="text-center" ><button type="submit" class="btn btn-lg btn-primary">Save</button></td>
            </tr>
        </tbody>
    </table>
    
</form>

<p >
    <?php
    if ($ticketId > 0) {?>
        <a href="/ticket/{{{ $ticketId }}}" ><span class="label label-primary">Cancel</span></a>
    <?php
    }?>
    <a href="/projects" ><span class="label label-primary">Back to projects</span></a>
</p>

<div class="modal fade" id="descriptionHelp" tabindex="-1" role="dialog" aria-labelledby="descriptionHelp" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Description</h4>
            </div>
            <div class="modal-body">
                <p >This is for the full descrition of the ticket. You can use any of the <a href="http://en.wikipedia.org/wiki/Markdown" target="_blank" >markdown</a> syntax.</p>
                <p >You can also reference other tickets in this description, and it will create a "hard" link between the two (ie if you viewed the other ticket, you'd see a reference to this one).</p>
                <p >The reference consists of a relationship type indicator, followed by a hash ("#"), followed by the ticket id, eg ~#1234.<br />
                    The relationship types are:
                    <ul >
                        <li><strong >~</strong> - "is associated with", "see also"</li>
                        <li><strong >&gt;</strong> - "this is dependant upon"</li>
                        <li><strong >&lt;</strong> - "this is a dependency of"</li>
                        <li><strong >+</strong> - "this is a parent of"</li>
                        <li><strong >-</strong> - "this is a child of"</li>
                    </ul>
                </p>
                <p >
                    For example: +#1122, &gt;#3344
                </p>
                <p >
                    When viewing the ticket, the references are shown as links to the other ticket. If the reference is not valid (eg the ticket doesn't exist), it will be shown with square brackets around the id: &gt;#[4455].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="estimatedLengthHelp" tabindex="-1" role="dialog" aria-labelledby="estimatedLengthHelp" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Estimated Length</h4>
            </div>
            <div class="modal-body">
                <p >This is essentially a length of time (specifically a number of seconds)</p>
                <p >To make it easier, you enter the number of different units of time (minutes, hours, etc).
                    Enter the number, then the unit. You can enter more than one, and the times will be added.</p>
                <p >The units are:
                    <ul >
                        <li >m = minutes</li>
                        <li >h = hours</li>
                        <li >d = days</li>
                        <li >w = weeks</li>
                    </ul>
                </p>
                <p >The system will interpret your entries intelligently, so if you enter 48h, it will be deemed to be 2d. Also "30m 15m" will be deened to be "45m".</p>
                <p >Examples:
                    <ul >
                        <li >"30m" = 30 minutes"</li>
                        <li >"4h" = 4 hours"</li>
                        <li >"1d 12h" = 1 day, 12 hours (ie 1.5 days)</li>
                    </ul>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@stop
