<?php
class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(['id' => 1, 'username' => 'Unit tester', 'password' => 'unittest']);
    }

}