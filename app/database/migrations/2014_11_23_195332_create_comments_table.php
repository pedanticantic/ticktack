<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('ticket_id')->unsigned();
            $table->integer('added_by_user_id')->unsigned();
            $table->text('details');
			$table->timestamps();
            
            $table->foreign('added_by_user_id')->references('id')->on('users');
            $table->foreign('ticket_id')->references('id')->on('tickets');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
