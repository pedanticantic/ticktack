<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotesToTimeSpent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// We want to add a mandatory column to a table that contains records, so we add the
		// column with nulls allowed, update the values to some default, then alter it to
		// mandatory.
		Schema::table('time_spent', function(Blueprint $table)
		{
			$table->string('notes', 250)->nullable()->after('duration');
		});
		
		DB::table('time_spent')->update([
			'notes' => 'No notes'
		]);
		
		// There doesn't seem to be the opposite of "->nullable()".
		DB::statement('ALTER TABLE `time_spent` MODIFY `notes` VARCHAR(250) NOT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('time_spent', function(Blueprint $table)
		{
			$table->dropColumn('notes');
		});
	}

}
