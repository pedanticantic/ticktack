<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToDoViewsTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('to_do_views_tickets', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('to_do_view_id')->unsigned();
            $table->integer('ticket_id')->unsigned();
			$table->integer('x_position');
			$table->integer('y_position');
			$table->integer('width');
			$table->integer('height');
			$table->string('view_summary', 60)->nullable();
			$table->timestamps();
			
            $table->foreign('to_do_view_id')->references('id')->on('to_do_views');
            $table->foreign('ticket_id')->references('id')->on('tickets');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('to_do_views_tickets');
	}

}
