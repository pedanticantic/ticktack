<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsAssociationsView extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// @TODO: Might be an idea to write a DB function to reverse the relationship type.
		$viewDef = "CREATE OR REPLACE VIEW tickets_associations_view AS " .
				   // Get the records from the point of view of the "src" ticket.
		           "SELECT `tasrc1`.`id`, `tasrc1`.`relationship_type`, `tasrc1`.`src_ticket_id`, `tasrc1`.`src_sort_order`, `tasrc1`.`trg_ticket_id`, `tasrc1`.`trg_sort_order`, `tasrc1`.`pinned` " .
		           "FROM   `tickets_associations` AS `tasrc1` " .
		           "UNION ALL " .
				   // Get the records from the point of view of the "trg" ticket.
		           "SELECT `tatrg1`.`id`, " .
		           "       CASE `tatrg1`.`relationship_type` " .  // Need to reverse the meaning of the relationship.
		           "         WHEN 'dependent' THEN 'dependency' " .
		           "         WHEN 'dependency' THEN 'dependent' " .
		           "         WHEN 'parent' THEN 'child' " .
		           "         WHEN 'child' THEN 'parent' " .
		           "         ELSE 'associated' " .
		           "       END, " .
		           "       `tatrg1`.`trg_ticket_id`, `tatrg1`.`trg_sort_order`, `tatrg1`.`src_ticket_id`, `tatrg1`.`src_sort_order`, `tatrg1`.`pinned` " .
		           "FROM   `tickets_associations` AS `tatrg1` " .
				   // But exclude any "trg" ticket that was included in the "src" part.
		           "LEFT JOIN `tickets_associations` AS `tadupe1` ON " .
		           "          `tatrg1`.`src_ticket_id` = `tadupe1`.`trg_ticket_id` " .
		           "AND       `tatrg1`.`trg_ticket_id` = `tadupe1`.`src_ticket_id` " .
		           "AND       `tatrg1`.`id` != `tadupe1`.`id` " .
		           "AND       `tatrg1`.`relationship_type` = " .
		           "            CASE `tadupe1`.`relationship_type` " .  // Need to reverse the meaning of the relationship.
		           "              WHEN 'dependent' THEN 'dependency' " .
		           "              WHEN 'dependency' THEN 'dependent' " .
		           "              WHEN 'parent' THEN 'child' " .
		           "              WHEN 'child' THEN 'parent' " .
		           "            ELSE 'associated' " .
		           "          END " .
		           "WHERE  `tadupe1`.`id` IS NULL " .
		           "ORDER BY `src_ticket_id`, `src_sort_order` ";
		DB::statement($viewDef);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("DROP VIEW `tickets_associations_view`");
	}

}
