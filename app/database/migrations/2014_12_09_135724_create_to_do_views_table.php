<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToDoViewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('to_do_views', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('owner_user_id')->unsigned();
			$table->string('view_name', 60);
			$table->integer('viewport_x');
			$table->integer('viewport_y');
			$table->timestamps();
			
            $table->foreign('owner_user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('to_do_views');
	}

}
