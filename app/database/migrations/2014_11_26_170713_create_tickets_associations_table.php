<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsAssociationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * The relationship direction is confusing; the rule is:
	 * "trg_ticket_id is relationship_type of src_ticket_id"
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets_associations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('relationship_type', ['associated', 'dependent', 'dependency', 'parent', 'child']);
            $table->integer('src_ticket_id')->unsigned();
            $table->tinyInteger('src_sort_order')->unsigned();
            $table->integer('trg_ticket_id')->unsigned();
            $table->tinyInteger('trg_sort_order')->unsigned();
			$table->boolean('pinned');
			$table->timestamps();
			
            $table->foreign('src_ticket_id')->references('id')->on('tickets');
            $table->foreign('trg_ticket_id')->references('id')->on('tickets');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets_associations');
	}

}
