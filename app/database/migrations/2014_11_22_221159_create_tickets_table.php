<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->string('summary', 100);
            $table->text('description');
            $table->integer('created_by_user_id')->unsigned();
            $table->enum('priority', ['very high', 'high', 'medium', 'low', 'very low']);
            $table->enum('status', ['new', 'assigned', 'in progress', 'resolved', 'closed']);
            $table->timestamp('due_datetime')->nullable();
            $table->integer('estimated_length_seconds')->unsigned()->nullable();
			$table->timestamps();
            
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('created_by_user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
