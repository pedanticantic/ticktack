ticktack
========

Ticket tracking system to be used as a playground for my development whims

This is not a commercial product - do not use it for real data.

It is:
* to hone my existing development skills
* to learn new skills
* to demonstrate my abilities to potential employers if I ever look to change jobs in the future

WARNINGS
========

Please assume everything you enter on this system will be publicly visible.

I reserve the right to delete users, tickets, comments, projects, etc.
