/*
** The initialise method is called by the main javascript file after JQuery has been included and
** the page has finished loading.
*/

todoview = {
    viewId: null,  // Will be populated with the view id.
    gridSize: 10,  // A fundamental property of this universe!
    $container: null,  // The container DOM element.
    $canvas: null,  // The canvas DOM element.
    $viewport: null,  // The viewport DOM element.
    initialise: function() {
        // Check all the various DOM elements exist.
        this.$container = $('#tdv_canvas_container');
        this.$canvas = $('#tdv_canvas');
        this.$viewport = $('#tdv_viewport');
        if (this.$container.length == 1 && this.$canvas.length == 1 && this.$viewport.length == 1) {
            // Need to set the sizes of the canvas container and canvas. Then set the position
            // of the container relative to the viewport, and the canvas relative to the container.
            // Give them reasonable settings now. Then when the AJAX request returns with all the
            // ticket info, set them properly.
            this.$container.css({
                height: (3 * this.$viewport.height()) + 'px',
                width: (3 * this.$viewport.width()) + 'px',
                top: (-1 * this.$viewport.height()) + 'px',
                left: (-1 * this.$viewport.width()) + 'px'
            });
            // Give the canvas an initial size and position while we wait for the AJAX request
            // to be made & received. The result will determine the actual size/position.
            this.$canvas.css({
                height: (2 * this.$viewport.height()) + 'px',
                width: (2 * this.$viewport.width()) + 'px',
                top: (0.5 * this.$viewport.height()) + 'px',
                left: (0.5 * this.$viewport.width()) + 'px'
            });
            // Now make the canvas draggable within the confines of the container.
            this.$canvas.draggable({
                containment: "parent",
                stop: function() {
                    // Save the new position of the canvas to the database.
                    todoview.canvas.saveViewportPosition();
                }
            });
            // Set the height of the viewport based on the size of the window.
            this.setViewportSize();
            // Add an event handler in case the user changes the size in future.
            $(window).resize(function() {
                todoview.setViewportSize();
            })
            // Add an event listener to handle the user double-clicking a relationship box.
            this.$canvas.on('dblclick', '.relationships .side .box', function() {
                var ticketId = $(this).data('tdvRemoteTicketId');
                //console.log('Double-clicked - remote ticket is: ' + ticketId);
                var ticket = todoview.getTicketById(ticketId);
                if (ticket == null) {
                    // Ticket doesn't exist in the view, so make a request to the server to add it.
                    todoview.addTicketToView(ticketId, $(this).data('tdvThisTicketId'));
                } else {
                    ticket.navigate();
                }
            });
            // When the "edit summary" pop-up is shown, put the cursor in the textarea.
            $('#toDoViewsEditSummary').on('shown.bs.modal', function(ev) {
                $('#ticket_view_summary').focus();
            });
            // Add a "save" handler for the edit summary form.
            $('#save_summary').on('click', function() {
                var ticket = todoview.getTicketById(todoview.editSummaryTicketId);
                if (ticket != null) {
                    // Put the new view-specific summary into the ticket (even if empty).
                    newSummary = $('#ticket_view_summary').val();
                    ticket.setSummary(newSummary);
                    ticket.storeViewSpecificSummary(true);  // The "true" means force-remove the value.
                    // Put the new summary in the ticket on screen.
                    ticket.domElement.find('.summary').html(ticket.getSummary());
                    // Send an AJAX request to the server to update the database.
                    $.getJSON(
                        '/todoview/' + todoview.viewId + '/ticket/' + ticket.id + '/summary',
                        {cmd: 'update', view_summary: newSummary},
                        function(result) {
                            // @TODO: Handle the result being false.
                            // console.log('Result of save...');
                            // console.log(result);
                        }
                    );
                }
            });
            // Add a "clear" handler for the edit summary form. Clear the textarea and put the cursor in it.
            $('#clear_summary').on('click', function() {
                $('#ticket_view_summary').val('').focus();
            });
        }
        // Make an API call to the server to get the tickets and relationships on this view.
        todoview.viewId = this.$canvas.data('viewId');
        //console.log('View id: ' + todoview.viewId);
        $.getJSON(
            '/todoview/' + todoview.viewId,
            function(viewData) {
                // console.log('It seemed to be successful');
                // console.log(viewData);
                var tickets = viewData.tickets;
                for(tidx in tickets) {
                    var ticket = tickets[tidx];
                    todoview.tickets.push(new todoview.ticket(ticket.id, ticket.x_pos, ticket.y_pos, ticket.type_image, ticket.status_image, ticket.summary, ticket.view_summary));
                }
                var rels = viewData.relationships;
                for(ridx in rels) {
                    var rel = rels[ridx];
                    //console.log('Next rel: ' + rel.id);
                    todoview.relationships.push(new todoview.relationship(rel.id, rel.src_ticket_id, rel.trg_ticket_id, rel.relationship_type));
                }
                todoview.canvas.setViewportOffset(viewData.viewport.x, viewData.viewport.y);
                //console.log('All tickets...');
                //console.log(todoview.tickets);
                //console.log('All relationships...');
                //console.log(todoview.relationships);
                todoview.drawAll();
            }
        ).fail(function() {
            alert('Failed to retrieve the ticket data for this view. Try re-loading the page.');
        });
    },
    setViewportSize: function() {
        // Set the viewport size. The width is set in the CSS to be 100%, which doesn't need to
        // change, so we only need to worry about the height.
        // We want the bottom of the viewport to be n pixels from the bottom of the window.
        // Therefore the height must be: <window height> - <viewport top pos> - <required gap at bottom>
        var windowHeight = $( window ).height();
        var viewportTop = parseInt($('#tdv_viewport').offset().top, 10);
        //console.log('Window height: ' + windowHeight + '; viewport top: ' + viewportTop);
        var viewportHeight = windowHeight - viewportTop - 125;
        $('#tdv_viewport').height(viewportHeight + 'px');
    },
    // An object to represent one ticket.
    ticket: function(id, x, y, typeImage, statusImage, global_summary, view_summary) {
        this.id = id;
        this.x_pos = parseInt(x, 10);
        this.y_pos = parseInt(y, 10);
        this.typeImage = typeImage;
        this.statusImage = statusImage;
        this.global_summary = global_summary;
        this.view_summary = null;  // Gets set after the "Summary" methods below.
        this.relationships = new Array();  // Array of references to its relationships.
        this.domElement = null;
        
        // Method to return the appropriate summary (view-specific if there is one, otherwise global).
        // It must replace tag- and ampersand type things with the html entity.
        this.getSummary = function() {
            var theSummary = (this.view_summary == null) ? this.global_summary : this.view_summary;
            //theSummary = theSummary.replace(/&/g, '&amp;');  // Must do this one first, of course!
            theSummary = theSummary.replace(/</g, '&lt;');
            theSummary = theSummary.replace(/>/g, '&gt;');
            return theSummary;
        }
        this.setSummary = function(newSummary) {
            this.view_summary = (newSummary == '' ? null : newSummary);
        }
        this.setSummary(view_summary);
        
        // Method to "draw" this ticket on screen.
        this.drawTicket = function() {
            //console.log('Drawing ticket #' + this.id);
            if (this.domElement == null) {
                //console.log('DOM Element is null');
                // Need to create the DOM element. It's quite complicated, hence using lots of vars.
                var ticketHeader = this.typeImage +
                                   this.statusImage +
                                   '<span class="ticket_id" ><a title="Click to view this ticket" href="/ticket/' + this.id + '">' + this.id + '</a></span>' +
                                   '<span class="remove pull-right label label-danger" title="Click to remove this ticket from this view" >x</span>';
                var ticketRels = {left: '', right: ''};
                for(ridx in this.relationships) {
                    var relInfo = this.relationships[ridx].getBoxInfo(this.id);
                    ticketRels[relInfo.prop] += '<div class="box" id="' + relInfo.boxId + '" data-tdv-this-ticket-id="' + this.id + '" data-tdv-remote-ticket-id="' + relInfo.remoteTicketId + '" title="Double-click to add this ticket or navigate to it." >' + relInfo.html + '</div>';
                }
                ticketHtml = '<div class="ticket" id="ticket_' + this.id + '" data-tdv-ticket-id="' + this.id + '" title="Click and drag to move this ticket" >' +
                                '<div class="header" >' + ticketHeader + '</div>' +
                                '<div class="body" ><span class="glyphicon glyphicon-pencil ticket_edit" title="Click to edit the summary for this view" data-toggle="modal" data-target="#toDoViewsEditSummary" ></span><span class="summary" >' + this.getSummary() + '</span></div>' +
                                '<div class="relationships" ><div class="side left" >' + ticketRels.left + '</div><div class="side right" >' + ticketRels.right + '</div></div>' +
                             '</div>'
                this.domElement = $(ticketHtml);
                $('#tdv_canvas').append(this.domElement);
                // The nnnew DOM elements have a class that causes an animation, but the relationship
                // lines are drawn as the animation starts, so the ends don't appear in the right
                // place, so set a timer to correct them when the animation finishes.
                var thisTicket = this;  // For the closure below.
                window.setTimeout(
                    function() {
                        thisTicket.drawRelationships();
                    }
                    ,500
                );
                // Make it draggable within the confines of the canvas, in steps equal to the grid size.
                this.domElement.draggable({
                    containment: "parent",
                    grid: [todoview.gridSize, todoview.gridSize],
                    start: function() {
                        todoview.zIndex += 10;
                        $(this).css('zIndex', todoview.zIndex);
                    },
                    drag: function() {
                        var ticketId = $(this).data('tdvTicketId');
                        var ticket = todoview.getTicketById(ticketId);
                        if (ticket != null) {
                            ticket.drawRelationships();
                        }
                    },
                    stop: function() {
                        // We need to tell the ticket object and the server that this ticket on this view has a new position.
                        var ticketId = $(this).data('tdvTicketId');
                        var ticket = todoview.getTicketById(ticketId);
                        if (ticket != null) {
                            // console.log('Ticket dragging stopped. Ticket id is: ' + ticketId + '; View id is: ' + todoview.viewId);
                            ticket.drawRelationships();
                            ticket.updatePosition($(this).position());
                        }
                    }
                });
                // Add a handler for the remove button.
                this.domElement.find('.remove').click(function() {
                    if (confirm('Are you sure you want to remove this ticket from this view?')) {
                        var ticketId = $(this).parent().parent().data('tdvTicketId');
                        var ticket = todoview.getTicketById(ticketId);
                        if (ticket != null) {
                            ticket.removeFromView();
                        }
                    }
                });
                // Add a handler to show/hide the edit icon when the mouse moves over the ticket.
                this.domElement.on('mouseover mouseout', function() {
                    $(this).find('.ticket_edit').toggleClass('show_edit');
                });
                // Add a handler to populate the "edit summary" pop-up when the edit icon is clicked.
                this.domElement.find('.ticket_edit').on('click', function() {
                    var ticketId = $(this).parent().parent().data('tdvTicketId');
                    var ticket = todoview.getTicketById(ticketId);
                    if (ticket != null) {
                        ticket.popluateEditSummary();
                    }
                });
            }
            // Now position the element.
            //console.log('Positioning the element');
            this.domElement.css({
                left: (todoview.gridSize * this.x_pos + todoview.canvas.origin_x) + 'px',
                top: (todoview.gridSize * this.y_pos + todoview.canvas.origin_y) + 'px'
            });
        };
        this.drawRelationships = function() {
            // Simply loop through all the ticket's relationships and ask each one
            // to draw itself.
            for(ridx in this.relationships) {
                this.relationships[ridx].drawRelationship();
            }
        }
        this.hideTicket = function() {
            //console.log('Removing the DOM elements from the document');
            this.domElement.addClass('remove');
            var thisTicket = this;  // For the closure below.
            // The timing of this must match the CSS animation value (currently 0.75s).
            window.setTimeout(function() {
                thisTicket.domElement.remove();
                for(ridx in thisTicket.relationships) {
                    thisTicket.relationships[ridx].hideRelationship();
                }
                todoview.removeTicket(thisTicket.id);
                // Check whether the canvas size/position needs to be adjusted.
                todoview.canvas.setSizeAndPosition();
            },
            750);
        };
        // Update the position of the ticket in the database. We must take into account the
        // position of the origin, of course.
        this.updatePosition = function(newPosition) {
            // Get the positions, factoring in the grid size.
            var left = (parseInt(newPosition.left, 10) - todoview.canvas.origin_x) / todoview.gridSize;
            var top = (parseInt(newPosition.top, 10) - todoview.canvas.origin_y) / todoview.gridSize;
            // console.log('New position is: ' + left + ', ' + top);
            // Set the position in this object.
            this.x_pos = left;
            this.y_pos = top;
            // Now tell the server.
            // @TODO: The success callback will receive data about any tickets that had to be moved
            //        to accommodate this ticket barging in here.
            $.getJSON(
                '/todoview/' + todoview.viewId + '/ticket/' + this.id + '/update_position',
                {x_pos: this.x_pos, y_pos: this.y_pos},
                function(updateInfo) {
                    //console.log('Server update after ticket drop seems to be successful');
                    //console.log(updateInfo.affected);
                    // Finish this off...
                    // The data includes a 'success' property - test it!
                    todoview.moveTickets(updateInfo.affected);
                }
            ).fail(function() {
                alert('Failed to inform the server of the new ticket position. Try re-loading and moving it again.');
            });
            // Check whether the canvas size/position needs to be adjusted.
            todoview.canvas.setSizeAndPosition();
        };
        this.removeFromView = function() {
            //console.log('User wants to remove this ticket (' + this.id + ') from the view');
            thisTicket = this;  // Keep a reference to the object for the closure.
            $.getJSON(
                '/todoview/' + todoview.viewId + '/ticket/' + this.id + '/remove',
                function(updateInfo) {
                    // console.log('It seemed to be successful');
                    // console.log(updateInfo);
                    // @TODO: Finish this off...
                    // The data includes a 'success' property - test it!
                    
                    // Call the routine to remove the ticket from the screen.
                    // It does a little animation, then removes all the DOM elements and data.
                    thisTicket.hideTicket();
                }
            ).fail(function() {
                alert('Failed to inform the server. Try re-loading and removing it again.');
            });
        }
        this.removeRelationships = function() {
            //console.log('Removing relationships...');
            // Repeatedly take the first relationship, remove references to this
            // ticket from that relationship, then remove the relationship from
            // the array. Keep going until there are none left.
            while (this.relationships.length > 0) {
                //console.log('Next relationship...');
                // This should really be in the relationship object.
                thisRel = this.relationships[0];
                if (thisRel.srcTicket != null && thisRel.srcTicket.id == this.id) {
                    thisRel.srcTicket = null;
                }
                if (thisRel.trgTicket != null && thisRel.trgTicket.id == this.id) {
                    thisRel.trgTicket = null;
                }
                this.relationships.splice(0, 1);
            }
        }
        
        // Method to navigate to this ticket - ie centre the canvas on this ticket.
        this.navigate = function() {
            // Work out the position relative to the viewport where we want the top-left corner
            // of the ticket to appear.
            var viewportOffset = todoview.$viewport.offset();
            viewportOffset.left += (todoview.$viewport.width() / 2) - 60;
            viewportOffset.top += (todoview.$viewport.height() / 2) - 60;
            //console.log('Centre of viewport should be...');
            //console.log(viewportOffset);
            // Work out the current top-left corner of the ticket.
            var ticketOffset = $('#ticket_' + this.id).offset();
            //console.log('Ticket offset...');
            //console.log(ticketOffset);
            // Work out the vector we would need to move the ticket to get it in the centre.
            var slideX = viewportOffset.left - ticketOffset.left;
            var slideY = viewportOffset.top  - ticketOffset.top;
            //console.log('Slide vector: ' + slideY + ', ' + slideX);
            // Work out the current position of the canvas.
            var canvasOffset = todoview.$canvas.offset();
            //console.log('Canvas offset...');
            //console.log(canvasOffset);
            // Add the vector on to its offset and update the actual value in the canvas.
            // Use the JQuery animate method to make a lovely smooth slide to the new position.
            todoview.$canvas.animate(
                {left: '+=' + slideX, top: '+=' + slideY},
                800,
                function() {
                    // Save the new position of the canvas to the database.
                    todoview.canvas.saveViewportPosition();
                });
        }
        
        // Method to set a new position for the ticket. It updates the internal position, then
        // redisplays the ticket and its relationships.
        this.setPosition = function(newPosition) {
            if (newPosition.x) {
                this.x_pos = newPosition.x;
            }
            if (newPosition.y) {
                this.y_pos = newPosition.y;
            }
            this.drawTicket();
            this.drawRelationships();
        }
        
        // Method to populate the "Edit summary" pop-up with the details from this ticket.
        // It's called when the user clicks the edit icon.
        this.popluateEditSummary = function() {
            todoview.editSummaryTicketId = this.id;  // Remember which ticket this is.
            $('#ticket_global_summary').html(this.global_summary);
            $('#ticket_view_summary').val(this.view_summary);
        }
        
        // Method to store the view-specific summary for this ticket & view in local storage.
        this.storeViewSpecificSummary = function(forceRemove) {
            forceRemove = (forceRemove === true);
            var storeKey = todoview.viewId + ':' + this.id;
            if (forceRemove || this.view_summary == null) {
                // View-specific summary is null, so remove any entry from storage.
                localStorage.removeItem(storeKey);
            } else {
                // @TODO: Maybe prefix the value with a date? Will help when garbage-collecting.
                var storeValue = this.view_summary;
                window.localStorage.setItem(storeKey, storeValue);
            }
        }
    },
    // An object representing one relationship
    relationship: function(id, srcId, trgId, relType) {
        //console.log('Creating a relationship - ' + id);
        this.id = id;
        // Remember the raw ticket ids in case those tickets are not in the view - user still
        // needs to know they're there.
        this.srcTicketId = srcId;
        this.trgTicketId = trgId;
        // For the source and target tickets, find and store the matching ticket object (if there
        // is one). If one was found, add a reference to this relationship to that ticket's list
        // of relationships. Ie there are pointers boths ways between tickets and relationships.
        this.srcTicket = todoview.getTicketById(this.srcTicketId);
        if (this.srcTicket != null) {
            //console.log('Source ticket exists!');
            this.srcTicket.relationships.push(this);
        }
        this.trgTicket = todoview.getTicketById(this.trgTicketId);
        if (this.trgTicket != null) {
            //console.log('Target ticket exists!');
            this.trgTicket.relationships.push(this);
        }
        this.relationshipType = relType;
        // Determine which side (left or right) the relationship should be shown for the
        // source ticket. It's automatically on the opposite side for the target.
        this.leftSide = true;
        if (this.relationshipType == 'parent' || this.relationshipType == 'dependency') {
            this.leftSide = false;
        } else {
            if (this.relationshipType == 'associated' && this.srcTicketId < this.trgTicketId) {
                this.leftSide = false;
            }
        }
        //console.log('For rel ' + this.id + ', ' + this.trgTicketId + ' is a ' + this.relationshipType + ' of ' + this.srcTicketId);
        //console.log(this.leftSide);
        
        this.getBoxInfo = function(forTicketId) {
            var result = {prop: '', html: '', boxId: '', remoteTicketId: ''}
            var relType = this.toSymbol(this.relationshipType);
            if (forTicketId == this.trgTicketId) {
                result.boxId = this.trgTicketId + '_' + this.srcTicketId;
                result.remoteTicketId = this.srcTicketId;
                if (this.leftSide) {
                    result.prop = 'left';
                    result.html = this.reverseSymbol(relType) + ' ' + this.srcTicketId + '<br />';
                } else {
                    result.prop = 'right';
                    result.html = this.srcTicketId + ' ' + relType + '<br />';
                }
            } else {
                result.boxId = this.srcTicketId + '_' + this.trgTicketId;
                result.remoteTicketId = this.trgTicketId;
                if (this.leftSide) {
                    result.prop = 'right';
                    result.html = this.trgTicketId + ' ' + this.reverseSymbol(relType) + '<br />';
                } else {
                    result.prop = 'left';
                    result.html = relType + ' ' + this.trgTicketId + '<br />';
                }
            }
            result.boxId = 'box_' + result.boxId + '_' + this.relationshipType;
            //console.log('Result...');
            //console.log(result);
            return result;
        }
        
        this.reverseType = function(currentType) {
            switch (currentType) {
                case 'parent':
                    currentType = 'child';
                    break;
                case 'child':
                    currentType = 'parent';
                    break;
                case 'dependent':
                    currentType = 'dependency';
                    break;
                case 'dependency':
                    currentType = 'dependent';
                    break;
            }
            return currentType;
        }
        
        this.reverseSymbol = function(currentType) {
            switch (currentType) {
                case '&gt;':
                    currentType = '&lt;';
                    break;
                case '&lt;':
                    currentType = '&gt;';
                    break;
                case '+':
                    currentType = '-';
                    break;
                case '-':
                    currentType = '+';
                    break;
            }
            return currentType;
        }
        
        this.toSymbol = function(relType) {
            switch (relType) {
                case 'parent':
                    relType = '&lt;';
                    break;
                case 'child':
                    relType = '&gt;';
                    break;
                case 'dependent':
                    relType = '+';
                    break;
                case 'dependency':
                    relType = '-';
                    break;
                case 'associated':
                    relType = '~';
                    break;
            }
            return relType;
        }
        
        // This method draws a line between the two tickets at either end of the relationship.
        // Sounds simple... it's not!
        // @TODO: I should define all the different types of paths, give each one a number. Then
        //        when the line segments are created, keep references to them. While tickets
        //        are being dragged, if the path type stays the same, we only need to update
        //        their positions.
        this.drawRelationship = function() {
            //console.log('Drawing relationship: ' + this.id);
            // Only do this if both tickets are in the view.
            if (this.srcTicket == null || this.trgTicket == null) {
                return;
            }
            
            // For now, we recreate everything, even when dragging, so do this now.
            // @TODO: in future, I will simply adjust the positions/sizes of the lines
            //        where possible.
            groupClass = 'rel_group_' + this.id;
            $('.' + groupClass).remove();
            horizClass = 'line horizontal ' + this.relationshipType;
            vertClass = 'line vertical ' + this.relationshipType;
            
            // Okay, identify the two "boxes" that we need to link together.
            // I've made it easy because their ids are "box_ticketid1_ticketid2_relationship".
            // Then the parent cell will have a class of either left or right so we know which
            // side it's on. We call a method for each end of the line. It returns salient
            // information, like the exact coordinates of the endpoint and whether it facing left
            // or right.
            startInfo = this.getLineEndpointInfo(this.srcTicketId, this.trgTicketId);
            endInfo   = this.getLineEndpointInfo(this.trgTicketId, this.srcTicketId);
            //console.log('Endpoints...');
            //console.log(startInfo);
            //console.log(endInfo);
            
            // Okay, we need to work out which of several orientations & relative positions
            // we're dealing with. To make it easy, we first make sure "start" is facing right
            // and "end" left (it's not possible for them both to face the same way).
            if (startInfo.dir == 'left' && endInfo.dir == 'right') {
                tmp = startInfo;
                startInfo = endInfo;
                endInfo = tmp;
            }
            
            // Now, there are a few relative positions to consider.
            // If "end" is to the right of "start", it's relatively easy.
            if (startInfo.x_pos + 10 < endInfo.x_pos) {
                // Need a line going right for 5 pixels, then vertical to reach the level of
                // the end, then horizontal for (gap - 10), which should bring us to the gap.
                line2Top = startInfo.y_pos;
                line2Height = endInfo.y_pos - startInfo.y_pos;
                line3Top = startInfo.y_pos + line2Height;
                line3Width = endInfo.x_pos - startInfo.x_pos - 5;
                if (line2Height < 0) {
                    line2Height = -line2Height;
                    line2Top -= line2Height;
                }
                line1 = '<div class="' + horizClass + ' ' + groupClass + '" style="left: ' + startInfo.x_pos + 'px; top: ' + startInfo.y_pos + 'px; width: 5px; height: 3px"><br /><div>';
                $(line1).appendTo('#tdv_canvas');
                line2 = '<div class="' + vertClass + ' ' + groupClass + '" style="left: ' + (5 + startInfo.x_pos) + 'px; top: ' + line2Top + 'px; width: 3px; height: ' + line2Height + 'px"><br /><div>';
                $(line2).appendTo('#tdv_canvas');
                line3 = '<div class="' + horizClass + ' ' + groupClass + '" style="left: ' + (5 + startInfo.x_pos) + 'px; top: ' + line3Top + 'px; width: ' + line3Width + 'px; height: 3px"><br /><div>';
                $(line3).appendTo('#tdv_canvas');
            } else {
                // The path is a little more complicated. It now needs to go:
                // right (15px), up/down, left, up/down, right (5px)
                line2x = startInfo.x_pos + 15;
                line3y = Math.max(startInfo.y_pos, endInfo.y_pos) + 20;  // @TODO: this needs a lot more work!
                if (startInfo.ticket_top > (   endInfo.ticket_bottom + 15)) {
                    line3y = startInfo.ticket_top - 20;
                } else if ( endInfo.ticket_top > ( startInfo.ticket_bottom + 15)) {
                    line3y = endInfo.ticket_top - 20;
                }
                line2y = Math.min(startInfo.y_pos, line3y);
                line2height = 1 + Math.abs(line3y - startInfo.y_pos);
                line4x = endInfo.x_pos - 10;
                line3width = line2x - line4x;
                line4height = Math.abs(line3y - endInfo.y_pos);
                line4y = Math.min(endInfo.y_pos, line3y);
                
                line1 = '<div class="' + horizClass + ' ' + groupClass + '" style="left: ' + startInfo.x_pos + 'px; top: ' + startInfo.y_pos + 'px; width: 15px; height: 3px"><br /><div>';
                $(line1).appendTo('#tdv_canvas');
                line2 = '<div class="' + vertClass + ' ' + groupClass + '" style="left: ' + line2x + 'px; top: ' + line2y + 'px; width: 3px; height: ' + line2height + 'px"><br /><div>';
                $(line2).appendTo('#tdv_canvas');
                line3 = '<div class="' + horizClass + ' ' + groupClass + '" style="left: ' + line4x + 'px; top: ' + line3y + 'px; width: ' + line3width + 'px; height: 3px"><br /><div>';
                $(line3).appendTo('#tdv_canvas');
                line4 = '<div class="' + vertClass + ' ' + groupClass + '" style="left: ' + line4x + 'px; top: ' + line4y + 'px; width: 3px; height: ' + line4height + 'px"><br /><div>';
                $(line4).appendTo('#tdv_canvas');
                line5 = '<div class="' + horizClass + ' ' + groupClass + '" style="left: ' + line4x + 'px; top: ' + endInfo.y_pos + 'px; width: 15px; height: 3px"><br /><div>';
                $(line5).appendTo('#tdv_canvas');
                
            }
        }
        
        this.getLineEndpointInfo = function(ticketId1, ticketId2) {
            result = {dir: null, x_pos: null, y_pos: null, ticket_top: null, ticket_bottom: null};
            canvasOffset = $('#tdv_canvas').offset();
            $box = $('#box_' + ticketId1 + '_' + ticketId2 + '_' + this.relationshipType);
            if ($box.length == 0) {
                $box = $('#box_' + ticketId1 + '_' + ticketId2 + '_' + this.reverseType(this.relationshipType));
            }
            // Some temp code just to make sure I'm picking things up correctly.
            boxPos = $box.offset();
            boxPos.top  -= canvasOffset.top;
            boxPos.left -= canvasOffset.left;
            if ($box.parent().hasClass('right')) {
                boxPos.top += 10;
                boxPos.left += $box.width() + 3;
                result.dir = 'right';
            } else {
                boxPos.top += 10;
                boxPos.left -= 1;
                result.dir = 'left';
            }
            boxPos.left = Math.round(boxPos.left);
            boxPos.top = Math.round(boxPos.top);
            result.x_pos = boxPos.left;
            result.y_pos = boxPos.top;
            
            $ticket = $('#ticket_' + ticketId1);
            ticketOffset = $ticket.offset();
            ticketOffset.top -= canvasOffset.top;
            ticketOffset.top = Math.round(ticketOffset.top);
            result.ticket_top = ticketOffset.top;
            ticketBottom = ticketOffset.top + $ticket.height();
            result.ticket_bottom = ticketBottom;
            return result;
        }
        
        // Method to remove all the relationship lines from the screen.
        this.hideRelationship = function() {
            // Luckily all the line have a very simple, relationship-specific class...
            $('.rel_group_' + this.id).remove();
        }
    },
    // Arrays of all tickets that have been added to this view, and all their relationships.
    tickets: new Array(),
    relationships: new Array(),
    // This z-index gets incremented and assigned to the ticket being dragged. It means the
    // ticket always appears above the other ones.
    zIndex: 1000,
    // The id of the ticket whose summary is being edited, if any.
    editSummaryTicketId: null,
    // Method to go through every ticket (in the view) and ask each one to draw itself.
    // The ticket will manage drawing its relationships.
    drawAll: function() {
        this.canvas.setSizeAndPosition();
        for(tidx in this.tickets) {
            this.tickets[tidx].drawTicket();
        }
        for(ridx in this.relationships) {
            this.relationships[ridx].drawRelationship();
        }
    },
    // Return the object representing the given ticket id, or null if there isn't one.
    getTicketById: function(ticketId, returnIndex) {
        for(tidx in this.tickets) {
            if (this.tickets[tidx].id == ticketId) {
                return (returnIndex === true ? tidx : this.tickets[tidx]);
            }
        }
        return null;
    },
    // Method to remove a ticket (and its relationships) from memory.
    removeTicket: function(ticketId) {
        // So, loop through the array of tickets until we find the one with this id.
        var ticketIndex = this.getTicketById(ticketId, true);
        if (ticketIndex != null) {
            // Save any view-specific summary in local storage.
            this.tickets[ticketIndex].storeViewSpecificSummary();
            //console.log('About to remove relationships...');
            this.tickets[ticketIndex].removeRelationships();
            //console.log('And removed the ticket from memory');
            this.tickets.splice(ticketIndex, 1);
        }
    },
    // Method to add the given ticket to the view.
    addTicketToView: function(ticketId, relativeToTicketId) {
        //console.log('Adding ticket #' + ticketId + ' relative to #' + relativeToTicketId);
        var viewSummary = this.checkLocalViewSummary(ticketId);
        $.getJSON(
            '/todoview/' + todoview.viewId + '/ticket/' + ticketId + '/add',
            {relative_ticket: relativeToTicketId, view_summary: viewSummary},
            function(addInfo) {
                // console.log('It seemed to have been successful');
                // console.log(addInfo);
                // @TODO: Finish this off...
                // @TODO: The data should include a 'success' property - test it!
                // addInfo should have 2 properties: "ticket" with an array of 1 ticket, and
                // "relationships" with zero or more relationships.
                // Create the ticket and the relationships.
                var ticket = addInfo.tickets[0];
                var ticketObj = new todoview.ticket(ticket.id, ticket.x_pos, ticket.y_pos, ticket.type_image, ticket.status_image, ticket.summary, ticket.view_summary);
                todoview.tickets.push(ticketObj);
                var rels = addInfo.relationships;
                for(ridx in rels) {
                    var rel = rels[ridx];
                    //console.log('Next rel: ' + rel.id);
                    // @TODO: Need to make sure we don't duplicate any relationships.
                    todoview.relationships.push(new todoview.relationship(rel.id, rel.src_ticket_id, rel.trg_ticket_id, rel.relationship_type));
                }
                // Draw the new ticket, which will also show any new relationships.
                ticketObj.drawTicket();
                ticketObj.drawRelationships();
                // Check whether the canvas size/position needs to be adjusted.
                todoview.canvas.setSizeAndPosition();
            }
        ).fail(function() {
            alert('Failed to inform the server. Try re-loading and removing it again.');
        });
    },
    // Canvas object to manage offsets, etc.
    canvas: {
        viewport_x: 0,
        viewport_y: 0,
        origin_x: 0,
        origin_y: 0,
        // Remember the last min/max canvas sizes. When dragging & dropping a ticket, etc,
        // if the sizes change, we need to redraw everything.
        lastSizes: {
            min_x: 0,
            min_y: 0,
            max_x: 0,
            max_y: 0
        },
        setViewportOffset: function( offset_x, offset_y) {
            this.viewport_x = parseInt(offset_x, 10);
            this.viewport_y = parseInt(offset_y, 10);
            // console.log('Setting viewport offset...');
            // console.log(this);
        },
        // Method to take all the ticket positions and the viewport size,
        // and determine the size and position of the canvas DOM element.
        setSizeAndPosition: function() {
            // This is a bit involved. The canvas must cover all the tickets in the view,
            // but must also be at least as big as the viewport. Then the canvas container
            // must be the size of the canvas plus the viewport height/width.
            var min_x = 0,
                min_y = 0,
                max_x = 0,
                max_y = 0;
            for(tidx in todoview.tickets) {
                this_ticket = todoview.tickets[tidx];
                if (this_ticket.x_pos < min_x) {
                    min_x = this_ticket.x_pos;
                }
                if (this_ticket.y_pos < min_y) {
                    min_y = this_ticket.y_pos;
                }
                if (this_ticket.x_pos > max_x) {
                    max_x = this_ticket.x_pos;
                }
                if (this_ticket.y_pos > max_y) {
                    max_y = this_ticket.y_pos;
                }
            }
            max_x += 12;  // To account for the width of the tickets.
            max_y += 16;  // To account for the height of the tickets.
            
            // This is called if the user moves a ticket, etc. If the dimensions haven't changed,
            // don't do anything.
            if (this.lastSizes.min_x == min_x &&
                this.lastSizes.min_y == min_y &&
                this.lastSizes.max_x == max_x &&
                this.lastSizes.max_y == max_y) {
                //console.log('No need to change canvas size/pos');
                return;
            }
            //console.log('We do need to change canvas size/pos');
            
            // Remember the new dimensions.
            this.lastSizes.min_x = min_x;
            this.lastSizes.min_y = min_y;
            this.lastSizes.max_x = max_x;
            this.lastSizes.max_y = max_y;
            // console.log('Last sizes...');
            // console.log(this.lastSizes);
            // Store the position of the origin relative to the top-left corner of the canvas.
            this.origin_x = todoview.$viewport.width() + (min_x * todoview.gridSize);
            this.origin_y = todoview.$viewport.height() + (min_y * todoview.gridSize);
            // console.log('Origin x,y...');
            // console.log({x: this.origin_x, y: this.origin_y});
            
            // console.log('Viewport dims...');
            // console.log({width: todoview.$viewport.width(), height: todoview.$viewport.height()});
            canvas_width = (max_x - min_x) * todoview.gridSize + 2 * todoview.$viewport.width();
            canvas_height = (max_y - min_y) * todoview.gridSize + 2 * todoview.$viewport.height();
            // console.log('Canvas DOM width/height...');
            // console.log({canvas_width, canvas_height});
            // Set the width/height of the canvas element.
            todoview.$canvas.width(canvas_width + 'px');
            todoview.$canvas.height(canvas_height + 'px');
            // Calculate the width and height of the container, and set it.
            container_width = 2 * canvas_width - todoview.$viewport.width();
            container_height = 2 * canvas_height - todoview.$viewport.height();
            todoview.$container.width(container_width + 'px');
            todoview.$container.height(container_height + 'px');
            // Now set the position of the container.
            container_x = todoview.$viewport.width() - canvas_width;
            container_y = todoview.$viewport.height() - canvas_height;
            todoview.$container.css('left', container_x + 'px');
            todoview.$container.css('top', container_y + 'px');
            
            // We keep track of the position of the viewport relative to the origin on the canvas.
            // We have to "subtract" the position of the container from the canvas origin.
            canvas_x = - this.origin_x - container_x - (todoview.gridSize * this.viewport_x);
            canvas_y = - this.origin_y - container_y - (todoview.gridSize * this.viewport_y);
            todoview.$canvas.css('left', canvas_x + 'px');
            todoview.$canvas.css('top', canvas_y + 'px');
            
            todoview.drawAll();
            
        },
        // Method to take the current canvas position and save it back to the database.
        // It's called after the user drags & drops, or the system moves the canvas to
        // centre a ticket (double-click a related ticket).
        saveViewportPosition: function() {
            // So, work out the position of the canvas, taking in to account the position
            // of the container and the canvas, the location of the origin, and the grid size.
            // console.log('Positions of canvas and container...');
            // console.log(todoview.$canvas.position());
            // console.log(todoview.$container.position());
            canvas_pos = todoview.$canvas.position();
            container_pos = todoview.$container.position();
            // @TODO: I have no idea why I have to add 9 and 13....
            this.viewport_x = 9 - Math.round((this.origin_x + (canvas_pos.left + container_pos.left)) / todoview.gridSize);
            this.viewport_y = 13 - Math.round((this.origin_y + (canvas_pos.top + container_pos.top)) / todoview.gridSize);
            // console.log('New viewport position...');
            // console.log({x: this.viewport_x, y: this.viewport_y});
            
            // Send a POST request to the server to update the viewport position.
            // In theory it should be a PUT, but the shorthand AJAX method doesn't support it.
            $.post(
                '/todoview/' + todoview.viewId + '/update_viewport_position',
                {viewport_x: this.viewport_x, viewport_y: this.viewport_y}
            );
        }
    },
    // This method simply repositions tickets that were affected by the user dropping a ticket
    // on top of them.
    moveTickets: function(ticketIds) {
        //console.log('Move tickets');
        //console.log(ticketIds);
        for(var tidx in ticketIds) {
            ticketToMove = todoview.getTicketById(tidx);
            ticketToMove.setPosition({x: ticketIds[tidx]});
        }
    },
    // Method to see if there is a stored view-specific summary for the given ticket,
    // and whether the user wants to restore it.
    // It returns null if either there isn't one, or the user doesn't want to restore it;
    // or the summary if they do.
    checkLocalViewSummary: function(ticketId) {
        var storeKey = todoview.viewId + ':' + ticketId;
        var result = window.localStorage.getItem(storeKey);
        if (result) {
            var restore = confirm('There is a local copy of the view-specific summary for this ticket. Do you want to restore it?');
            if (!restore) {
                result = null;
            }
        }
        return result;
    }
    
}