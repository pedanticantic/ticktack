$('document').ready(function() {
    ticktack.initialise();
});

ticktack = {
    initialise: function() {
        
        // Handle the user wanting to add a comment.
        $('#toggle_add_comment').click(function() {
            $addCommentContainer = $('#add_comment');
            $addCommentButton = $('#toggle_add_comment');
            $addCommentContainer.toggleClass('hide');
            if ($addCommentContainer.hasClass('hide')) {
                $addCommentButton.html($addCommentButton.data('ttAdd'));
            } else {
                $addCommentButton.html($addCommentButton.data('ttCancel'));
                $('#comment_box').focus();
            }
        });
        
        // Handle the user wanting to add a relation.
        $('#toggle_add_relation').click(function() {
            $addRelationContainer = $('#add_relation');
            $addRelationButton = $('#toggle_add_relation');
            $addRelationContainer.toggleClass('hide');
            if ($addRelationContainer.hasClass('hide')) {
                $addRelationButton.html($addRelationButton.data('ttAdd'));
            } else {
                $addRelationButton.html($addRelationButton.data('ttCancel'));
                $('#relation_box').focus();
            }
        });
        
        // Handle the user wanting to record time against a ticket.
        $('#toggle_record_time_spent').click(function() {
            $recordTimeSpentContainer = $('#record_time_spent');
            $recordTimeSpentButton = $('#toggle_record_time_spent');
            $recordTimeSpentContainer.toggleClass('hide');
            if ($recordTimeSpentContainer.hasClass('hide')) {
                $recordTimeSpentButton.html($recordTimeSpentButton.data('ttAdd'));
            } else {
                $recordTimeSpentButton.html($recordTimeSpentButton.data('ttCancel'));
                $('#duration').focus();
            }
        });
        
        /*
         * Various methods to pop-up confirmation boxes for deleting records.
         * @TODO: In future, we could use a single class of "confirm-delete" with
         *        a single handler, and then have the message as a data attribute
         *        of the DOM element.
         */
        // Handle the user wanting to delete an assignment.
        $('.confirm-delete-assignment').click(function() {
            return confirm('Are you sure you want to remove this assignment?');
        });
        // Confirm that the user really wants to delete the relationship.
        $('.confirm-delete-relationship').click(function() {
            return confirm('Are you sure you want to delete this relationship?');
        });
        
        // Add the date and/or time picker functionality to fields with the appropriate tags/classes.
        $('input.datetimepicker').datetimepicker({
            format: 'd/m/Y H:i'
        });
        
        // Make the system remember the last ticket tab to be clicked.
        // When a tab is clicked, store the id in a cookie.
        $('a[data-toggle="tab"]').on('click', function(e){
            // Save the latest tab using a cookie. We store the id of the link object.
            $.cookie('last_ticket_tab', $(e.target).attr('id'));
        });
        // On loading the page, simulate a click on that tab (if we remembered one).
        var lastCommentTab = $.cookie('last_ticket_tab');
        if (lastCommentTab) {
            $('#' + lastCommentTab).click();
        }
        
        // Anything with a class of 'click-on-load' should be clicked on load. Pretty obvious, really.
        $('.click-on-load').click();
        
        // If the todoview object exists, initialise it.
        if (window.todoview) {
            todoview.initialise();
        }
        
    }
}